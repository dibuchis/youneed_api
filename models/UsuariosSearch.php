<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Usuarios;
use yii\db\Expression;

/**
 * UsuariosSearch represents the model behind the search form about `app\models\Usuarios`.
 */
class UsuariosSearch extends Usuarios
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tipo_identificacion', 'estado', 'habilitar_rastreo', 'plan_id', 'banco_id',
                'preferencias_deposito', 'estado_validacion_documentos', 'traccar_id', 'servicio_id'], 'integer'],
            [['jornada_trabajo', 'ubicacion_name'], 'string'],
            [['identificacion', 'imagen', 'nombres', 'apellidos', 'email', 'numero_celular', 'telefono_domicilio',
                'clave', 'token_push', 'token', 'fecha_creacion', 'activacion', 'fecha_desactivacion',
                'causas_desactivacion', 'fecha_cambio_plan', 'tipo_cuenta', 'numero_cuenta', 'observaciones',
                'imei', 'multiple_id', 'servicio_id', 'ubicacion_name', 'nombre_beneficiario'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usuarios::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            //   'id' => $this->id,
            'tipo_identificacion' => $this->tipo_identificacion,
            'estado' => $this->estado,
            'habilitar_rastreo' => $this->habilitar_rastreo,
            'fecha_creacion' => $this->fecha_creacion,
            'activacion' => $this->activacion,
            'fecha_desactivacion' => $this->fecha_desactivacion,
            'plan_id' => $this->plan_id,
            'fecha_cambio_plan' => $this->fecha_cambio_plan,
            'banco_id' => $this->banco_id,
            'preferencias_deposito' => $this->preferencias_deposito,
            'estado_validacion_documentos' => $this->estado_validacion_documentos,
            'traccar_id' => $this->traccar_id,
        ]);

        $query->andFilterWhere(['like', 'identificacion', $this->identificacion])
            ->andFilterWhere(['like', 'imagen', $this->imagen])
            ->andFilterWhere(['like', 'nombres', $this->nombres])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'numero_celular', $this->numero_celular])
            ->andFilterWhere(['like', 'telefono_domicilio', $this->telefono_domicilio])
            ->andFilterWhere(['like', 'clave', $this->clave])
            ->andFilterWhere(['like', 'token_push', $this->token_push])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'causas_desactivacion', $this->causas_desactivacion])
            ->andFilterWhere(['like', 'tipo_cuenta', $this->tipo_cuenta])
            ->andFilterWhere(['like', 'numero_cuenta', $this->numero_cuenta])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'imei', $this->imei]);

        return $dataProvider;
    }

    public function searchSuperadmin($params)
    {
        $query = Usuarios::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            //  'id' => $this->id,
            'tipo_identificacion' => $this->tipo_identificacion,
            'estado' => $this->estado,
            'habilitar_rastreo' => $this->habilitar_rastreo,
            'fecha_creacion' => $this->fecha_creacion,
            'activacion' => $this->activacion,
            'fecha_desactivacion' => $this->fecha_desactivacion,
            'plan_id' => $this->plan_id,
            'fecha_cambio_plan' => $this->fecha_cambio_plan,
            'banco_id' => $this->banco_id,
            'preferencias_deposito' => $this->preferencias_deposito,
            'estado_validacion_documentos' => $this->estado_validacion_documentos,
            'traccar_id' => $this->traccar_id,
            'es_super' => 1,
        ]);

        $query->andFilterWhere(['like', 'identificacion', $this->identificacion])
            ->andFilterWhere(['like', 'imagen', $this->imagen])
            ->andFilterWhere(['like', 'nombres', $this->nombres])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'numero_celular', $this->numero_celular])
            ->andFilterWhere(['like', 'telefono_domicilio', $this->telefono_domicilio])
            ->andFilterWhere(['like', 'clave', $this->clave])
            ->andFilterWhere(['like', 'token_push', $this->token_push])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'causas_desactivacion', $this->causas_desactivacion])
            ->andFilterWhere(['like', 'tipo_cuenta', $this->tipo_cuenta])
            ->andFilterWhere(['like', 'numero_cuenta', $this->numero_cuenta])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'imei', $this->imei]);
        return $dataProvider;
    }

    public function searchClientes($params)
    {
        $query = Usuarios::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tipo_identificacion' => $this->tipo_identificacion,
            'estado' => $this->estado,
            'habilitar_rastreo' => $this->habilitar_rastreo,
            'fecha_creacion' => $this->fecha_creacion,
            'activacion' => $this->activacion,
            'fecha_desactivacion' => $this->fecha_desactivacion,
            'plan_id' => $this->plan_id,
            'fecha_cambio_plan' => $this->fecha_cambio_plan,
            'banco_id' => $this->banco_id,
            'preferencias_deposito' => $this->preferencias_deposito,
            'estado_validacion_documentos' => $this->estado_validacion_documentos,
            'traccar_id' => $this->traccar_id,
            'es_cliente' => 1,
        ]);

        $query->andFilterWhere(['like', 'identificacion', $this->identificacion])
            ->andFilterWhere(['like', 'imagen', $this->imagen])
            ->andFilterWhere(['like', 'nombres', $this->nombres])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'numero_celular', $this->numero_celular])
            ->andFilterWhere(['like', 'telefono_domicilio', $this->telefono_domicilio])
            ->andFilterWhere(['like', 'clave', $this->clave])
            ->andFilterWhere(['like', 'token_push', $this->token_push])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'causas_desactivacion', $this->causas_desactivacion])
            ->andFilterWhere(['like', 'tipo_cuenta', $this->tipo_cuenta])
            ->andFilterWhere(['like', 'numero_cuenta', $this->numero_cuenta])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'imei', $this->imei]);

        return $dataProvider;
    }

    public function searchAsociados($params)
    {
        $query = Usuarios::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => array_merge($dataProvider->getSort()->attributes, [
                'ubicacion_name' => [
                    'asc' => ['ciudades.nombre' => SORT_ASC, 'paises.nombre' => SORT_ASC],
                    'desc' => ['ciudades.nombre' => SORT_DESC, 'paises.nombre' => SORT_DESC],
                    'label' => 'Ubicacion',
                    'default' => SORT_ASC
                ],
                'nombreCompleto',
                'multiple_id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC,],
                    'label' => 'Id',
                    'default' => SORT_ASC
                ],
            ])
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        isset($this->servicio_id) ? $query->joinWith("usuariosServicios", false, 'INNER JOIN') : '';

            $query->joinWith("ciudad", false, 'INNER JOIN')
                ->joinWith("pais", false, 'INNER JOIN');
        isset($this->banco_id) ? $query->joinWith("banco", false, 'INNER JOIN') : '';
        isset($this->plan_id) ? $query->joinWith("plan", false, 'INNER JOIN') : '';

        $query->groupBy('usuarios.id'); //to fix the pagination on use joinWith


        $query->andFilterWhere([
            'usuarios.id' => $this->id,
            'tipo_identificacion' => $this->tipo_identificacion,
            'estado' => $this->estado,
            'habilitar_rastreo' => $this->habilitar_rastreo,
            'fecha_creacion' => $this->fecha_creacion,
            'activacion' => $this->activacion,
            'fecha_desactivacion' => $this->fecha_desactivacion,
            'plan_id' => $this->plan_id,
            'fecha_cambio_plan' => $this->fecha_cambio_plan,
            'banco_id' => $this->banco_id,
            'preferencias_deposito' => $this->preferencias_deposito,
            'estado_validacion_documentos' => $this->estado_validacion_documentos,
            'traccar_id' => $this->traccar_id,
            'es_asociado' => 1,
            'usuarios_servicios.servicio_id' => $this->servicio_id,
            'bancos.id' => $this->banco_id,
            'planes.id' => $this->plan_id,
        ]);

        $query->andFilterWhere(['like', 'identificacion', $this->identificacion])
            ->andFilterWhere(['like', 'imagen', $this->imagen])
            ->andFilterWhere(['like', 'nombres', $this->nombres])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'numero_celular', $this->numero_celular])
            ->andFilterWhere(['like', 'telefono_domicilio', $this->telefono_domicilio])
            ->andFilterWhere(['like', 'clave', $this->clave])
            ->andFilterWhere(['like', 'token_push', $this->token_push])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'causas_desactivacion', $this->causas_desactivacion])
            ->andFilterWhere(['like', 'tipo_cuenta', $this->tipo_cuenta])
            ->andFilterWhere(['like', 'numero_cuenta', $this->numero_cuenta])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'imei', $this->imei]);

        $query->andFilterWhere(['IN', 'usuarios.id', $this->multiple_id]);
        $query->andFilterWhere(['LIKE', new Expression("CONCAT(paises.nombre, ' / ', ciudades.nombre )"), $this->ubicacion_name]);
        // var_dump($query->createCommand()->getRawSql());die();
        return $dataProvider;
    }
}
