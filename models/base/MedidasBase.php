<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "medidas".
*
    * @property integer $id
    * @property string $descripcion
    * @property string $sufijo
*/
class MedidasBase extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'medidas';
}

/**
* @inheritdoc
*/
public function rules()
{
        return [
            [['descripcion', 'sufijo'], 'required'],
            [['descripcion'], 'string', 'max' => 120],
            [['sufijo'], 'string', 'max' => 15],
            [['sufijo'], 'unique'],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'id' => 'ID',
    'descripcion' => 'Medida',
    'sufijo' => 'Sufijo',
];
}
}