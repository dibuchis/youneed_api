<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Servicios;
use yii\db\Expression;

/**
 * ServiciosSearch represents the model behind the search form about `app\models\Servicios`.
 */
class ServiciosSearch extends Servicios
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'obligatorio_certificado', 'categoria_id','medida_id'], 'integer'],
            [['nombre', 'slug', 'incluye', 'no_incluye', 'imagen', 'categoria_id','aplica_iva','obligatorio_certificado',
            'medida_id','tiene_soft_delete'], 'safe'],
            [['proveedor_subtotal', 'proveedor_iva', 'proveedor_total', 'subtotal', 'iva', 'total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Servicios::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => array_merge($dataProvider->getSort()->attributes, [
                'categoria_id' => [
                    'asc' => ['categorias.nombre' => SORT_ASC],
                    'desc' => ['categorias.nombre' => SORT_DESC,],
                    'label' => 'Categoria',
                    'default' => SORT_ASC
                ],
            ])
        ]);

        $query->joinWith("categoriasServicios",true,' INNER JOIN ')
             ->joinWith('categoriasServicios.categoria', true, ' INNER JOIN ');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'servicios.id' => $this->id,
            // 'tarifa_base' => $this->tarifa_base,
            // 'tarifa_dinamica' => $this->tarifa_dinamica,
             'servicios.aplica_iva' => $this->aplica_iva,
             'obligatorio_certificado' => $this->obligatorio_certificado,
             'medida_id' => $this->medida_id,
             'tiene_soft_delete' => $this->tiene_soft_delete,
        ]);

        $query->andFilterWhere(['like', 'servicios.nombre', $this->nombre])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'incluye', $this->incluye])
            ->andFilterWhere(['like', 'no_incluye', $this->no_incluye])
            ->andFilterWhere(['like', 'categorias_servicios.categoria_id', $this->categoria_id])
            ->andFilterWhere(['like', 'servicios.imagen', $this->imagen]);

        return $dataProvider;
    }
}
