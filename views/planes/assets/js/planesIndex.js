function callIndexAsociadosModal(element) {
    let title =$(element).data('title');
    let url = $(element).data('url');
    $('#ajaxCrudModal').css('z-index', 99999);
    $.get(url, function (data) {
        $('#planesIndexModal').find('.title-modal').eq(0).remove();
        $('#planesIndexModal .modal-header').append('<h3 class="title-modal">'+title+'</h3>');
        $('#planesIndexModal').modal('show');
        $('#planesIndexModal .modal-content, #planesIndexModal .modal-dialog').css('width',' fit-content');
        $('#planesIndexModal').find('.modal-body').eq(0).html(data);
    });
}

