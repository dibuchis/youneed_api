<?php

use app\models\Planes;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'attribute' => 'nombre',
        /* 'value' => function ($model) {
             return $model->id;
         },*/
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(Planes::find()->asArray()->all(), 'nombre', 'nombre'),
        'filterWidgetOptions' => ['pluginOptions' => [ 'placeholder' => 'Escriba...', 'allowClear' => true,],],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'pvp',
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Delete',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'],
        'template' => '{view} {update} {delete} {asociados}',
        'buttons' => [
            'asociados' => function ($url, $model, $key) {
                return Html::a(' <span class="glyphicon glyphicon-list-alt"></span>', 'javascript:void(0)',
                    ['class' => 'btn btn-black modal-asociados', 'title' => 'Asociados', 'data-plan_id' => $model->id,
                        'data-url' => Url::toRoute(['usuarios/asociados', 'UsuariosSearch[plan_id]' => $model->id]),
                        'data-title'=>'Asociados segun '.$model->nombre,
                        'onclick' => 'callIndexAsociadosModal(this)']);
            }
        ]
    ],

];   