<?php

use app\models\Configuraciones;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */
?>
<div class="pedidos-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            ['attribute' => 'cliente_id',
                'value' => function ($model) {
                    return "[ID - {$model->cliente_id}] {$model->cliente->nombreCompleto}";
                },
            ],
            ['attribute' => 'asociado_id',
                'value' => function ($model) {
                    return "[ID - {$model->asociado_id}] {$model->asociado->nombreCompleto}";
                },
            ],
            'latitud',
            'longitud',
            'identificacion',
            'razon_social',
            'email:email',
            'telefono',
            'fecha_para_servicio',
            'direccion_completa:ntext',
            'observaciones_adicionales:ntext',
            ['attribute' => 'forma_pago',
                'label' => Yii::t('app', 'Forma de pago'),
                'value' => function ($model) {
                    return Yii::$app->params['forma_pago'][$model->forma_pago];
                },
            ],
            ['attribute' => 'tarjeta.numero',
                'label' => Yii::t('app', '# Tarjeta'),
                'value' => function ($model) {
                    return isset($model->tarjeta) ? $model->tarjeta->numero : '';
                },
            ],
            'codigo_postal',
            ['attribute' => 'tipo_atencion',
                'value' => function ($model) {
                    return Yii::$app->params['tipo_atencion'][$model->tipo_atencion];;
                },
            ],
            'tiempo_llegada',
            'fecha_creacion',
            ['attribute' => 'estado',
                'value' => function ($model) {
                    return Yii::$app->params['estados_pedidos'][$model->estado];;
                },
            ],
            'subtotal_cliente',
            'iva_cliente',
            'iva_0_cliente',
            'iva_impuesto_cliente',
            'total_cliente',
            'fecha_llegada_atencion',
            'fecha_finalizacion_atencion',
            'valores_transferir_asociado',
            'valores_cancelacion_servicio_cliente',
            'tiempo_aproximado_llegada',
            ['attribute' => 'ciudad_id',
                'value' => function ($model) {
                    return isset($model->ciudad) ? $model->ciudad->pais->nombre . ' / ' . $model->ciudad->nombre : ' ';
                },
            ],

            ['attribute' => 'servicio_id',
                'label' => Yii::t('app', 'Servicio ID'),
                'value' => function ($model) {
                    return $model->servicio->id;;
                },
            ],
            ['attribute' => 'servicio_id',
                'value' => function ($model) {
                    return $model->servicio->nombre;;
                },
            ],
            'cantidad',
            'subtotal',
            'iva',
            'iva_0',
            'iva_impuesto',
            'total',
            'identificacion_factura',
            'nombre_factura',
            'direccion'
        ],
    ]) ?>

</div>
