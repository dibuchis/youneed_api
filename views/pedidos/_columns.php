<?php

use app\common\models\constants\YouNeedContext;
use app\models\Servicios;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'id',
        'width' => '30px',
    ],
    [
        'attribute' => 'cliente_id',
        'label' => Yii::t('app', 'Cliente'),
        'value' => function ($model) {
            if ($model->cliente) {
                return $model->cliente->getNombreCompleto();
            } else {
                return NULL;
            }
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'placeholder' => 'Escriba...',
                'minimumInputLength' => 2,
                'allowClear' => true,
                'ajax' => [
                    'url' => Url::to(['usuarios/ajax-get-usuarios']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term, tipo:"CLIENTE",targetField:"nombres"}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(data) { return data.nombreCompleto; }'),
                'templateSelection' => new JsExpression('function (data) {  return data.text; }'),

            ],
        ],
    ],

    [
        'attribute' => 'asociado_id',
        'label' => Yii::t('app', 'Asociado'),
        'value' => function ($model) {
            if ($model->asociado) {
                return $model->asociado->getNombreCompleto();
            } else {
                return NULL;
            }
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'placeholder' => 'Escriba...',
                'minimumInputLength' => 2,
                'allowClear' => true,
                'ajax' => [
                    'url' => Url::to(['usuarios/ajax-get-usuarios']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term, 
                                                tipo:"ASOCIADO",targetField:"nombres"}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(data) { return data.nombreCompleto; }'),
                'templateSelection' => new JsExpression('function (data) {   return data.text; }'),

            ],
        ],
    ],
    [
        'attribute' => 'servicio_id',
        'value' => function ($model) {
            return isset($model->servicio) ? "[ID - {$model->servicio_id}] ".$model->servicio->nombre : '';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(Servicios::find()->where('tiene_soft_delete=0')->All(), 'id', 'nombre'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Seleccione...',],
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fecha_creacion',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fecha_para_servicio',
    ],
    [
        'attribute' => 'forma_pago',
        'label' => Yii::t('app', 'Forma de pago'),
        'value' => function ($model) {
            return Yii::$app->params['forma_pago'][$model->forma_pago];
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(YouNeedContext::getTiposFormaPagos(), 'id', 'value'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Seleccione...',],
        ],
    ],
    [
        'attribute' => 'estado',
        'label' => Yii::t('app', 'Estado'),
        'value' => function ($model) {
            return Yii::$app->params['estados_pedidos'][$model->estado];
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(YouNeedContext::getTiposEstados(), 'id', 'value'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Seleccione...',],
        ],
    ],
    [
        'attribute' => 'cantidad',
    ],
	[
		'attribute' => 'ciudad_id',
		'value' => 'ciudad.nombre'
	 ],
    'subtotal_cliente',
    'iva_cliente',
    'total_cliente',
    'subtotal_asociado',
    'iva_asociado',
    'total_asociado',
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
          'template' => '{view}',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        }, 
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
    ],

];