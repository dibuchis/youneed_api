<?php

use app\common\models\constants\YouNeedContext;
use app\models\Ciudades;
use app\models\Tarjetas;
use app\models\Usuarios;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pedidos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pedidos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cliente_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Usuarios::find()->where('es_cliente=1')->asArray()->all(), 'id',
            function ($model) {
                return $model['nombres'] . ' ' . $model['apellidos'];
            }),
        'options' => ['placeholder' => 'Seleccione Cliente'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'asociado_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Usuarios::find()->where('es_asociado=1')->asArray()->all(), 'id',
            function ($model) {
                return $model['nombres'] . ' ' . $model['apellidos'];
            }),
        'options' => ['placeholder' => 'Seleccione Cliente'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'latitud')->textInput() ?>

    <?= $form->field($model, 'longitud')->textInput() ?>

    <?= $form->field($model, 'identificacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'razon_social')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombres')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_para_servicio')->textInput() ?>

    <?= $form->field($model, 'direccion_completa')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'observaciones_adicionales')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ciudad_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Ciudades::find()->asArray()->all(), 'id',
            function ($model) {
                return $model['nombre'];
            }),
        'options' => ['placeholder' => 'Seleccione Ciudad'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'forma_pago')->widget(Select2::classname(), [
        'data' =>  ArrayHelper::map(YouNeedContext::getTiposFormaPagos(), 'id', 'value'),
        'options' => ['placeholder' => 'Seleccione Ciudad'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>


    <?= $form->field($model, 'tarjeta_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Tarjetas::find()->asArray()->all(), 'id','numero'),
        'options' => ['placeholder' => 'Seleccione tarjeta'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'codigo_postal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_atencion')->widget(Select2::classname(), [
        'data' =>  ArrayHelper::map(YouNeedContext::getTiposAtencion(), 'id', 'value'),
        'options' => ['placeholder' => 'Seleccione Tipo Atencion'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>


    <?= $form->field($model, 'tiempo_llegada')->textInput() ?>

    <?= $form->field($model, 'fecha_creacion')->textInput() ?>

    <?= $form->field($model, 'estado')->widget(Select2::classname(), [
        'data' =>  ArrayHelper::map(YouNeedContext::getTiposEstados(), 'id', 'value'),
        'options' => ['placeholder' => 'Seleccione Tipo Atencion'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <?= $form->field($model, 'subtotal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iva')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iva_0')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_llegada_atencion')->textInput() ?>

    <?= $form->field($model, 'fecha_finalizacion_atencion')->textInput() ?>

    <?= $form->field($model, 'valores_transferir_asociado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'valores_cancelacion_servicio_cliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tiempo_aproximado_llegada')->textInput() ?>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
