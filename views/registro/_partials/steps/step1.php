<?php

use app\models\Ciudades;
use borales\extensions\phoneInput\PhoneInput;
use dosamigos\fileupload\FileUpload;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="panel panel-primary setup-content panel-default" id="step-1">
    <div class="panel-heading">
        <h3 class="panel-title">Queremos conocerte mejor</h3>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <h4>Datos Personales:</h4>
            </div>
            <div class="col-md-3 col-md-offset-2 foto-container">
                <?php
                echo $form->field($model, 'imagen')->textarea(['style' => 'display:none;']);
                echo Html::img($model->imagen, ['style' => 'height:200px;', 'id' => 'vista_previa_imagen']); ?>
                <label for="usuarios-imagen_upload" id="img-upload-plus"><i
                            class="glyphicon glyphicon-plus"></i></label>
                <?= Html::img(Url::to('@web/images/ajax-loader.gif'), ['class' => 'loader']); ?>
                <?= FileUpload::widget([
                    'model' => $model,
                    'attribute' => 'imagen_upload',
                    'url' => ['ajax/subirfotografia', 'id' => $model->id],
                    'options' => ['accept' => 'image/*'],
                    'clientOptions' => [
                        'maxFileSize' => 6000000,
                        'dataType' => 'json'
                    ],
                    'clientEvents' => [
                        'fileuploaddone' => 'function(e, data) {
															//console.log(data);
						                                    $("#usuarios-imagen").val( data.result[0].base64 );
						                                    $("#vista_previa_imagen").attr("src", data.result[0].base64);
						                                    $(".loader").hide();
						                                }',
                        'fileuploadfail' => 'function(e, data) {
											var msg = "";
											var obj = Object.keys(data.messages);
											obj.forEach(function(e){ msg +=  "<br>" + data.messages[e] });
											swal.fire({
                                            "type": "error",
                                            "html": "Hubo un error." + msg
                                            });
											//console.log(data.messages);
                                            $(".loader").hide();
						                                }',
                        'fileuploadstart' => 'function(e, data) {
						            	$(".loader").show();
						                                }',
                    ],
                ]); ?>
                <div class="alert alert-info" style="max-width: 200px;">
                    Subir una imagen con una foto tamaño carnet, el rostro debe ser visible. (Peso máximo 2mb, formato
                    png|jpg)
                </div>
            </div>
            <div class="col-md-5 col-md-pushed-2">
                <div class="row mt-2">
                    <div class="col-md-6">
                        <?= $form->field($model, 'nombres')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-12"></div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-md-6">
                        <?php
                        echo $form->field($model, 'numero_celular')->widget(PhoneInput::className(), [
                            'jsOptions' => [
                                // 'preferredCountries' => ['EC'],
                                'onlyCountries' => ['EC'],
                                'nationalMode' => false,
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-12"></div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'clave')->passwordInput(['autocomplete' => "new-password"]) ?>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group field-usuarios-clave required has-success">
                            <label class="control-label">Confirmar Clave</label>
                            <input type="password" id="confirmar_clave" autocomplete="off" class="form-control"
                                   required>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-md-12"></div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'pais_id')->widget(\kartik\widgets\Select2::classname(), [
                            'data' => \yii\helpers\ArrayHelper::map(\app\models\Paises::find()->orderBy('nombre')->asArray()->all(), 'id',
                                function ($model, $defaultValue) {
                                    return $model['nombre'];
                                }
                            ),
                            'options' => ['placeholder' => Yii::t('app', 'Seleccione un país'), 'id' => 'cat1-id', 'value' => 1],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                    </div>

                    <div class="col-md-6">
                        <?php
                        echo $form->field($model, 'ciudad_id')->widget(DepDrop::classname(), [
                            'type' => DepDrop::TYPE_SELECT2,
                            'data' => yii\helpers\ArrayHelper::map(Ciudades::find()->orderBy('nombre')->asArray()->all(), 'id',
                                function ($model, $defaultValue) {
                                    return $model['nombre'];
                                }
                            ),
                            'options' => ['placeholder' => Yii::t('app', 'Seleccione una cuidad'), 'id' => 'subcat11-id'],
                            'select2Options' => ['pluginOptions' => ['multiple' => false, 'allowClear' => true]],
                            'pluginOptions' => [
                                'depends' => ['cat1-id'],
                                'placeholder' => 'Seleccionar una ciudad',
                                'loadingText' => 'Cargando...',
                                'url' => Url::to(['/ajax/ciudades']),
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <h4>Documentos:</h4>
            </div>

            <div class="col-md-2 col-md-offset-2">
                <?= $form->field($model, 'tipo_identificacion')->dropDownList(Yii::$app->params['tipo_identificacion'], ['prompt' => 'Seleccione']) ?>
            </div>
            <div class="col-md-3 col-md-pushed-5">
                <?= $form->field($model, 'identificacion')->textInput(['maxlength' => true]) ?>
            </div>
            <!-- DOCUMENTOS -->
            <div class="col-md-8 col-md-offset-2">
                <!-- <hr> -->
                <?php
                //TODO: problemas a futuro no se alimenta desde la tabla tipo documento de la base de datos
                $array_documentos = Yii::$app->params['tipos_documentos'];

                foreach ($array_documentos as $documento) { ?>
                    <div class="document-input-asociado <?php echo($documento["atributo_upload"] == "fotografia_cedula_upload" ? "" : "hidden"); ?>"
                         id="<?php echo "form_" . $documento['atributo_upload']; ?>">
                        <?php echo $form->field($model, $documento['atributo_modelo'])->textarea(['style' => 'display:none;']); ?>
                        <a style="display: none;" target="_blank"
                           id="vista_<?php echo $documento['atributo_modelo']; ?>" class="btn-primary btn" href="">Ver
                            documento subido</a>
                        <?= Html::img(Url::to('@web/images/ajax-loader.gif'), ['class' => 'loader loader_' . $documento['atributo_modelo']]); ?>
                        <?= FileUpload::widget([
                            'model' => $model,
                            'attribute' => $documento['atributo_upload'],
                            'url' => ['ajax/subirdocumento', 'atributo_upload' => $documento['atributo_upload'], 'atributo_modelo' => $documento['atributo_modelo']],
                            'options' => ['accept' => 'image/*,application/pdf'],
                            'clientOptions' => [
                                'maxFileSize' => 2000000,
                                'dataType' => 'json'
                            ],
                            'clientEvents' => [
                                'fileuploaddone' => 'function(e, data) {
																$("#usuarios-' . $documento['atributo_modelo'] . '").val( data.result[0]["url"] );
																$("#vista_' . $documento['atributo_modelo'] . '").attr("href", data.result[0]["url"]);
																$("#vista_' . $documento['atributo_modelo'] . '").show();
																$(".loader_' . $documento['atributo_modelo'] . '").hide();
															}',
                                'fileuploadfail' => 'function(e, data) {

															}',
                                'fileuploadstart' => 'function(e, data) {
											$(".loader_' . $documento['atributo_modelo'] . '").show();
															}',
                            ],
                        ]); ?>
                        <hr>
                    </div>

                <?php } ?>
                <div class="alert alert-info">
                    El tamaño los archivos no debe ser mayor a 2MB
                </div>
            </div>
            <!-- FIN DOCUMENTOS -->
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-md-pushed-2">
                <button class="btn btn-primary nextBtn pull-right ml-15" type="button">Siguiente</button>
                <button class="btn btn-warning saveBtn pull-right ml-15" onclick="saveForm()" type="button">Guardar
                </button>
            </div>
        </div>
    </div>
</div>