<?php

use app\models\Bancos;
use kartik\widgets\Select2;
use \yii\helpers\ArrayHelper;
?>
<div class="panel panel-primary setup-content" id="step-3">
    <div class="panel-heading">
        <h3 class="panel-title">¿Dónde recibirás tu pago?</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-md-pushed-2">
                <div class="alert alert-info help-panel">
                    <!-- Los datos proporcionados servirán para realizar los pagos por sus servicios realizados -->
                    Registra los datos de tu cuenta bancaria, donde se te depositará los valores que
                    corresponden a la ejecución de tus servicios. Rápido y seguro.
                </div>
                <?= $form->field($model, 'banco_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Bancos::find()->orderBy('nombre')->asArray()->all(), 'id',
                        function ($model, $defaultValue) {
                            return $model['nombre'];
                        }
                    ),
                    'options' => ['placeholder' => 'Seleccione'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => false,
                    ],
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-md-pushed-2">
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'nombre_beneficiario')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'tipo_cuenta')->dropDownList(['Corriente' => 'Corriente', 'Ahorros' => 'Ahorros',], ['prompt' => 'Seleccione']) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'numero_cuenta')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-md-pushed-2">
                <div class="row">
                    <div class="col-md-8">
                        <?= $form->field($model, 'tipo_ruc')->dropDownList(Yii::$app->params['tipo_ruc'], ['prompt' => 'Seleccionar..']) ?>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-md-pushed-2">
                <button class="btn btn-primary nextBtn pull-right ml-15" type="button">Siguiente</button>
                <button class="btn btn-primary backBtn pull-right ml-15" type="button">Anterior</button>
                <button class="btn btn-warning saveBtn pull-right ml-15" onclick="saveForm()" type="button">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>