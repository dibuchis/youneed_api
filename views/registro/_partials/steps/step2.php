<?php

use app\models\Categorias;
use app\models\Servicios;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$_DAYS = ['LUNES' => 'Lunes', 'MARTES' => 'Martes', 'MIERCOLES' => 'Miércoles', 'JUEVES' => 'Jueves',
    'VIERNES' => 'Viernes', 'SABADO' => 'Sábado', 'DOMINGO' => 'Domingo',];
?>

<div class="panel panel-primary setup-content" id="step-2">
    <div class="panel-heading">
        <h3 class="panel-title">¿Qué servicios quieres brindar?</h3>
    </div>
    <div class="panel-body">
        <div class="col-md-10 col-md-offset-1 col-md-pushed-1">
            <div class="alert alert-info help-panel">
                Selecciona una o más categorías de acuerdo a tu experiencia y conocimientos” “Recuerda que algunos
                servicios requieren presentar certificados que validen tu experiencia y conocimientos.
            </div>
        </div>
        <div class="col-md-10 col-md-offset-1 col-md-pushed-1">
            <h4>Escoger Categoría:</h4>
        </div>
        <div class="col-md-10 col-md-offset-1 col-md-pushed-1 loader-wrapper">
            <?php
            $lista_categorias = ArrayHelper::map(Categorias::find()->orderBy('nombre')->asArray()->all(), 'id',
                function ($model, $defaultValue) {
                    return $model;
                }
            );

            echo '<div class="owl-carousel owl-carousel-cat owl-theme" id="owl-categorias">';
            foreach ($lista_categorias as $val) {
                echo "<div class='cat-item' data-id='" . $val["id"] . "'><img src='" . $val["imagen"] . "'><span>" . $val["nombre"] . "</span></div>";
            }
            echo '</div>';

            echo Html::hiddenInput('input-type-1', 'Additional value 1', ['id' => 'input-type-1']);
            echo Html::hiddenInput('input-type-2', 'Additional value 2', ['id' => 'input-type-2']);

            $lista_servicios = ArrayHelper::map(Servicios::find()->orderBy('nombre')->asArray()->all(), 'id',
                function ($model, $defaultValue) {
                    return $model;
                }
            );

            ?>
        </div>

        <div class="col-md-10 col-md-offset-1 col-md-pushed-1" id="seccion-servicios">
            <hr>
            <h4>Escoger Servicio:</h4>
        </div>
        <div class="col-md-10 col-md-offset-1 col-md-pushed-1 loader-wrapper">
            <?php
            echo '<div id="servicios-wrapper">';
            echo '<div class="owl-carousel owl-carousel-serv owl-theme" id="owl-servicios">';
            echo '</div>';
            echo '</div>';
            echo '<hr>';
            echo '<h4>Servicios Agregados:</h4>';
            echo "<div class='servicios-agregados' id='servicios-agregados'> </div>";
            echo "<div class='alert alert-info'>Los campos marcados con asterisco (<span class='reqCertSrvInfo'>*</span>) requieren certificado</div>";
            echo '<hr>';
                      echo $form->field($model, 'categorias')->hiddenInput(['maxlength' => true])->label(false);
            echo $form->field($model, 'servicios')->hiddenInput(['maxlength' => true])->label(false);
          ?>

        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-md-pushed-1">
                <div class="col-md-12">
                    <?= $form->field($model, 'jornada_trabajo')->hiddenInput()  ?>

                    <?= $this->render('components/_workCalendar') ?>
                </div>
                <button class="btn btn-primary nextBtn pull-right ml-15" type="button">Siguiente</button>
                <button class="btn btn-primary backBtn pull-right ml-15" type="button">Anterior</button>
                <button class="btn btn-warning saveBtn pull-right ml-15" onclick="saveForm()" type="button">Guardar
                </button>
            </div>
        </div>
    </div>
</div>