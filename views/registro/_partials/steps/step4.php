<?php

use app\models\Planes;
use yii\helpers\Html;

?>
<div class="panel panel-primary setup-content" id="step-4">
    <div class="panel-heading">
        <h3 class="panel-title">Escoge tu plan para empezar a dar tus servicios</h3>
    </div>
    <div class="panel-body">
        <div class="col-md-8 col-md-offset-2 col-md-pushed-2">
            <div class="alert alert-info help-panel">
                El pago del plan no se efectuará al momento del registro, unicamente se hará por una sola vez
                cuando tengas tu primer contrato con un cliente y realices el servicio. Si deseas conocer los
                beneficios de ser Asociado haz click <a href="/registro/beneficios" target="_blank">aquí</a>.
            </div>
        </div>

        <section id="plans">

            <div class="row">
                <?php
                $planes = Planes::find()->all();
                ?>
                <!-- item -->
                <div class="col-md-4 col-md-offset-2 text-center">
                    <div class="panel panel-success panel-pricing" id="free-plan-panel">
                        <div class="panel-heading-plan">
                            <i class="fa fa-desktop"></i>
                            <h3><span class="plan-name"><?php echo $planes[0]->nombre; ?></span><br/>
                                $<?php echo $planes[0]->pvp; ?> USD</h3>
                        </div>
                        <div class="panel-body text-center real-price-panel">
                            <p>
                                <strong>Precio sin descuento: <span
                                        class="real-price"><?php echo $planes[0]->sin_descuento; ?> USD </span>
                                </strong>
                                <br>
                                <small>(<?php echo $planes[0]->descuento_1; ?> % de descuento)</small>
                            </p>
                        </div>
                        <?php echo $planes[0]->descripcion; ?>
                        <div class="panel-footer">
                            <a plan-id="<?php echo $planes[0]->id; ?>"
                               plan-nombre="<?php echo $planes[0]->nombre; ?>"
                               class="btn btn-lg btn-block btn-success seleccion_plan" href="javascript:;">Seleccionar</a>
                        </div>
                    </div>
                </div>
                <!-- /item -->

                <!-- item -->
                <div class="col-md-4 col-md-pushed-2 text-center">
                    <div class="panel panel-success panel-pricing" id="normal-plan-panel">
                        <div class="panel-heading-plan">
                            <i class="fa fa-desktop"></i>
                            <h3><span class="plan-name"><?php echo $planes[1]->nombre; ?></span><br/>
                                $<?php echo $planes[1]->pvp; ?>  </h3>
                        </div>
                        <div class="panel-body text-center real-price-panel">
                            <p>
                                <strong>Precio sin descuento: <span
                                        class="real-price"><?php echo $planes[1]->sin_descuento; ?> USD </span>
                                </strong>
                                <br>
                                <small>(<?php echo $planes[1]->descuento_1; ?> % de descuento)</small>
                            </p>
                        </div>
                        <?php echo $planes[1]->descripcion; ?>
                        <div class="panel-footer">
                            <a plan-id="<?php echo $planes[1]->id; ?>"
                               plan-nombre="<?php echo $planes[1]->nombre; ?>"
                               class="btn btn-lg btn-block btn-success seleccion_plan" href="javascript:;">Seleccionar</a>
                        </div>
                    </div>
                </div>
                <!-- /item -->

            </div>
            <div class="col-md-8 col-md-offset-2 col-md-pushed-2">
                <div class="alert alert-success plan_seleccionado" style="display: none;">
                    Plan seleccionado:
                </div>

                <?= $form->field($model, 'plan_id')->hiddenInput()->label(false); ?>

                <p>Antes de registrarse lea los <a href="/registro/terminos" target="_blank">Términos y
                        condiciones</a> y <a href="/registro/aceptacion" target="_blank">Acuerdos de
                        Asociado</a> para aceptar su plan.</p>
                <!-- <div style="height: 200px!important; overflow-y: scroll;" > -->
                <!-- <?php //echo $terminos; ?> -->
                <!-- </div> -->

                <?= $form->field($model, 'terminos_condiciones')->checkbox(['checked' => true]); ?>
                <?= $form->field($model, 'acuerdos_asociado')->checkbox(['checked' => true]); ?>


            </div>
        </section>

        <div class="col-md-8 col-md-offset-2 col-md-pushed-2">
            <button class="btn btn-primary backBtn pull-right ml-15" type="button">Anterior</button>
            <?= Html::submitButton($model->isNewRecord ? 'Registrarse' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary btn-lg center-block' : 'btn btn-primary']) ?>
            <?php echo $form->errorSummary($model, ['class' => 'registro-error-sumary clearfix alert alert-danger']); ?>
        </div>
    </div>
</div>