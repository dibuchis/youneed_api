<?php

use kartik\time\TimePicker; ?>

<div class="row">
    <div class="col-sm-3">
        <label class="control-label">Desde</label>
    </div>
    <div class="col-sm-9">
        <?= TimePicker::widget(array_merge($start_config, ['name' => $start_name])) ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <label class="control-label">Hasta</label>
    </div>
    <div class="col-sm-9">
        <?= TimePicker::widget(array_merge($end_config, ['name' => $end_name])) ?>
    </div>
</div>


