<?php
use yii\helpers\Html;

$webPath = Yii::$app->ManageAssets->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/workCalendarBehavior.js', ['depends' => [yii\web\YiiAsset::className()], 'position' => $this::POS_END]);

$_base_config_time_picker =
    ['options' => [
        'class' => 'small timepicker',
    ],
        'pluginOptions' => [
            'format' => 'hh:ii',
            'autoclose' => true
        ]
    ];

$_base_config_time_picker_no_meridian =
    ['disabled' => true,
        'options' => [
            'class' => 'small timepicker',
        ],
        'pluginOptions' => [
            'format' => 'hh:ii',
            'autoclose' => true,
            'showMeridian' => false,
            'readonly' => true,
        ]
    ];

$_start_time_0 = array_merge(['value' => '08:00 AM'], $_base_config_time_picker);
$_end_time_0 = array_merge(['value' => '01:00 PM'], $_base_config_time_picker);
$_start_time = array_merge(['value' => '00:00'], $_base_config_time_picker_no_meridian);
$_end_time = array_merge(['value' => '23:59',], $_base_config_time_picker_no_meridian);

?>
<table id="work-time" class="table table-responsive table-striped">
    <thead>
    <tr>
        <td></td>
        <td>Día</td>
        <td>Jornada</td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?= Html::checkbox('day[any][enabled]', false, ['id' => 'chkAll',
                'class' => 'check-days check-any-day']) ?></td>
        <td>Cualquier día - 24 horas</td>
        <td>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Desde</label>
                </div>
                <div class="col-sm-9">
                    <label> 00:00 </label>
                    <input class="any-day-input" name="day[any][0][start]" type="hidden" value="00:00" disabled>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Hasta</label>
                </div>
                <div class="col-sm-9">
                    <label> 23:59 </label>
                    <input class="any-day-input" name="day[any][0][end]" type="hidden" value="23:59" disabled>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td><?= Html::checkbox('day[1][enabled]', false, ['id' => 'chkMonday', 'class' => 'check-days check-one-day']) ?></td>
        <td>Lunes</td>
        <td>
            <?= $this->render('_limit_time_range', ['start_name' => 'day[1][0][start]', 'end_name' => 'day[1][0][end]',
                'start_config' => $_start_time_0, 'end_config' => $_end_time_0]) ?>
        </td>
    </tr>
    <tr>
        <td><?= Html::checkbox('day[2][enabled]', false, ['id' => 'chkTuesday', 'class' => 'check-days check-one-day']) ?></td>
        <td>Martes</td>
        <td>
            <?= $this->render('_limit_time_range', ['start_name' => 'day[2][0][start]', 'end_name' => 'day[2][0][end]',
                'start_config' => $_start_time_0, 'end_config' => $_end_time_0]) ?>
        </td>
    </tr>
    <tr>

        <td><?= Html::checkbox('day[3][enabled]', false, ['id' => 'chkWednesday', 'class' => 'check-days check-one-day']) ?></td>
        <td>Miércoles</td>
        <td>
            <?= $this->render('_limit_time_range', ['start_name' => 'day[3][0][start]', 'end_name' => 'day[3][0][end]',
                'start_config' => $_start_time_0, 'end_config' => $_end_time_0]) ?>
        </td>

    </tr>
    <tr>
        <td><?= Html::checkbox('day[4][enabled]', false, ['id' => 'chkThursday', 'class' => 'check-days check-one-day']) ?></td>
        <td>Jueves</td>
        <td>
            <?= $this->render('_limit_time_range', ['start_name' => 'day[4][0][start]', 'end_name' => 'day[4][0][end]',
                'start_config' => $_start_time_0, 'end_config' => $_end_time_0]) ?>
        </td>
    </tr>
    <tr>
        <td><?= Html::checkbox('day[5][enabled]', false, ['id' => 'chkFriday', 'class' => 'check-days check-one-day']) ?></td>
        <td>Viernes</td>
        <td>
            <?= $this->render('_limit_time_range', ['start_name' => 'day[5][0][start]', 'end_name' => 'day[5][0][end]',
                'start_config' => $_start_time_0, 'end_config' => $_end_time_0]) ?>
        </td>
    </tr>
    <tr>

        <td><?= Html::checkbox('day[6][enabled]', false, ['id' => 'chkSaturday', 'class' => 'check-days check-one-day']) ?></td>
        <td>Sábado</td>
        <td>
            <?= $this->render('_limit_time_range', ['start_name' => 'day[6][0][start]', 'end_name' => 'day[6][0][end]',
                'start_config' => $_start_time_0, 'end_config' => $_end_time_0]) ?>
        </td>
    </tr>
    <tr>
        <td><?= Html::checkbox('day[7][enabled]', false, ['id' => 'chkSunday', 'class' => 'check-days check-one-day']) ?></td>
        <td>Domingo</td>
        <td>
            <?= $this->render('_limit_time_range', ['start_name' => 'day[7][0][start]', 'end_name' => 'day[7][0][end]',
                'start_config' => $_start_time_0, 'end_config' => $_end_time_0]) ?>
        </td>
    </tr>
    </tbody>
</table>

