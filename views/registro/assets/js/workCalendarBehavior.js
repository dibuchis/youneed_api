const blocked_ux_color = '#80808038';

$(document).ready(function () {
    $("#work-time .timepicker, #work-time input[type='text']").prop('disabled', true);
    $('#work-time tbody tr .timepicker,#work-time tbody tr label ').css('color', blocked_ux_color);

    //fix the behavior on click in input
    $(".timepicker").click(function () {
        $(this).siblings('.picker').eq(0).click();
    })
});

//enable the row if a check_day is clicked
$(".check-days").change(function () {
    let tr = $(this).parent().parent();
    if ($(this).is(":checked")) {
        $(tr).find('.timepicker, label').css('color', 'black');
        $(tr).find('.timepicker').prop('disabled', false);
    } else {
        $(tr).find('.timepicker, label').css('color', blocked_ux_color);
        $(tr).find('.timepicker').prop('disabled', true);
    }
});

//change the checked days if 24-dias check es enabled
$(".check-any-day").eq(0).click(function () {
    let hidden_inputs = $('.any-day-input');
    if ($(this).is(':checked')) {
        $(hidden_inputs).prop('disabled', false);
        $('.check-one-day').each(function (index, element) {
            if ($(element).is(':checked')) {
                $(element).click();
            }
        });
    } else {
        $(hidden_inputs).prop('disabled', true);
        $('.check-one-day').each(function () {
            $(this).click();
        })
    }
});

//Disable the any day checkbox if a checkbox of weekdays is checked
$('.check-one-day').click(function () {
    if ($(this).is(':checked')) {
        let hidden_inputs = $('.any-day-input');
        $('.check-any-day').eq(0).prop('checked', false);
        $(hidden_inputs).prop('disabled', true);
        $(hidden_inputs).parent().parent().parent().find('label').css('color', blocked_ux_color);
    }
});


$('form #work-time input').change(function () {
    let inputs = $('form #work-time input').not('[name="hour"]').not('[name="minute"]').not('[name="meridian"]');
    let val = JSON.stringify(inputs.serializeArray())==="[]"?'':JSON.stringify(inputs.serializeArray());
    $('form input[name="Usuarios[jornada_trabajo]"]').val(val);
});