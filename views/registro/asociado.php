<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Servicios;
use app\models\Planes;
use app\models\TiposDocumentos;
use yii\helpers\Url;
use kartik\widgets\DepDrop;
use dosamigos\fileupload\FileUpload;
use borales\extensions\phoneInput\PhoneInput;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>-->

<span id="test"></span>
<?php $form = ActiveForm::begin([
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'id' => 'asociado-register-form'] // important
]); ?>

<!-- <script>
Swal.fire({
    type:"info",
    text:"Antes de continuar asegúrate de disponer en tu teléfono o PC una imagen actualizada de tu perfil o logo si es una empresa, imagen de tu Cédula o RUC o RISE o Pasaporte con permiso de Trabajo, dependiendo del tipo de documento que necesites ingresar"
});
</script> -->

<div class="container" id="form-registro-asociado">
    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
             <div class="stepwizard-step col-xs-3">
                 <a href="#step-1" id="btn-step-1" type="button" data-step="1" class="btn btn-success btn-circle btn-step">1</a>
                 <p><small>Información personal</small></p>
             </div>
            <div class="stepwizard-step col-xs-3">
                <a href="#step-2" id="btn-step-2" type="button" data-step="2"
                   class="btn btn-default btn-circle btn-step">2</a>
                <p><small>Servicios</small></p>
            </div>
            <div class="stepwizard-step col-xs-3">
                <a href="#step-3" id="btn-step-3" type="button" data-step="3"
                   class="btn btn-default btn-circle btn-step">3</a>
                <p><small>Información para pagos</small></p>
            </div>
            <div class="stepwizard-step col-xs-3">
                <a href="#step-4" id="btn-step-4" type="button" data-step="4"
                   class="btn btn-default btn-circle btn-step">4</a>
                <p><small>Planes</small></p>
            </div>
        </div>
    </div>

    <form role="form" autocomplete="off">
        <?= $this->render('_partials/steps/step1', ['form' => $form, 'model' => $model])?>
        <?= $this->render('_partials/steps/step2',['form' => $form, 'model' => $model]) ?>
        <?= $this->render('_partials/steps/step3',['form' => $form, 'model' => $model]) ?>
        <?= $this->render('_partials/steps/step4',['form' => $form, 'model' => $model]) ?>
    </form>
</div>
<style>
    /*.help-block{
      display:none;
    }
    .has-error .form-control{
    border:1px solid #ccc;
    }
    .has-error .help-block, .has-error .control-label, .has-error .radio, .has-error .checkbox, .has-error .radio-inline, .has-error .checkbox-inline, .has-error.radio label, .has-error.checkbox label, .has-error.radio-inline label, .has-error.checkbox-inline label{
    color:#7f7f82;
    }*/
    .panel-pricing h3 {
        padding: 0px 15px !important;
        margin-top: 20px !important;
        line-height: 1em;
        font-size: 28px;
    }

    .panel-pricing h3 span {
        display: inline-block;
        font-size: 18px;
        line-height: 18px !important;
        margin-bottom: 10px;
        font-weight: 600;
    }

    .panel-pricing > p {
        padding: 20px 13px 10px !important;
        line-height: 1.4em;
    }

    .panel-pricing ul li {
        font-size: 12px;
        font-family: 'Montserrat', sans-serif !important;
        color: #7f7f82;
        font-weight: 600 !important;
        line-height: 12px;
        padding: 15px 30px 15px 40px !important;
        min-height: 50px !important;
    }
</style>
<?php ActiveForm::end(); ?>
