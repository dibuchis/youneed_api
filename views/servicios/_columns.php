<?php

use app\models\Categorias;
use app\models\Medidas;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\base\MedidasBase;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'id',
    ],
    [
        'attribute' => 'categoria_id',
        'value' => function ($model) {
            return $model->categoriasServicios->categoria->nombre;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(Categorias::find()->all(), 'id', 'nombre'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Seleccione...',],
        ],
    ],
    [
        'attribute' => 'imagen',
        'value' => function ($model, $key, $index, $widget) {
            return '<img width="120px;" src="' . $model->imagen . '">';
        },
        'format' => 'raw'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nombre',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'incluye',
        'value' => function ($model, $key, $index, $widget) {
            return strip_tags($model->incluye);
        },
        'format' => 'raw'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'no_incluye',
        'value' => function ($model, $key, $index, $widget) {
            return strip_tags($model->no_incluye);
        },
        'format' => 'raw'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'subtotal',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'iva',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'total',
    ],
    [

        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'proveedor_subtotal',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'proveedor_iva',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'proveedor_total',
    ],
    [
        'attribute' => 'aplica_iva',
        'value' => function ($model) {
            return $model->aplica_iva == 1 ? 'SI' : 'NO';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [0=>'NO',1=>'SI'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Seleccione...',],
        ],
    ],
    [
        'attribute' => 'obligatorio_certificado',
        'value' => function ($model) {
            return $model->obligatorio_certificado == 1 ? 'SI' : 'NO';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [0=>'NO',1=>'SI'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Seleccione...',],
        ],
    ],
    [
        'attribute' => 'medida_id',
        'value' => function ($model) {
            return isset($model->medidas)?$model->medidas->descripcion:null;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' =>  ArrayHelper::map(Medidas::find()->all(), 'id', 'descripcion'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Seleccione...',],
        ],
    ],
    [
        'attribute' => 'tiene_soft_delete',
        'value' => function ($model) {
            return ($model->tiene_soft_delete)?'SI':'NO';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' =>  ['0'=>'NO','1'=>'SI'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'placeholder' => 'Seleccione...',],
        ],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Delete',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'],
        'template' => '{view} {update} {delete} {asociados} {soft_delete}',
        'buttons' => [
            'asociados' => function ($url, $model, $key) {
                return Html::a(' <span class="glyphicon glyphicon-list-alt"></span>', 'javascript:void(0)',
                    ['class' => 'btn btn-black modal-servicios', 'title' => 'Asociados', 'data-servicio_id'=>$model->id,
                        'data-url'=>Url::toRoute(['usuarios/asociados', 'UsuariosSearch[servicio_id]' => $model->id]),
                        'data-title'=>'Asociados segun servicio '.$model->nombre,
                        'data-target_modal_name'=>'usuariosServiciosIndexModal',
                        'onclick'=>'callAuxModalIndex(this)']) ;
            },
            'soft_delete' => function ($url, $model, $key) {
                if($model->tiene_soft_delete==0){
                    return Html::a(' <span class="glyphicon glyphicon-ban-circle"></span>', 'javascript:void(0)',
                        ['class' => 'btn btn-black', 'title' => 'Dar de baja',
                            'data-model_id'=>$model->id,
                            'data-status_soft_delete'=>1,
                            'data-url'=>Url::toRoute(['servicios/dar-de-baja']),
                            'onclick'=>'darDeBaja(this)']) ;
                }else{
                    return Html::a(' <span class="glyphicon glyphicon-check"></span>', 'javascript:void(0)',
                        ['class' => 'btn btn-black', 'title' => 'Dar de alta',
                            'data-model_id'=>$model->id,
                            'data-status_soft_delete'=>0,
                            'data-url'=>Url::toRoute(['servicios/dar-de-baja']),
                            'onclick'=>'darDeBaja(this)']) ;
                }
            }
        ]
    ],

];   