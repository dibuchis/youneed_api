<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'attribute'=>'imagen',
        'value'=>function ($model, $key, $index, $widget) {
            return '<img width="120px;" src="'.$model->imagen.'">';
        },
      'format'=>'raw'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'nombre',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'descripcion',
        'value'=>function ($model, $key, $index, $widget) {
            return strip_tags($model->descripcion);
        },
        'format'=>'raw'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fecha_creacion',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'],
        'template' => '{view} {update} {delete} {servicio}',
        'buttons' => [
            'servicio' => function ($url, $model, $key) {
                return Html::a(' <span class="glyphicon glyphicon-list-alt"></span>', 'javascript:void(0)',
                    ['class' => 'btn btn-black modal-categoria', 'title' => 'Servicios', 'data-categoria_id'=>$model->id,
                        'data-url'=>Url::toRoute(['servicios/index', 'ServiciosSearch[categoria_id]' => $model->id]),
                        'data-title'=>'Servicios de la categoria '.$model->nombre,
                        'data-target_modal_name'=>'usuariosCategoriasModal',
                        'onclick'=>'callAuxModalIndex(this)']) ;
            }
        ]
    ],

];   