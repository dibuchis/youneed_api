<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Medidas */
?>
<div class="medidas-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
