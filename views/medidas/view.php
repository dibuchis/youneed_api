<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Medidas */
?>
<div class="medidas-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'descripcion',
            'sufijo',
        ],
    ]) ?>

</div>
