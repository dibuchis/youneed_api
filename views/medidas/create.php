<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Medidas */

?>
<div class="medidas-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
