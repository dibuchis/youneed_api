<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'id',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'identificacion',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nombres',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'apellidos',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'email',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'numero_celular',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'telefono_domicilio',
    ],
    [
        'attribute' => 'estado',
        'filter' => false,
        'value' => function ($model, $key, $index, $widget) {
            return Yii::$app->params['parametros_globales']['estados'][$model->estado];
        },
        'filter' => array_merge(['' => 'Todos'], Yii::$app->params['parametros_globales']['estados']),
        'format' => 'raw'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fecha_creacion',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Delete',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'],
        'template' => '{view} {update} {delete} {pedidos}',
        'buttons' => [
            'pedidos' => function ($url, $model, $key) {
               return Html::a(' <span class="glyphicon glyphicon-tasks"></span>', 'javascript:void(0)',
                   ['class' => 'btn btn-black modal-pedidos_client', 'title' => 'Pedidos', 'data-client_id'=>$model->id,
                       'data-url'=>Url::toRoute(['pedidos/index', 'PedidosSearch[cliente_id]' => $model->id]),
                       'data-title'=>'Pedidos segun el cliente',
                       'data-target_modal_name'=>'clientesPedidosModal',
                       'onclick'=>'callAuxModalIndex(this)']) ;
            }
        ]

    ],

];   