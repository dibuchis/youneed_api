<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */

?>
<div class="usuarios-asociados-create">
    <?= $this->render('_form-asociados', [
        'model' => $model,
    ]) ?>
</div>
