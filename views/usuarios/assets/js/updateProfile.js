/**
 * Muestra el documento bas64 en un iframe
 * @param element
 */
function viewDoc(element) {
    let doc = $(element).data('doc');
    let iframe = "<iframe src='" + doc + "' style='width: 100%;height: 68vh;'></iframe>";
    $('#modal_documentos .modal-body').append(iframe);
    $('#modal_documentos').modal('show');
}

/**
 * Permite cerrar solo el modal del visualizador del documento
 * @param element
 */
function closeModalViewDoc(element) {
    $('#modal_documentos').modal('hide');
    setTimeout(() => $('#modal_documentos .modal-body').children().remove(), 500);
}

