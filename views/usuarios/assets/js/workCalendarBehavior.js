//llena la info de la jornada de trabajo del usuario

$("body").off("click", 'a[href="#infoservicios"]');
$('body').on('click','a[href="#infoservicios"]',function(){
    setTimeout(() => {
        $('.timepicker, .check-days').prop('disabled',true)
        if (data_journal !== {} && data_journal !== undefined) {
            $.each(data_journal, function (key, value) {
                if (key != 'any') {
                    $('#work-time input[name="day[' + key + '][0][start]"]').eq(0).val(value[0]['start']);
                    $('#work-time input[name="day[' + key + '][0][end]"]').eq(0).val(value[0]['end'] );
                }
            });
        }
    }, 500);
});
