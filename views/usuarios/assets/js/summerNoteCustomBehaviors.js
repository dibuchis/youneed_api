var observation_ok = 'withErrors';

$("body").off("change", "input[name='aux_error_observation_list[]']");
$("body").off("change", "input[name='aux_success_observation_list[]']");
$("body").off("change", "input[name='aux_error_layout_observation_list[]']");


//Evento para la lista de observaciones   de errores
$("body").on("change", "input[name='aux_error_observation_list[]']", function () {
    if ($(this).is(':checked')) {
        observation_ok = 'withErrors';
        createErrorItem($(this).data('label'), $(this).data('id'));
    }
});

//Evento para la lista de mensajes de exito
$("body").on("change", "input[name='aux_success_observation_list[]']", function () {
    cleanSummerNote();
    observation_ok = 'withoutErrors';
    if ($(this).is(':checked')) {
        observation_ok = 'withoutErrors';
        $('#aux_observation').append('<p>' + $(this).data('label') + '</p>');
        writeInSummerNote();
    }
});

//evento para la el layout de observaciones
$("body").on("change", "input[name='aux_error_layout_observation_list[]']", function () {
    if ($(this).is(':checked')) {
        observation_ok = 'withErrors';
        let layout = $(this).data('label').split("^");
        createLayout(layout);
    }
});

function createErrorItem(label_in, id_in) {
    mergeIntoObservaciones("<li class='message-observation' data-context='" + id_in + "'>" + label_in + "</li>");
}

function mergeIntoObservaciones(item) {
    let x = getValueOfSummerNote();
    $('#aux_observation').append($.parseHTML(x));
    if ($('#aux_observation').find('ul#observations-list').length == 0) {
        $('#aux_observation').append('<ul id="observations-list">' + item + '</ul>');
    } else {
        $('#aux_observation').find('ul#observations-list').append(item);
    }
    writeInSummerNote();
}

function createLayout(layout) {
    let old = getValueOfSummerNote();
    $('#aux_observation').append($.parseHTML(old));

    let header = layout[0];
    let footer = layout[1];

    let headerhtml = '<div id="header-observation"><p>' + header + ' </p></div>';
    let footerhtml = '<br><div id="footer-observation"><p>' + footer + ' </p></div>';
    let bodyHtml = '';

    if ($('#aux_observation').find('ul#observations-list').length != 0) {
        bodyHtml = headerhtml + $('#aux_observation').find('ul#observations-list').html() + footerhtml;
    } else {
        bodyHtml = headerhtml + $('#aux_observation').append('<ul id="observations-list"></ul>').html() + footerhtml;
    }

    $('#aux_observation').children().remove();
    $('#aux_observation').append(bodyHtml);
    writeInSummerNote();
}

function getValueOfSummerNote() {
    $('#aux_observation').children().remove();
    let x = $('#usuarios-observaciones').summernote('code');
    return x;
}

function writeInSummerNote() {
    $('#usuarios-observaciones').summernote('code', $('#aux_observation').children());
}


function cleanSummerNote() {
    $('#aux_observation').children().remove();
    writeInSummerNote();
}

function sendEmail() {
    let emailTo = $("input[name='Usuarios[email]']").val();
    let body = $('#usuarios-observaciones').summernote('code');
    let type = observation_ok;
    if (body == "<p><br></p>") {
        alert('No tiene observaciones para enviar');
    } else {
        $.post("ajax-send-email", {emailTo: emailTo, body: body, type: type}, function (data) {
            alert(data.message)
        });
    }

}