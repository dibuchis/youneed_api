<?php

use app\common\models\constants\DocumentObservation;
use app\models\Bancos;
use app\models\Ciudades;
use app\models\Documentos;
use app\models\Paises;
use app\models\Planes;
use kartik\widgets\Select2;
use marqu3s\summernote\Summernote;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\redactor\widgets\Redactor;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\DepDrop;
use borales\extensions\phoneInput\PhoneInput;

$webPath = Yii::$app->ManageAssets->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/updateProfile.js', ['depends' => [yii\web\YiiAsset::className()], 'position' => $this::POS_END]);


/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */
/* @var $form yii\widgets\ActiveForm */
?>
    <script>
        var data_journal = JSON.parse('<?= empty($model->jornada_trabajo) ? '{}' : $model->jornada_trabajo  ?>');
    </script>
    <div class="usuarios-form">

        <?php $form = ActiveForm::begin([
            'enableClientValidation' => true,
            'enableAjaxValidation' => true,
            ]); ?>

        <?php if ($model->es_asociado == 1) { ?>
            <?= $model->es_cliente ? $form->field($model, 'es_cliente')->hiddenInput(['readonly' => 'readonly']) : '' ?>
            <?= $model->es_asociado ? $form->field($model, 'es_asociado')->hiddenInput(['readonly' => 'readonly']) : '' ?>
            <?= $model->es_operador ? $form->field($model, 'es_operador')->hiddenInput(['readonly' => 'readonly']) : '' ?>
            <?= $model->es_super ? $form->field($model, 'es_super')->hiddenInput(['readonly' => 'readonly']) : '' ?>

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#infogeneral">Información general</a></li>

                <li><a data-toggle="tab" href="#infoservicios">Servicios</a></li>
                <li><a data-toggle="tab" href="#infopagos">Pagos</a></li>
                <li><a data-toggle="tab" href="#infodocumentos">Documentos</a></li>
                <li><a data-toggle="tab" href="#infoconfirmaciones">Cuenta</a></li>

            </ul>
            <div class="tab-content">
                <div id="infogeneral" class="tab-pane fade in active">

                    <div class="row">

                        <div class="col-md-4">
                            <img src="<?= $model->imagen ?>" class="img-responsive">
                        </div>

                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Datos Personales: </h4>
                                </div>
                                <div class="col-md-12">
                                    <?= $form->field($model, 'tipo_identificacion')->dropDownList([1 => 'Cédula', 2 => 'RUC', 3 => 'RISE', 4 => 'Pasaporte'], ['prompt' => 'Seleccione']) ?>
                                </div> <div class="col-md-12">
                                    <?= $form->field($model, 'identificacion')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $form->field($model, 'nombres')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'numero_celular')->textInput(['maxlength' => true]) ?>
                                </div>

                                <div class="col-md-6">
                                    <?= $form->field($model, 'telefono_domicilio')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <?= $form->field($model, 'pais_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Paises::find()->orderBy('nombre')->asArray()->all(), 'id',
                                    function ($model, $defaultValue) {
                                        return $model['nombre'];
                                    }
                                ),
                                'options' => ['placeholder' => Yii::t('app', 'Seleccione una empresa'), 'id' => 'cat-id'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]); ?>

                        </div>
                        <div class="col-md-6">

                            <?php
                            $data_adicionales = Ciudades::find()->andWhere(['pais_id' => $model->pais_id])->all();
                            $array_adicionales = [];
                            foreach ($data_adicionales as $adis) {
                                $array_adicionales [$adis->id] = $adis->nombre;
                            }
                            // $array_filtros = array();
                            // if ($model->id > 0) {
                            //     $productos = AdicionalesProductos::find()->where(' producto_id = '.$model->id)->all();
                            //     foreach ($productos as $p) {
                            //         $array_filtros[ $p->adicional_id ] = [ 'selected' => true ];
                            //     }
                            // }
                            ?>

                            <?php
                            // $lista_empresas = \yii\helpers\ArrayHelper::map(\app\models\Empresas::find()->orderBy('nombre_comercial')->asArray()->all(), 'id',
                            //         function($model, $defaultValue) {
                            //             return $model['ruc'].'-'.$model['nombre_comercial'];
                            //         }
                            // );
                            // echo $form->field($model, 'empresa_id')->dropDownList($lista_empresas, ['id'=>'cat-id']);
                            echo Html::hiddenInput('input-type-1', 'Additional value 1', ['id' => 'input-type-1']);
                            echo Html::hiddenInput('input-type-2', 'Additional value 2', ['id' => 'input-type-2']);

                            echo $form->field($model, 'ciudad_id')->widget(DepDrop::classname(), [
                                'type' => DepDrop::TYPE_SELECT2,
                                'data' => $array_adicionales,
                                'options' => ['id' => 'subcat1-id', 'placeholder' => 'Seleccione'],
                                'select2Options' => ['pluginOptions' => ['multiple' => false, 'allowClear' => true]],
                                'pluginOptions' => [
                                    'depends' => ['cat-id'],
                                    'url' => Url::to(['/ajax/ciudades']),
                                    'params' => ['input-type-1', 'input-type-2'],
                                ]
                            ]);
                            ?>

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Datos de la cuenta: </h4>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'clave')->passwordInput(['maxlength' => true]) ?>
                        </div>

                        <!--<div class="col-md-12">

                        </div>-->

                        <!--<div class="col-md-12">

                        </div>-->

                        <div class="col-md-4">
                            <?= $form->field($model, 'fecha_creacion')->textInput(['maxlength' => true, 'readonly' => 'readonly']) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'fecha_activacion')->textInput(['maxlength' => true, 'readonly' => 'readonly']) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'fecha_desactivacion')->textInput(['maxlength' => true, 'readonly' => 'readonly']) ?>
                        </div>

                        <!--<div class="col-md-12">-->

                        <!--</div>-->
                    </div>


                </div>

                <div id="infoservicios" class="tab-pane fade">

                    <?php
                    $categorias = $model->usuariosCategorias;
                    $servicios = $model->usuariosServicios;
                    ?>

                    <label>Categorias (varios)</label><br>
                    <span>
                            <ul>
                            <?php foreach ($categorias as $categoria) {
                                echo "<li>" . $categoria->categoria->nombre . '</li>';
                            } ?>
                            </ul>
                        </span>
                    <p>&nbsp;</p>
                    <label>Servicios (varios)</label><br>
                    <span>
                            <ul>
                            <?php foreach ($servicios as $servicio) { ?>
                                <?= "<li>" ?>
                                <div class="row-servicios"
                                     style="display: flex; justify-content: space-between;border-bottom: 1px solid lightgray;padding-bottom: 5px;">
                                    <div>
                                        <?= $servicio->servicio->nombre . ($servicio->servicio->obligatorio_certificado == 1 ? " *" : "") ?>
                                    </div>
                                    <div>
                                         <?php if (isset($servicio->documento_id)) {
                                             $documento = Documentos::findOne($servicio->documento_id);

                                             echo Html::tag('input', '',
                                                 ['class' => 'btn btn-success btn-sm', 'value' => 'Ver Documento',
                                                     'data-doc' => $documento->imagen, 'onclick' => 'viewDoc(this)']);
                                         } ?>
                                    </div>
                                </div>
                                <?= "</li>" ?>
                            <?php } ?>
                            </ul>
                        </span>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'plan_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Planes::find()->orderBy('nombre')->asArray()->all(), 'id', function ($model, $defaultValue) {
                                    return $model['nombre'];
                                }
                                ),
                                'options' => ['placeholder' => Yii::t('app', 'Seleccione')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'jornada_trabajo')->hiddenInput() ?>
                            <?= $this->renderAjax('_partials/components/_workCalendar') ?>
                        </div>
                    </div>


                </div>

                <div id="infopagos" class="tab-pane fade">
                    <?= $form->field($model, 'banco_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Bancos::find()->orderBy('nombre')->asArray()->all(), 'id',
                            function ($model, $defaultValue) {
                                return $model['nombre'];
                            }
                        ),
                        'options' => ['placeholder' => 'Seleccione'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'multiple' => false,
                        ],
                    ]); ?>

                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'nombre_beneficiario')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'tipo_cuenta')->dropDownList(['Corriente' => 'Corriente', 'Ahorros' => 'Ahorros',]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'numero_cuenta')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'tipo_ruc')->dropDownList(Yii::$app->params['tipo_ruc']) ?>
                        </div>
                    </div>


                </div>

                <div id="infodocumentos" class="tab-pane fade">

                    <?php if (!empty($model->fotografia_cedula)) { ?>
                        <?= $form->field($model, 'fotografia_cedula')->hiddenInput(); ?>
                        <a data-fancybox="gallery" target="_blank" class="btn-primary btn"
                           href="<?php echo $model->fotografia_cedula ?>">Ver documento subido</a>
                        <hr>
                    <?php } ?>

                    <?php if (!empty($model->ruc)) { ?>
                        <?= $form->field($model, 'ruc')->hiddenInput(); ?>
                        <a data-fancybox="gallery" target="_blank" class="btn-primary btn"
                           href="<?php echo $model->ruc ?>">Ver
                            documento subido</a>
                        <hr>
                    <?php } ?>

                    <?php if (!empty($model->visa_trabajo)) { ?>
                        <?= $form->field($model, 'visa_trabajo')->hiddenInput(); ?>
                        <a data-fancybox="gallery" target="_blank" class="btn-primary btn"
                           href="<?php echo $model->visa_trabajo ?>">Ver documento subido</a>
                        <hr>
                    <?php } ?>

                    <?php if (!empty($model->rise)) { ?>
                        <?= $form->field($model, 'rise')->hiddenInput(); ?>
                        <a data-fancybox="gallery" target="_blank" class="btn-primary btn"
                           href="<?php echo $model->rise ?>">Ver documento subido</a>
                        <hr>
                    <?php } ?>

                    <?php if (!empty($model->referencias_personales)) { ?>
                        <?= $form->field($model, 'referencias_personales')->hiddenInput(); ?>
                        <a data-fancybox="gallery" target="_blank" class="btn-primary btn"
                           href="<?php echo $model->referencias_personales ?>">Ver documento subido</a>
                        <hr>
                    <?php } ?>

                    <?php if (!empty($model->titulo_academico)) { ?>
                        <?= $form->field($model, 'titulo_academico')->hiddenInput(); ?>
                        <a data-fancybox="gallery" target="_blank" class="btn-primary btn"
                           href="<?php echo $model->titulo_academico ?>">Ver documento subido</a>
                        <hr>
                    <?php } ?>


                </div>
                <div id="infoconfirmaciones" class="tab-pane fade">

                    <?php if ($model->estado_validacion_documentos == 0) { ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Activación de usuarios</div>
                                    <div class="panel-body">
                                        <?= $form->field($model, 'causas_desactivacion')->widget(Redactor::className()) ?>
                                        <?= $form->field($model, 'estado')->dropDownList(Yii::$app->params['estados_genericos'], ['prompt' => 'Seleccione']) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 border border-secondary">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Validación de documentos</div>
                                    <div class="panel-body">
                                        <?= $this->render('_partials/form_asociados/_lista_observaciones')?>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <?= $form->field($model, 'observaciones')
                                                    ->widget(Summernote::className(), [
                                                        'clientOptions' => [
                                                            'id' => 'summernote'
                                                        ]
                                                    ]) ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <button type="button" id="btn_notificar"
                                                    class="btn btn-secondary btn-lg btn-block">
                                                Notificar al asociado las observaciones
                                            </button>
                                        </div>


                                        <?= $form->field($model, 'estado_validacion_documentos')->dropDownList(Yii::$app->params['parametros_globales']['estados_condiciones'], ['prompt' => 'Seleccione']) ?>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Bloqueos y Notificaciones</div>
                                    <div class="panel-body">
                                        <a class="btn-danger btn" id="bloquear-usuario"
                                           data-id="<?php echo $model->ID ?>"
                                           href="#">Bloquear Usuario</a>

                                    </div>
                                </div>

                            </div>
                        </div>


                    <?php} if ($model->estado_validacion_documentos == 1) { ?>
                        <a class="btn-danger btn" id="cancelar-cuenta" data-id="<?php echo $model->ID ?>" href="#">Cancelar
                            Cuenta</a>
                    <?php } ?>
                </div>
            </div>
            <?php if (!Yii::$app->request->isAjax) { ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            <?php } ?>
        <?php } ?>


        <?php ActiveForm::end(); ?>

    </div>

<?= $this->render('_partials/_modal_documentos') ?>