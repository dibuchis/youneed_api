<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */
?>
<div class="usuarios-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            [
                'attribute'=>'tipo_identificacion',
                'value'=>function ($model) {
                    $texto = '';
                    if( $model->tipo_identificacion == 1 ){
                        $texto = 'Cédula';
                    }
					if( $model->tipo_identificacion == 2 ){
                        $texto = 'RUC';
                    }
					if( $model->tipo_identificacion == 3 ){
                        $texto = 'Rise';
                    }
					if( $model->tipo_identificacion == 4 ){
                        $texto = 'Pasaporte';
                    }
                    return $texto;

                }
            ],
            'identificacion',
            [
                'attribute'=>'imagen',
                'value'=>function ($model) {
                    if( !is_null( $model->imagen ) ){
                        return '<img width="200px" src="'.$model->imagen.'">';   
                    }
                },
                'format'=>['raw'],
            ],
            'nombres',
            'apellidos',
            'email:email',
            'numero_celular',
            'telefono_domicilio',
            [
                'attribute'=>'estado',
                'value'=>function ($model) {
                     $color = 'default';
                    $texto = '';
                    if( $model->estado == 1 ){
                        $color = 'success';
                        $texto = 'Activo';
                    }elseif( $model->estado == 0 ){
                        $color = 'warning';
                        $texto = 'Inactivo';
                    }else{
                        $color = 'danger';
                        $texto = 'Sin estado';
                    }
                    return '<span class="btn-block btn-xs btn-'.$color.'"><center>'.$texto.'</center></span>';

                },
                'format'=>['raw'],
            ],
            [
                'attribute'=>'habilitar_rastreo',
                'value'=>function ($model) {
                     $color = 'default';
                    $texto = '';
                    if( $model->estado == 1 ){
                        $color = 'success';
                        $texto = 'Permite rastreo';
                    }elseif( $model->estado == 0 ){
                        $color = 'warning';
                        $texto = 'No permite rastreo';
                    }else{
                        $color = 'danger';
                        $texto = 'No permite rastreo';
                    }
                    return '<span class="btn-block btn-xs btn-'.$color.'"><center>'.$texto.'</center></span>';

                },
                'format'=>['raw'],
            ],
            ['label'=>'Ciudad', 'value'=>isset($model->ciudad)?$model->ciudad->nombre:'SIN INFO'],
            'fecha_creacion',
        ],
    ]) ?>


    <?php if( $model->es_asociado == 1 ){ ?>
        <h2>Información de asociado</h2>
        <?= DetailView::widget([
        'model' => $model,
            'attributes' => [
                'activacion',
                'fecha_desactivacion',
                'causas_desactivacion:ntext',
                ['label'=>'Plan', 'value'=>isset($model->plan)?$model->plan->nombre:'NINGUNA'],
                'fecha_cambio_plan',
                ['label'=>'Banco', 'value'=>isset($model->banco)?$model->banco->nombre:'NINGUNA'],
                // 'tipo_cuenta',
                'numero_cuenta',
				/*[
			        'label'=>'Preferencia de depósito',
					'value'=> function($model){
						$depositos = [1=>'Lunes a viernes', 2=>'Fines de semana', 3=>'Cualquier día'];
						$deposito = $depositos[$model->preferencias_deposito];
						return $deposito;
					},
					// 'filterType'=>GridView::FILTER_SELECT2,
				],*/
				/*[
			        'label'=>'Días de trabajo',
					'value'=> function($model){
						$_dias = [1=>'Lunes a viernes', 2=>'Fines de semana', 3=>'Cualquier día'];
						$dias = $_dias[$model->dias_trabajo];
						return $dias;
					},
					// 'filterType'=>GridView::FILTER_SELECT2,
				],*/
				/*[
			        'label'=>'Jornada de trabajo',
					'value'=> function($model){
						$_horarios = [1=>'7am a 12am', 2=>'12am a 7pm', 3=>'7pm a 7am', 4=>'24 horas'];
						$horarios = $_horarios[$model->horarios_trabajo];
						return $horarios;
					},
					// 'filterType'=>GridView::FILTER_SELECT2,
				],*/
                // 'dias_trabajo',
                // 'horarios_trabajo',
                'estado_validacion_documentos',
                'observaciones:ntext',
            ],
        ]) ?>
    <?php } ?>

</div>
