<?php

use app\models\Usuarios;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    /* [
         'class' => '\kartik\grid\DataColumn',
         'attribute' => 'id',
     ],*/
    [
        'attribute' => 'multiple_id',
        'value' => function ($model) {
            return $model->id;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(Usuarios::find()->where('es_asociado=1')->asArray()->all(), 'id', 'id'),
        'filterWidgetOptions' => ['pluginOptions' => ['multiple' => true]],
        //TODO: enableAjax
        /* 'filterWidgetOptions' => [
             'pluginOptions' => [
                 'placeholder' => 'Escriba...',
                 //'minimumInputLength' => 2,
                 'allowClear' => true,
                 'tokenSeparators' => [',', ' '],
                 'multiple'=>true,
                 'tags' => true,
                 'ajax' => [
                     'url' => Url::to(['usuarios/ajax-get-usuarios']),
                     'dataType' => 'json',
                     'data' => new JsExpression('function(params) { return {q:params.term, tipo:"ASOCIADO", targetField:"multiple_id"}; }')
                 ],
                 'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                 'templateResult' => new JsExpression('function(data) { return data.id; }'),
                 'templateSelection' => new JsExpression('function (data) {  return data.id; }'),

             ],
         ],*/
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'identificacion',
    ],
    [
        'attribute' => 'id',
        'label' => Yii::t('app', 'Nombre Completo'),
        'value' => function ($model) {
            return $model->getNombreCompleto();
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'placeholder' => 'Escriba...',
                'minimumInputLength' => 2,
                'allowClear' => true,
                'ajax' => [
                    'url' => Url::to(['usuarios/ajax-get-usuarios']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term, tipo:"ASOCIADO", targetField:"nombres"}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(data) { return data.nombreCompleto; }'),
                'templateSelection' => new JsExpression('function (data) {  return data.text; }'),

            ],
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'email',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'numero_celular',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'telefono_domicilio',
    ],
    [
        'attribute' => 'estado',
        'value' => function ($model, $key, $index, $widget) {
            return Yii::$app->params['parametros_globales']['estados'][$model->estado];
        },
        'filter' => array_merge(['' => 'Todos'], Yii::$app->params['parametros_globales']['estados']),
        'format' => 'raw'
    ],
    [
        'attribute' => 'estado_validacion_documentos',
        'value' => function ($model, $key, $index, $widget) {
            return Yii::$app->params['parametros_globales']['estados_condiciones'][$model->estado_validacion_documentos];
        },
        'filter' => array_merge(['' => 'Todos'], Yii::$app->params['parametros_globales']['estados_condiciones']),
        'format' => 'raw'
    ],
    [
        'attribute' => 'ubicacion_name',
        'value' => function ($model, $key, $index, $widget) {
            if ($model->pais && $model->ciudad) {
                return $model->pais->nombre . ' / ' . $model->ciudad->nombre;
            } else {
                return '';
            }
        },
        'format' => 'raw'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fecha_creacion',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'activacion',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fecha_desactivacion',
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Delete',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'],
        'template' => '{view} {update} {delete} {clientes}',
        'buttons' => [
            'clientes' => function ($url, $model, $key) {
                /*  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>*/
                //glyphicon glyphicon-tasks
                return Html::a(' <span class="glyphicon glyphicon-list-alt"></span>', 'javascript:void(0)',
                    ['class' => 'btn btn-black modal-pedidos_client', 'title' => 'Pedidos', 'data-asociado_id' => $model->id,
                        'data-url' => Url::toRoute(['pedidos/index', 'PedidosSearch[asociado_id]' => $model->id]),
                        'data-title' => 'Pedidos segun el asociado',
                        'data-target_modal_name' => 'asociadosPedidosModal',
                        'onclick' => 'callAuxModalIndex(this)']);
            }
        ]
    ],

];   