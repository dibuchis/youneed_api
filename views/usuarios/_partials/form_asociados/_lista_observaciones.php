<?php

use app\common\models\constants\DocumentObservation;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$webPath = Yii::$app->ManageAssets->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/summerNoteCustomBehaviors.js', ['depends' => [yii\web\YiiAsset::className()], 'position' => $this::POS_END]);


?>
<div id="aux_observation" style="display: none"></div>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion"
                   href="#estructuraMensajes" aria-expanded="true" aria-controls="estructuraMensajes">
                    Layout mensajes
                </a>
            </h4>
        </div>
        <div id="estructuraMensajes" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <?= Html::checkboxList('aux_error_layout_observation_list', [],
                            ArrayHelper::map(DocumentObservation::getLayoutErrorList(), 'id', 'value'),
                            ['item' => function ($index, $label, $name, $checked, $value) {
                                return Html::checkbox($name, $checked, [
                                    'value' => $value,
                                    'label' => $label,
                                    'data-label' => $label,
                                    'data-id' => $value,
                                ]);
                            }])
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion"
                   href="#listaErrores" aria-expanded="true" aria-controls="listaErrores">
                    Lista de Errores
                </a>
            </h4>
        </div>
        <div id="listaErrores" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <?= Html::checkboxList('aux_error_observation_list', [],
                            ArrayHelper::map(DocumentObservation::getErrorList(), 'id', 'value'),
                            ['item' => function ($index, $label, $name, $checked, $value) {
                                return Html::checkbox($name, $checked, [
                                    'value' => $value,
                                    'label' => $label,
                                    'data-label' => $label,
                                    'data-id' => $value,
                                ]);
                            }])
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse"
                   data-parent="#accordion" href="#noerrordocs" aria-expanded="false"
                   aria-controls="noerrordocs">
                    Sin errores en la documentacion
                </a>
            </h4>
        </div>
        <div id="noerrordocs" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <?= Html::checkboxList('aux_success_observation_list', [],
                            ArrayHelper::map(DocumentObservation::getSuccessMessage(), 'id', 'value'),
                            ['item' => function ($index, $label, $name, $checked, $value) {
                                return Html::checkbox($name, $checked, [
                                    'value' => $value,
                                    'label' => $label,
                                    'data-label' => $label,
                                    'data-id' => $value,
                                ]);
                            }])

                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

