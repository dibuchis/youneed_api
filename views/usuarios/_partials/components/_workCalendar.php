<?php
$webPath = Yii::$app->ManageAssets->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/workCalendarBehavior.js',
    ['depends' => [yii\web\YiiAsset::className()], 'position' => $this::POS_END]);

?>
<table id="work-time" class="table table-responsive table-striped">
    <thead>
    <tr>
        <td></td>
        <td>Día</td>
        <td>Jornada</td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <input type="checkbox" name='day[any][enabled]' checked="false" id="chkAll"
                   class="check-days check-any-day">
        </td>
        <td>Cualquier día - 24 horas</td>
        <td>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Desde</label>
                </div>
                <div class="col-sm-9">
                    <label> 00:00 </label>
                    <input class="any-day-input" name="day[any][0][start]" type="hidden" value="00:00" disabled>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Hasta</label>
                </div>
                <div class="col-sm-9">
                    <label> 23:59 </label>
                    <input class="any-day-input" name="day[any][0][end]" type="hidden" value="23:59" disabled>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <input type="checkbox" name='day[1][enabled]' checked="false" id="chkMonday"
                   class="check-days check-one-day">
        <td>Lunes</td>
        <td>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Desde</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[1][0][start]" class="form-control timepicker start_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Hasta</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[1][0][end]" class="form-control timepicker start_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>

                </div>
            </div>

        </td>
    </tr>
    <tr>
        <td>
            <input type="checkbox" name='day[2][enabled]' checked="false" id="chkTuesday"
                   class="check-days check-one-day">
        <td>Martes</td>
        <td>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Desde</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[2][0][start]" class="form-control timepicker start_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Hasta</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[2][0][end]" class="form-control timepicker end_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <input type="checkbox" name='day[3][enabled]' checked="false" id="chkWednesday"
                   class="check-days check-one-day">
        <td>Miércoles</td>
        <td>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Desde</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[3][0][start]" class="form-control timepicker start_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Hasta</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[3][0][end]" class="form-control timepicker end_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </td>

    </tr>
    <tr>
        <td>
            <input type="checkbox" name='day[4][enabled]' checked="false" id="chkThursday"
                   class="check-days check-one-day">
        </td>
        <td>Jueves</td>
        <td>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Desde</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[4][0][start]" class="form-control timepicker start_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Hasta</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[4][0][start]" class="form-control timepicker end_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <input type="checkbox" name='day[5][enabled]' checked="false" id="chkFriday"
                   class="check-days check-one-day">
        </td>
        <td>Viernes</td>
        <td>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Desde</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[5][0][start]" class="form-control timepicker start_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Hasta</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[5][0][end]" class="form-control timepicker end_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    <tr>

        <td>
            <input type="checkbox" name='day[6][enabled]' checked="false" id="chkSaturday"
                   class="check-days check-one-day">
        <td>Sábado</td>
        <td>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Desde</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[6][0][start]" class="form-control timepicker start_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Hasta</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[6][0][start]" class="form-control timepicker end_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <input type="checkbox" name='day[7][enabled]' checked="false" id="chkSunday"
                   class="check-days check-one-day">
        <td>Domingo</td>
        <td>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Desde</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[7][0][start]" class="form-control timepicker start_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label class="control-label">Hasta</label>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='text' name="day[7][0][start]" class="form-control timepicker end_day_time"/>
                            <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-time"></span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    </tbody>
</table>