<?php

use app\models\Usuarios;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'attribute' => 'multiple_id',
        'value' => function ($model) {
            return $model->id;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(Usuarios::find()->where('es_asociado=1')->asArray()->all(), 'id', 'id'),
        'filterWidgetOptions' => ['pluginOptions' => ['multiple' => true]],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'identificacion',
    ],
    [
        'attribute' => 'id',
        'label' => Yii::t('app', 'Nombre Completo'),
        'value' => function ($model) {
            return $model->getNombreCompleto();
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'placeholder' => 'Escriba...',
                'minimumInputLength' => 2,
                'allowClear' => true,
                'ajax' => [
                    'url' => Url::to(['usuarios/ajax-get-usuarios']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term, tipo:"ASOCIADO", targetField:"nombres"}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(data) { return data.nombreCompleto; }'),
                'templateSelection' => new JsExpression('function (data) {  return data.text; }'),

            ],
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'email',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'numero_celular',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'telefono_domicilio',
    ],
    [
        'attribute' => 'estado',
        'value' => function ($model, $key, $index, $widget) {
            return Yii::$app->params['parametros_globales']['estados'][$model->estado];
        },
        'filter' => array_merge(['' => 'Todos'], Yii::$app->params['parametros_globales']['estados']),
        'format' => 'raw'
    ],
    [
        'attribute' => 'ubicacion_name',
        'value' => function ($model, $key, $index, $widget) {
            return $model->pais->nombre .' / '. $model->ciudad->nombre;
        },
        'format' => 'raw'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'banco.nombre',
        'label' => Yii::t('app', 'Banco'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nombre_beneficiario',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'tipo_cuenta',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'numero_cuenta',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Delete',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'],
    ],

];   