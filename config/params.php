<?php

return [
    'adminEmail' => 'admin@example.com',
    'uploadImages' => '/img_temporales/',
    'uploadFiles' => '/documentos/',
    'api_token' => '8e705fdb6ed22df72e4fcbeb37bcf517',
    'google_key' => 'AIzaSyCWmnu8hgRqQzEIU3Sp35ygYoyq_WOIC6Q',
    'metros_redonda' => 6000, //6km
    'metros_redonda_movil' => 12500, //6km
    'metros_redonda_visita' => 100,
    'estados_genericos' => [
        1 => 'Activo',
        0 => 'Inactivo',
    ],
    'traccar' => array('socket_url' => 'ws://23.239.19.165:8082/api/socket',
        'rest_url' => 'http://23.239.19.165:8082/api/',
        'transmision_url' => 'http://23.239.19.165:5055/api',
        'usuario' => 'youneed',
        'clave' => '@AbitYouNeedServer2019',
    ),
    'token_firebase' => 'AIzaSyDkLPjdNxx6V09ZUJr8yBSO-EqykywlFHw',
    'parametros_globales' => [
        'estados' => [0 => 'Inactivo', 1 => 'Activo'],
        'estados_condiciones' => [0 => 'No', 1 => 'Si'],
        'estados_acciones' => [0 => 'Mantenimiento', 1 => 'Activo'],
        'estados_tareas' => [0 => 'Pendiente', 1 => 'Realizado'],
        'iva_valor' => '1.12',
        'iva_display' => '12%',
        'valor_visita_diagnostico' => 10,
        'texto_visita_diagnostico' => 'Visita diagnóstico',
    ],
    'estados_pedidos' => [
        /* 0 => 'En espera',
         1 => 'Reservada',
         2 => 'En ejecución',
         3 => 'Pagada',
         4 => 'Cancelada',
         5=> 'Penalizado'
        */
        0 => 'EN_ESPERA',
        1 => 'ACEPTADO',
        2 => 'RECHAZADO',
        3 => 'CONFIRMADO',
        4 => 'TERMINADO',
        5 => 'PAGADO',
        6 => 'CANCELADO',
    ],
    'tipo_atencion' => [
        0 => 'Urgente',
        1 => 'Alta',
        2 => 'Normal',
        3 => 'Baja',
    ],
    'tipo_ruc' => [
        0 => 'Sin RUC',
        1 => 'Persona Natural NO OBLIGADA a llevar contabilidad',
        2 => 'Persona Natural OBLIGADA a llevar contabilidad',
        3 => 'Persona Jurídica',
        4 => 'Regimen Impositivo Simplificado (RISE)',
    ],
    'tipo_identificacion' => [
        1 => 'Cédula',
        2 => 'RUC',
        3 => 'RISE',
        4 => 'Pasaporte'
    ],
    'email' => [
        'is_prod_env' => YII_ENV_PROD
    ],
    'sms' => [
        'is_prod_env' => YII_ENV_PROD
    ],
    'tipos_documentos' => [
        ['atributo_upload' => 'fotografia_cedula_upload', 'atributo_modelo' => 'fotografia_cedula'],
        ['atributo_upload' => 'ruc_upload', 'atributo_modelo' => 'ruc'],
        ['atributo_upload' => 'visa_trabajo_upload', 'atributo_modelo' => 'visa_trabajo'],
        ['atributo_upload' => 'rise_upload', 'atributo_modelo' => 'rise'],
        ['atributo_upload' => 'referencias_personales_upload', 'atributo_modelo' => 'referencias_personales'],
        //[ 'atributo_upload' => 'titulo_academico_upload', 'atributo_modelo' => 'titulo_academico' ],
    ],
    'forma_pago' => [
        1 => 'tarjeta debito/credito',
        2 => 'transferencia bancaria',
        3 => 'pago en efectivo',
        4 => 'otro medio',
    ]
];