<?php


namespace app\common\utils;

/**
 * Class DataTransformation
 * @author Mauricio Chamorro unrealmach@gmail.com
 * @package app\common\utils
 */
class DataTransformation
{
    public static function transformToSelect2($map)
    {
        $output = [];
        foreach ($map as $key => $value) {
            $select = ['id' => $key, 'text' => $value];
            $output[] = $select;
        }
        return $output;

    }
}