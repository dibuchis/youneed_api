<?php
namespace app\common\utils;

use app\common\models\constants\ResponseConstants;

/**
 * Class CommonUtil
 * @package app\common\utils
 * @author Mauricio Chamorro unrealmach@gmail.com
 */
class CommonUtil
{
    /**
     * Get the errors from model Active Record
     * @param $model
     * @return string
     */
    public static function getErrorsFromModel($model)
    {
        $outPut = '';
        if ($model!=null && !$model->validate()) {
            foreach ($model->getErrors() as $key => $value) {
                $outPut .= '<ul>';
                foreach ($value as $error) {
                    $outPut .= '<li>' . $error . '</li>';
                }
                $outPut .= '</ul><br>';
            }
        }else{
            $outPut=ResponseConstants::INVALID_DATA;
        }
        return $outPut;
    }
}