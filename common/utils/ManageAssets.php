<?php

namespace app\common\utils;
use Yii;
use yii\base\Component;

/**
 * Help to publish assets into of views
 * Class ManageAssets
 * @author Mauricio Chamorro unrealmach@gmail.com
 * @package app\common\utils
 */
class ManageAssets extends Component
{
    /**
     * Publish an asset (js) in a public web folder
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function getPublishAssetDirectory()
    {
        list($path, $webPath) = Yii::$app->getAssetManager()->publish(\Yii::$app->controller->module->basePath
            .'/views/'. Yii::$app->controller->id.'/assets/js/');
        return $webPath;
    }

}