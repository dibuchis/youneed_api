<?php
namespace app\common\utils;

use app\common\models\constants\ResponseConstants;
use app\common\models\constants\SMSContext;
use app\common\models\constants\YouNeedContext;
use app\common\models\SimpleContextResponse;

/**
 * Class SMSComposer
 * @author Mauricio Chamorro unrealmach@gmail.com
 * @package app\common\utils
 */
class SMSComposer
{
    /**
     * Send SMS
     * @param $type
     * @param $params
     * @return array
     */
    public static function sendSMS($type, $params)
    {
        $simpleResponse = new SimpleContextResponse();
        try {
            $params['emailTo'] = \Yii::$app->params['sms']['is_prod_env'] == false ? YouNeedContext::SMS_TESTING : $params['SMSTo'];

            switch ($type) {
                case  SMSContext::ACCEPTED_ORDER :
                    $template = \Yii::$app->view
                        ->renderFile('@app/common/views/sms/templates/accepted_order.php', []);
                    self::sendGeneralSMS($params['SMSTo'], $template, $params);
                    break;
                case  SMSContext::REJECT_ORDER :
                    $template = \Yii::$app->view
                        ->renderFile('@app/common/views/sms/templates/reject_order.php', []);
                    self::sendGeneralSMS($params['SMSTo'], $template, $params);
                    break;
                case  SMSContext::CONFIRMED_ORDER :
                    $template = \Yii::$app->view
                        ->renderFile('@app/common/views/sms/templates/confirmed_order.php', []);
                    self::sendGeneralSMS($params['SMSTo'], $template, $params);
                    break;
                default :
                    break;
            }

            return $simpleResponse
                ->setContext(1, ResponseConstants::SUCCESS_SENDED_SMS);
        } catch (Exception $e) {
            Yii::error($e->getMessage());
            return $simpleResponse
                ->setContext(0, ResponseConstants::NO_SENDED_SMS);
        }
    }

    //TODO: implementar con el API de terceros
    private static function sendGeneralSMS($SMSto, $template, $params)
    {
        return true;
    }


}