<?php


namespace app\common\utils;


use app\common\models\constants\EmailContext;
use app\common\models\constants\ResponseConstants;
use app\common\models\constants\YouNeedContext;
use app\common\models\SimpleContextResponse;
use Yii;

/**
 * Class EmailComposer assist the send of email
 * @author Mauricio Chamorro unrealmach@gmail.com
 * @package app\common\utils
 */
class EmailComposer
{
    /**
     * Send a email
     * @param $type EmailContext
     * @param $params
     * @return array JSON response
     */
    public static function sendEmail($type, $params)
    {
        $simpleResponse = new SimpleContextResponse();
        try {
            $params['emailTo'] = Yii::$app->params['email']['is_prod_env'] == false ? YouNeedContext::EMAIL_TESTING : $params['emailTo'];

            switch ($type) {
                case EmailContext::RECOVERY_PASSWORD:
                    self::sendRecoveryPassword($params['emailTo'], $params['link'], $params['model']);
                    break;
                case EmailContext::SERVICE_REQUEST:
                    self::sendServiceRequest($params['emailTo'], $params['servicio'], $params['cliente']);
                    break;
                case EmailContext::ASSOCIATE_SERVICE_REQUEST:
                    self::sendAssociateServiceRequest($params['emailTo'], $params['asociado'],
                        $params['cliente'], $params['pedido'], $params['servicio']);
                    break;
                case EmailContext::RESERVED_SERVICE:
                    self::sendReservedService($params['emailTo'], $params['pedido']);
                    break;
                case EmailContext::CANCELED_SERVICE:
                    self::sendCanceledService($params['emailTo'], $params['pedido']);
                    break;
                case EmailContext::CANCELED_SERVICE_FROM_CLIENT:
                    self::sendCanceledServiceFromClient($params['emailTo'], $params['pedido']);
                    break;
                case EmailContext::ACCEPTED_SERVICE:
                    self::sendAcceptedService($params['emailTo'], $params['pedido']);
                    break;
                case EmailContext::SEND_OBSERVATIONS:
                    self::sendObservations($params['emailTo'], $params['body']);
                    break;
                case EmailContext::SEND_REVIEW_OK:
                    self::sendReviewOkObservations($params['emailTo'], $params['body']);
                    break;
                case EmailContext::SEND_TEST:
                    self::sendTestEmail($params['emailTo']);
                    break;
                case EmailContext::SEND_SUCCESS_REGISTER_CLIENT:
                    self::sendSuccessRegisterClient($params['emailTo'], $params['body']);
                    break;
                case EmailContext::SEND_SUCCESS_RECOVERY_PASSWORD:
                    self::sendSuccessRecoveryPassword($params['emailTo'], $params['body']);
                    break;
                case EmailContext::SEND_SUCCESS_ASSOCIATE_REGISTER:
                    self::sendNewAssociateRegister($params['emailTo'], $params['body']);
                    break;
                case EmailContext::REJECT_SERVICE:
                    self::sendRejectService($params['emailTo'], $params['body']);
                    break;
                case EmailContext::CONFIRMED_SERVICE:
                    self::confirmedService($params['emailTo'], $params['body']);
                    break;
                default:
                    break;

            }
            return $simpleResponse
                ->setContext(1, ResponseConstants::SUCCESS_SENDED_EMAIL);
        } catch (Exception $e) {
            Yii::error($e->getMessage());
            return $simpleResponse
                ->setContext(0, ResponseConstants::NO_SENDED_EMAIL);
        }
    }

    private static function sendRecoveryPassword($emailTo, $link, $model)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::RECOVERY_PASSWORD_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/request_cambio_clave.php',
                    ['model' => $model, 'link' => $link]), 'text/html')
            ->send();
    }

    private static function sendServiceRequest($emailTo, $servicio, $cliente)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::SERVICE_REQUEST_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/request_servicio.php',
                    ['servicio' => $servicio, 'cliente' => $cliente]), 'text/html')
            ->send();
    }

    private static function sendAssociateServiceRequest($emailTo, $asociado, $cliente, $pedido, $servicio)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::ASSOCIATE_SERVICE_REQUEST_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/request_servicio_asociado.php',
                    ['asociado' => $asociado, 'cliente' => $cliente, 'pedido' => $pedido, 'servicio' => $servicio]), 'text/html')
            ->send();
    }

    private static function sendReservedService($emailTo, $pedido)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::RESERVED_SERVICE_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/reserved_service.php',
                    ['pedido' => $pedido]), 'text/html')
            ->send();
    }

    private static function sendCanceledService($emailTo, $pedido)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::CANCELED_SERVICE_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/canceled_service.php',
                    ['pedido' => $pedido]), 'text/html')
            ->send();
    }

    private static function sendCanceledServiceFromClient($emailTo, $pedido)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::CANCELED_SERVICE_FROM_CLIENT_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/canceled_service_from_client.php',
                    ['pedido' => $pedido]), 'text/html')
            ->send();
    }

    private static function sendObservations($emailTo, $body)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::SEND_OBSERVATIONS_SUBJECT)
            ->setHtmlBody($body, 'text/html')
            ->send();
    }

    private static function sendReviewOkObservations($emailTo, $body)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::SEND_REVIEW_OK_SUBJECT)
            ->setHtmlBody($body, 'text/html')
            ->send();
    }

    private static function sendTestEmail($emailTo)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::SEND_TEST_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/test.php'), 'text/html')
            ->send();
    }

    private static function sendSuccessRegisterClient($emailTo, $body)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::SEND_SUCCESS_REGISTER_CLIENT_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/success_register_client.php',
                    ['nombre' => $body]), 'text/html')
            ->send();
    }

    private static function sendSuccessRecoveryPassword($emailTo, $body)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::SEND_SUCCESS_RECOVERY_PASSWORD_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/success_recovery_password.php',
                    ['nombre' => $body]), 'text/html')
            ->send();
    }

    private static function sendNewAssociateRegister($emailTo, $body)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::SEND_SUCCESS_ASSOCIATE_REGISTER_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/success_register_associate.php',
                    ['model' => $body]), 'text/html')
            ->send();
    }

    private static function sendAcceptedService($emailTo, $pedido)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::ACCEPTED_SERVICE_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/accepted_service.php',
                    ['pedido' => $pedido]), 'text/html')
            ->send();
    }

    private static function sendRejectService($emailTo, $body)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::REJECT_SERVICE_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/reject_service.php',
                    ['pedido' => $body]), 'text/html')
            ->send();
    }


    private static function confirmedService($emailTo, $body)
    {
        Yii::$app->mailer->compose()
            ->setFrom(YouNeedContext::EMAIL_MASTER, YouNeedContext::NAME_MASTER)
            ->setTo($emailTo)
            ->setSubject(EmailContext::CONFIRMED_SERVICE_SUBJECT)
            ->setHtmlBody(
                \Yii::$app->view->renderFile('@app/common/views/email/templates/confirmed_service.php',
                    ['pedido' => $body]), 'text/html')
            ->send();
    }
}