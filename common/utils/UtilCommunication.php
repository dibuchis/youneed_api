<?php


namespace app\common\utils;


use app\common\models\constants\YouNeedContext;

/**
 * This is a function collection by helping to configure the response and request in the project
 * Class UtilCommunication
 * @package app\common\models
 */
class UtilCommunication
{
    private static function getStatusCodeMessage($status)
    {
        $codes = [
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        ];
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    public function setHeader($status)
    {

        $status_header = 'HTTP/1.1 ' . $status . ' ' . self::getStatusCodeMessage($status);
        $content_type = "application/json; charset=utf-8";

        header($status_header);
        header('Content-type: ' . $content_type);
        //  header('X-Powered-By: ' . "Abitmedia <abitmedia.com>"); //TODO: es necesario??
        header("Access-Control-Allow-Origin: ".YouNeedContext::DEV_FRONT_REMOTE_IP);
    }

    public function allowedDomains() {
        return [
             '*',                        // star allows all domains
        ];
    }

}