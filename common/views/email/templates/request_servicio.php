<?= \Yii::$app->view->renderFile('@app/common/views/email/header.php') ?>
<div style="padding:25px; margin:0px auto; max-width:650px;">
    <h2 style="font-family:Arial, Helvetica, sans-serif; color:#117c8f;">
        <?= $cliente->nombres ?>,
    </h2>
    <h3 style="font-family:Arial, Helvetica, sans-serif; color:#117c8f;">
        Solicitud de Servicio
    </h3>
</div>
<div style="margin:25px auto; max-width:650px;">
    <p style="font-family:Arial, Helvetica, sans-serif; color:#9a999e;">
        Has solicitado el servicio <?= $servicio->nombre ?>
        del asociado <?= $cliente->nombres . ' ' . $cliente->apellidos ?>,
        en breve tendras detalle de tu solicitud.
    </p>
    <p style="font-family:Arial, Helvetica, sans-serif; color:#9a999e;">
        Por favor, ingresa a tu perfil para ver el estado de tu solicitud:
    </p>
    <p>
        <a style="background-color: #178b89!important; border-color: #178b89!important; line-height: 1.42857143; text-align: center; white-space: nowrap; font-size: 14px; padding: 6px 12px; color: #fff; margin: 35px auto 10px; width: 180px; display: block;"
           href="https://youneed.com.ec/app/login.php">Mi Perfil</a>
    </p>
    <?= \Yii::$app->view->renderFile('@app/common/views/email/footer.php') ?>
</div>


