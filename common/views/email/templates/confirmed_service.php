<?= \Yii::$app->view->renderFile('@app/common/views/email/header.php') ?>
<div style="padding:25px; margin:0px auto; max-width:650px;">
    <h2 style="font-family:Arial, Helvetica, sans-serif; color:#117c8f;">
        <?= $pedido->cliente->nombres. ' '.$pedido->cliente->apellidos ?>,
    </h2>
    <h3 style="font-family:Arial, Helvetica, sans-serif; color:#117c8f;">
        Servicio Confirmado
    </h3>
</div>
<div style="margin:25px auto; max-width:650px;">
    <div style="margin:25px auto; max-width:650px;"><p style="font-family:Arial, Helvetica, sans-serif; color:#9a999e;">
        <p> Su pedido se encuentra en camino.</p>
        <p><b>Servicio solicitado: </b><?= $pedido->servicio->nombre ?></p>
        <p><b>Proveedor: </b><?= $pedido->asociado->nombres . " " . $pedido->asociado->apellidos ?></p>
        <p><b>Que incluye? </b><?= $pedido->servicio->incluye ?></p>
        <p><b>Valor unitario </b><?= $pedido->subtotal ?></p>
        <p><b>Cantidad </b><?= 'PROXIMAMENTE' ?></p>
        <p><b>IVA </b><?= $pedido->iva ?></p>
        <p><b>Valor Total </b><?= $pedido->total ?></p>
        <p><b>Fecha y Hora: </b><?= $pedido->fecha_para_servicio ?></p>

        <p style="font-family:Arial, Helvetica, sans-serif; color:#9a999e;">

            Asegurese de cumplias las siguientes recomendaciones:
            1. Que haya una persona, para que reciba el servicio o pedido.
            2. Verificar documento del Proveedor del servicio.
            3. Verificar que el servicio o pedido se realice o entregue segun detalle.
            4. Realizar el pago segun la forma de pago de su pedido.

            Le recordamos que YOUNEED presta el servicio de intermediacion, por lo que la Garantia del Servicio o Peido es responsabilidad del Proveedor, Terminos y Condiciones aceptadas.
        </p>
        <p>
            Ingrese a su cuenta de Youneed para realizar el pago de su pedido

            <a style="background-color: #178b89!important; border-color: #178b89!important; line-height: 1.42857143; text-align: center; white-space: nowrap; font-size: 14px; padding: 6px 12px; color: #fff; margin: 35px auto 10px; width: 180px; display: block;"
               href="https://youneed.com.ec/app/login.php">Mi Perfil</a>
        </p>
    </div>
    <?= \Yii::$app->view->renderFile('@app/common/views/email/footer.php') ?>
</div>
