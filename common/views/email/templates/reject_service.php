<?= \Yii::$app->view->renderFile('@app/common/views/email/header.php') ?>
<div style="padding:25px; margin:0px auto; max-width:650px;">
    <h2 style="font-family:Arial, Helvetica, sans-serif; color:#117c8f;">
        <?= $pedido->cliente->nombres ." ". $pedido->cliente->apellidos ?>,
    </h2>
    <h3 style="font-family:Arial, Helvetica, sans-serif; color:#117c8f;">
        Servicio Rechazado
    </h3>
</div>
<div style="margin:25px auto; max-width:650px;">
    <div style="margin:25px auto; max-width:650px;"><p style="font-family:Arial, Helvetica, sans-serif; color:#9a999e;">
        <p>Su pedido no puede ser atentido por el proveedor: <?= $pedido->asociado->nombres . " " . $pedido->asociado->apellidos ?>.</p>
        <p>Puede seleccionar a otro proveedor en la pagina de servicios:</p>

            <a style="background-color: #178b89!important; border-color: #178b89!important; line-height: 1.42857143; text-align: center; white-space: nowrap; font-size: 14px; padding: 6px 12px; color: #fff; margin: 35px auto 10px; width: 180px; display: block;"
               href="https://youneed.com.ec/servicios/">Servicios</a>
        </p>
    </div>
    <?= \Yii::$app->view->renderFile('@app/common/views/email/footer.php') ?>
</div>
