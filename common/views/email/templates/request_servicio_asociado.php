<?= \Yii::$app->view->renderFile('@app/common/views/email/header.php') ?>
<div style="padding:25px; margin:0px auto; max-width:650px;">
    <h2 style="font-family:Arial, Helvetica, sans-serif; color:#117c8f;">
        <?= $asociado->nombres ?>,
    </h2>
    <h3 style="font-family:Arial, Helvetica, sans-serif; color:#117c8f;">
        Solicitud de Servicio
    </h3>
</div>
<div style="margin:25px auto; max-width:650px;">
    <div style="margin:25px auto; max-width:650px; font-family:Arial, Helvetica, sans-serif; color:#9a999e;">
        <h3>Datos de la Solicitud:</h3>
        <table style="border-color:#e3e3e3;">
            <tr>
                <td>Nombre</td>
                <td><?= $cliente->nombres . " " . $cliente->apellidos ?></td>
            </tr>
            <tr>
                <td>Servicio</td>
                <td> <?= $servicio->nombre ?> </td>
            </tr>
            <tr>
                <td>Fecha de solicitud</td>
                <td><?= $pedido->fecha_creacion ?></td>
            </tr>
            <tr>
                <td>Fecha para dar Servicio</td>
                <td><?= $pedido->fecha_para_servicio ?></td>
            </tr>
        </table>
        <p style="font-family:Arial, Helvetica, sans-serif; color:#9a999e;">
            Por favor, ingresa a tu perfil para ver los datos de tu solicitud: </p>
        <p>
            <a style="background-color: #178b89!important; border-color: #178b89!important; line-height: 1.42857143; text-align: center; white-space: nowrap; font-size: 14px; padding: 6px 12px; color: #fff; margin: 35px auto 10px; width: 180px; display: block;"
               href="https://youneed.com.ec/app/login.php">Mi Perfil</a>
        </p>
    </div>
    <?= \Yii::$app->view->renderFile('@app/common/views/email/footer.php') ?>
</div>
