<?= \Yii::$app->view->renderFile('@app/common/views/email/header.php') ?>
<div style="padding:25px; margin:0px auto; max-width:650px;">
    <h2 style="font-family:Arial, Helvetica, sans-serif; color:#117c8f;">
        <?= $model->nombres ?>,
    </h2>
    <h3 style="font-family:Arial, Helvetica, sans-serif; color:#117c8f;">
        Has solicitado una recuperación de tu clave
    </h3>
</div>
<div style="margin:25px auto; max-width:650px;">
    <p style="font-family:Arial, Helvetica, sans-serif; color:#9a999e;">
        Para efectuar el cambio de clave de tu cuenta de Youneed, accede al siguiente link:
    </p>
    <p>
        <a href="<?= $link ?>"> <?= $link ?> </a>
    </p>

    <?= \Yii::$app->view->renderFile('@app/common/views/email/footer.php') ?>
</div>