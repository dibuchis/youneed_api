<?= \Yii::$app->view->renderFile('@app/common/views/email/header.php') ?>
<div style="padding:25px; margin:0px auto; max-width:650px;">
    <h2 style="font-family:Arial, Helvetica, sans-serif; color:#117c8f;">
        <?= $model->nombres ?>
    </h2>
    <h3 style="font-family:Arial, Helvetica, sans-serif; color:#117c8f;">
        ¡Bienvenido a YouNeed!
    </h3>
</div>
<div style="margin:25px auto; max-width:650px;">

        <p style="font-family:Arial, Helvetica, sans-serif; color:#9a999e;">
            Estimado Asociado,
        </p>
        <p style="font-family:Arial, Helvetica, sans-serif; color:#9a999e;">
            Gracias por unirte a la mayor red de
            profesionales y clientes que están usando YouNeed para ofrecer sus servicios, nuestro compromiso es brindarte
            las mejores herramientas para que canalices tu talento hacia la comunidad y obtengas los beneficios que siempre
            quisiste.
        </p>
        <p style="font-family:Arial, Helvetica, sans-serif; color:#9a999e;">
            Por favor, ingresa a tu perfil para ver los
            datos y documentos:
        </p>
        <p>
            <a style="background-color: #2bbdbb!important; border-color: #178b89 !important; line-height: 1.42857143; text-align: center; white-space: nowrap; font-size: 14px; padding: 6px 12px; color: #fff; margin: 35px auto 10px; width: 180px; display: block;color: #093131; font-size: 20px;  border-radius: 5px; text-decoration: blink;"
               href="https://youneed.com.ec/app/login.php">Perfil de Asociado</a>
        </p>
        <p><b>AVISO:</b> “Te llegará máximo en 24 horas un correo de VALIDACION y ACTIVACION de tu Registro”</p>
        <p><b>Nota:</b> “Youneed verificará los datos y documentos cargados. Youneed se reserva el derecho de admisión en
            caso de detectar inconsistencias en la información y documentos proporcionados y que no permitan verificar la
            identidad del Asociado”</p> .

    <?= \Yii::$app->view->renderFile('@app/common/views/email/footer.php') ?>
</div>


