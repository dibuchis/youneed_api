<?php

namespace app\common\models\sessions;

/**
 * Class UserSession
 * Contract to user, is a kind of DTO
 * @package app\common\models\sessions
 */
class UserSession
{
    private $_id;
    private $_estado;
    private $_display_name;
    private $_nombres;
    private $_apellidos;
    private $_email;
    private $_numero_celular;
    private $_telefono_domicilio;
    private $_imagen;
    private $_token;
    private $_traccar_id;
    private $_traccar_transmision;
    private $_imei;
    private $_items_cart;
    private $_fecha_creacion;
    private $_fecha_activacion;
    private $_identificacion;
    private $_tipo;
    private $_numero_cuenta;
    private $_pais;
    private $_ciudad;
    private $_plan;
    private $_categorias;
    private $_servicios;
    private $_pagos;
    private $_documentos;
    private $_cuenta;
    private $_plan_info;
    private $_banco;
    private $_jornada_trabajo;
    private $_banco_id;
    private $_nombre_beneficiario;
    private $_tipo_cuenta;
    private $_preferencias_deposito;
    private $_usuarios_servicios;//n-n table
    private $_tipo_ruc;//n-n table
    private $_fotografia_cedula;
    private $_ruc;
    private $_visa_trabajo;
    private $_rise;
    private $_referencias_personales;
    private $_titulo_academico;

    public function __construct()
    {
        $this->_id = NULL;
        $this->_estado = NULL;
        $this->_display_name = NULL;
        $this->_nombres = NULL;
        $this->_apellidos = NULL;
        $this->_email = NULL;
        $this->_numero_celular = NULL;
        $this->_telefono_domicilio = NULL;
        $this->_imagen = NULL;
        $this->_token = NULL;
        $this->_traccar_id = NULL;
        $this->_traccar_transmision = NULL;
        $this->_imei = NULL;
        $this->_items_cart = NULL;
        $this->_fecha_creacion = NULL;
        $this->_fecha_activacion = NULL;
        $this->_identificacion = NULL;
        $this->_tipo = NULL;
        $this->_numero_cuenta = NULL;
        $this->_pais = NULL;
        $this->_ciudad = NULL;
        $this->_plan = NULL;
        $this->_categorias = NULL;
        $this->_servicios = NULL;
        $this->_pagos = NULL;
        $this->_documentos = NULL;
        $this->_cuenta = NULL;
        $this->_plan_info = NULL;
        $this->_banco = NULL;
        $this->_jornada_trabajo = NULL;
        $this->_banco_id = NULL;
        $this->_nombre_beneficiario = NULL;
        $this->_tipo_cuenta = NULL;
        $this->_preferencias_deposito = NULL;
        $this->_usuarios_servicios = NULL;
        $this->_tipo_ruc = NULL;
        $this->_fotografia_cedula = NULL;
        $this->_ruc = NULL;
        $this->_visa_trabajo = NULL;
        $this->_rise = NULL;
        $this->_referencias_personales = NULL;
        $this->_titulo_academico = NULL;

    }

    public function getUserSession()
    {
        return [
            'id' => $this->_id,
            'estado' => $this->_estado,
            'display_name' => $this->_display_name,
            'nombres' => $this->_nombres,
            'apellidos' => $this->_apellidos,
            'email' => $this->_email,
            'numero_celular' => $this->_numero_celular,
            'telefono_domicilio' => $this->_telefono_domicilio,
            'imagen' => $this->_imagen,
            'token' => $this->_token,
            'traccar_id' => $this->_traccar_id,
            'traccar_transmision' => $this->_traccar_transmision,
            'imei' => $this->_imei,
            'items_cart' => $this->_items_cart,
            'fecha_creacion' => $this->_fecha_creacion,
            'fecha_activacion' => $this->_fecha_activacion,
            'identificacion' => $this->_identificacion,
            'tipo' => $this->_tipo,
            'numero_cuenta' => $this->_numero_cuenta,
            'pais' => $this->_pais,
            'ciudad' => $this->_ciudad,
            'plan' => $this->_plan,
            'categorias' => $this->_categorias,
            'servicios' => $this->_servicios,
            'pagos' => $this->_pagos,
            'documentos' => $this->_documentos,
            'cuenta' => $this->_cuenta,
            'plan_info' => $this->_plan_info,
            'banco' => $this->_banco,
            'jornada_trabajo' => $this->_jornada_trabajo,
            'banco_id' => $this->_banco_id,
            'nombre_beneficiario' => $this->_nombre_beneficiario,
            'tipo_cuenta' => $this->_tipo_cuenta,
            'preferencias_deposito' => $this->_preferencias_deposito,
            'usuarios_servicios' => $this->_usuarios_servicios,
            'tipo_ruc' => $this->_tipo_ruc,
            'fotografia_cedula' => $this->_fotografia_cedula,
            'ruc' => $this->_ruc,
            'visa_trabajo' => $this->_visa_trabajo,
            'rise' => $this->_rise,
            'referencias_personales' => $this->_referencias_personales,
            'titulo_academico' => $this->_titulo_academico,
        ];
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @param null $estado
     */
    public function setEstado($estado)
    {
        $this->_estado = $estado;
    }

    /**
     * @param null $display_name
     */
    public function setDisplayName($display_name)
    {
        $this->_display_name = $display_name;
    }

    /**
     * @param null $nombres
     */
    public function setNombres($nombres)
    {
        $this->_nombres = $nombres;
    }

    /**
     * @param null $apellidos
     */
    public function setApellidos($apellidos)
    {
        $this->_apellidos = $apellidos;
    }

    /**
     * @param null $email
     */
    public function setEmail($email)
    {
        $this->_email = $email;
    }

    /**
     * @param null $numero_celular
     */
    public function setNumeroCelular($numero_celular)
    {
        $this->_numero_celular = $numero_celular;
    }

    /**
     * @param null $telefono_domicilio
     */
    public function setTelefonoDomicilio($telefono_domicilio)
    {
        $this->_telefono_domicilio = $telefono_domicilio;
    }

    /**
     * @param null $imagen
     */
    public function setImagen($imagen)
    {
        $this->_imagen = $imagen;
    }

    /**
     * @param null $token
     */
    public function setToken($token)
    {
        $this->_token = $token;
    }

    /**
     * @param null $traccar_id
     */
    public function setTraccarId($traccar_id)
    {
        $this->_traccar_id = $traccar_id;
    }

    /**
     * @param null $traccar_transmision
     */
    public function setTraccarTransmision($traccar_transmision)
    {
        $this->_traccar_transmision = $traccar_transmision;
    }

    /**
     * @param null $imei
     */
    public function setImei($imei)
    {
        $this->_imei = $imei;
    }

    /**
     * @param null $items_cart
     */
    public function setItemsCart($items_cart)
    {
        $this->_items_cart = $items_cart;
    }

    /**
     * @param null $fecha_creacion
     */
    public function setFechaCreacion($fecha_creacion)
    {
        $this->_fecha_creacion = $fecha_creacion;
    }

    /**
     * @param null $fecha_activacion
     */
    public function setFechaActivacion($fecha_activacion)
    {
        $this->_fecha_activacion = $fecha_activacion;
    }

    /**
     * @param null $identificacion
     */
    public function setIdentificacion($identificacion)
    {
        $this->_identificacion = $identificacion;
    }

    /**
     * @param null $tipo
     */
    public function setTipo($tipo)
    {
        $this->_tipo = $tipo;
    }

    /**
     * @param null $numero_cuenta
     */
    public function setNumeroCuenta($numero_cuenta)
    {
        $this->_numero_cuenta = $numero_cuenta;
    }

    /**
     * @param null $pais
     */
    public function setPais($pais)
    {
        $this->_pais = $pais;
    }

    /**
     * @param null $ciudad
     */
    public function setCiudad($ciudad)
    {
        $this->_ciudad = $ciudad;
    }

    /**
     * @param null $plan
     */
    public function setPlan($plan)
    {
        $this->_plan = $plan;
    }

    /**
     * @param null $categorias
     */
    public function setCategorias($categorias)
    {
        $this->_categorias = $categorias;
    }

    /**
     * @param null $servicios
     */
    public function setServicios($servicios)
    {
        $this->_servicios = $servicios;
    }

    /**
     * @param null $pagos
     */
    public function setPagos($pagos)
    {
        $this->_pagos = $pagos;
    }

    /**
     * @param null $documentos
     */
    public function setDocumentos($documentos)
    {
        $this->_documentos = $documentos;
    }

    /**
     * @param null $cuenta
     */
    public function setCuenta($cuenta)
    {
        $this->_cuenta = $cuenta;
    }

    /**
     * @param null $plan_info
     */
    public function setPlanInfo($plan_info)
    {
        $this->_plan_info = $plan_info;
    }

    /**
     * @param null $banco
     */
    public function setBanco($banco)
    {
        $this->_banco = $banco;
    }

    /**
     * @param null $jornada_trabajo
     */
    public function setJornadaTrabajo($jornada_trabajo)
    {
        $this->_jornada_trabajo = $jornada_trabajo;
    }

    /**
     * @param null $banco_id
     */
    public function setBancoId($banco_id)
    {
        $this->_banco_id = $banco_id;
    }

    /**
     * @param null $nombre_beneficiario
     */
    public function setNombreBeneficiario($nombre_beneficiario)
    {
        $this->_nombre_beneficiario = $nombre_beneficiario;
    }

    /**
     * @param null $tipo_cuenta
     */
    public function setTipoCuenta($tipo_cuenta)
    {
        $this->_tipo_cuenta = $tipo_cuenta;
    }

    /**
     * @param null $preferencias_deposito
     */
    public function setPreferenciasDeposito($preferencias_deposito)
    {
        $this->_preferencias_deposito = $preferencias_deposito;
    }

    /**
     * @param null $usuarios_servicios
     */
    public function setUsuariosServicios($usuarios_servicios)
    {
        $this->_usuarios_servicios = $usuarios_servicios;
    }

    /**
     * @param null $tipo_ruc
     */
    public function setTipoRuc($tipo_ruc)
    {
        $this->_tipo_ruc = $tipo_ruc;
    }

    /**
     * @param null $fotografia_cedula
     */
    public function setFotografiaCedula($fotografia_cedula)
    {
        $this->_fotografia_cedula = $fotografia_cedula;
    }

    /**
     * @param null $ruc
     */
    public function setRuc($ruc)
    {
        $this->_ruc = $ruc;
    }

    /**
     * @param null $visa_trabajo
     */
    public function setVisaTrabajo($visa_trabajo)
    {
        $this->_visa_trabajo = $visa_trabajo;
    }

    /**
     * @param null $rise
     */
    public function setRise($rise)
    {
        $this->_rise = $rise;
    }

    /**
     * @param null $referencias_personales
     */
    public function setReferenciasPersonales($referencias_personales)
    {
        $this->_referencias_personales = $referencias_personales;
    }

    /**
     * @param null $titulo_academico
     */
    public function setTituloAcademico($titulo_academico)
    {
        $this->_titulo_academico = $titulo_academico;
    }



}
