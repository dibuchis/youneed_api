<?php


namespace app\common\models\constants;

/**
 * Class EmailContext helps to define the email messages
 * @package app\common\models\constants
 */
class EmailContext
{
    //EMAIL
    const RECOVERY_PASSWORD = 'RECOVERY_PASSWORD';
    const RECOVERY_PASSWORD_SUBJECT = 'YouNeed - Recuperacion de Clave';
    const SERVICE_REQUEST = 'SERVICE_REQUEST';
    const SERVICE_REQUEST_SUBJECT = "YouNeed - Solicitud de Servicio";
    const ASSOCIATE_SERVICE_REQUEST = "ASSOCIATE_SERVICE_REQUEST";
    const ASSOCIATE_SERVICE_REQUEST_SUBJECT = "YouNeed - Nueva Solicitud de Contrato por Servicios";

    const RESERVED_SERVICE = "RESERVED_SERVICE";
    const RESERVED_SERVICE_SUBJECT = "YouNeed - Servicio Reservado";
    const CANCELED_SERVICE = 'CANCELED_SERVICE';
    const CANCELED_SERVICE_SUBJECT = "YouNeed - Servicio Cancelado";
    const CANCELED_SERVICE_FROM_CLIENT = 'CANCELED_SERVICE_FROM_CLIENT';
    const CANCELED_SERVICE_FROM_CLIENT_SUBJECT = "YouNeed - Servicio Cancelado Por el Cliente";
    const ACCEPTED_SERVICE='ACCEPTED_SERVICE';
    const ACCEPTED_SERVICE_SUBJECT= "YouNeed - Pedido aceptado";
    const REJECT_SERVICE = 'REJECT_SERVICE';
    const REJECT_SERVICE_SUBJECT = "YouNeed - Pedido rechazado";
    const SEND_OBSERVATIONS = 'SEND_OBSERVATIONS';
    const SEND_OBSERVATIONS_SUBJECT = 'YouNeed - Problemas con su cuenta';
    const CONFIRMED_SERVICE= 'CONFIRMED_SERVICE';
    const CONFIRMED_SERVICE_SUBJECT= 'YouNeed - Servicio confirmado por el Proveedor';

    const SEND_REVIEW_OK = 'SEND_REVIEW_OK';
    const SEND_REVIEW_OK_SUBJECT = 'YouNeed - Bienvenido';
    const SEND_TEST = 'SEND_TEST';
    const SEND_TEST_SUBJECT = 'YouNeed - EMAIL TESTING';
    const SEND_SUCCESS_REGISTER_CLIENT = 'SEND_SUCCESS_REGISTER_CLIENT';
    const SEND_SUCCESS_REGISTER_CLIENT_SUBJECT = 'YouNeed - Bienvenido Cliente';
    const SEND_SUCCESS_RECOVERY_PASSWORD = 'SEND_SUCCESS_RECOVERY_PASSWORD';
    const SEND_SUCCESS_RECOVERY_PASSWORD_SUBJECT = 'YouNeed - Reseteo exitoso de clave';
    const SEND_SUCCESS_ASSOCIATE_REGISTER = 'SEND_SUCCESS_ASSOCIATE_REGISTER';
    const SEND_SUCCESS_ASSOCIATE_REGISTER_SUBJECT = 'YouNeed - Registro exitoso';
}