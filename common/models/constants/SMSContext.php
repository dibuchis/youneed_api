<?php


namespace app\common\models\constants;

/**
 * Class SMSContext helps to define de context message of SMS
 * @package app\common\models\constants
 */
class SMSContext
{
    const ACCEPTED_ORDER = 'ACCEPTED_ORDER';
    const REJECT_ORDER = 'REJECT_ORDER';
    const CONFIRMED_ORDER = 'CONFIRMED_ORDER';

}