<?php

namespace app\common\models\constants;

/**
 * Class ResponseConstants helps to define the responses of API
 * @package app\common\models\constants
 */
class ResponseConstants
{
    const INCORRECT_EMAIL_AND_PASSWORD = 'Email o contraseña incorrectos';
    const USER_NOT_FOUND = 'Usuario no encontrado o desactivado';
    const USER_VALID_IN_WEB_ENV = 'Usuario válido en ambiente web solamente';
    const USER_WITHOUT_ROL = 'Usuario sin rol definido';
    const ERROR_IN_LOGIN = 'Ocurrio un problema al ingresar';
    const EMAIL_AND_PASSWORD_REQUIRED = 'Email y contraseña son requeridos';
    const TYPE_PARAM_ERROR = 'El parámetro tipo puede ser: Asociado o Cliente';
    const VALIDATE_INFORMATION = 'Informacion de validacion';
    const ERROR_IN_CURRENT_USER = 'Ocurrio un error al registrar usuario';
    const INCORRECT_SENDED_PARAMETERS = 'Parámetros recibidos incorrectos';
    const SUCCESS_UPDATE_PROFILE = 'Sus datos han sido actualizados exitosamente.';
    const ERROR_FROM_SERVER = 'Hubo un error o sus datos no han cambiado, por favor intentar nuevamente.';
    const ERROR_PASSWORD_RESET = 'Hubo un error al intentar solicitar su nueva clave, por favor intentar nuevamente.';
    const INVALID_DATA = 'Los datos ingresados no son válidos, vuelva a intentarlo.';
    const SUCCESS_SENDED_EMAIL = 'Email Enviado Correctamente';
    const NO_SENDED_EMAIL = 'Email no enviado, verifique su email o consulte con el administrador del sistema.';
    const EMPTY_EMAIL_ERROR = 'Ingrese su email';
    const RECOVERY_EMAIL_SENDED = 'Se ha enviado un correo electrónico para restablecer su contraseña.';
    const COMMON_MESSAGE_ERROR = 'Hubo un error, por favor intentar nuevamente.';
    const REPLAY_NEW_PASSWORD_ERROR = 'Hubo un error al intentar solicitar su nueva clave, por favor intentar nuevamente.';
    const NO_TEXT_ERROR = 'No puede dejar espacios en blanco';
    const INVALID_RECOVERY_PASSWORD_ERROR = 'Lo sentimos su solicitud de recuperación de clave ha caducado o es incorrecta, vuelva a intentar generar una nueva solicitud de recuperación de clave.';
    const ERROR_WHEN_RECOVERY_PASSWORD = 'Hubo un error al intentar restablecer su contraseña.';
    const RECOVERY_PASSWORD_TRY_AGAIN_ERROR = 'No se pudo retablecer su contraseña, vuelva a intentarlo.';
    const SUCCESS_SENDED_SMS = 'SMS Enviado Correctamente';
    const NO_SENDED_SMS = 'SMS no enviado, verifique el contacto o consulte con el administrador del sistema.';

    const SUCCESS_REGISTER = 'Registrado Exitosamente';
    const SUCCESS_REGISTER_PASSWORD = 'Su contraseña se registró correctamente.';

    const ASSOCIATE_CLIENT = 'asociado_cliente';
    const ASSOCIATE = 'asociado';
    const CLIENT = 'cliente';
}

