<?php


namespace app\common\models\constants;


use Yii;

/**
 * Class YouNeedContext
 * @package app\common\models\constants
 */
class YouNeedContext
{
    const ASSOCIATED_SCENARIO = 'Asociado';
    const LAGGART_ASSOCIATE_SCENARIO = 'Asociado_rezagado'; //para asociados que no pueden registrarse y piden ayuda del administrador
    const CLIENT_SCENARIO = 'Cliente';
    const REGISTER_CLIENT_SCENARIO = 'Cliente_register'; // para el primer registro, se diferencia de cliente en que no pide el numero de identificacion
    const WEBAPP_SCENARIO = 'WebApp';

    const COUNTRY_PREFIX = '+593';
    const DEV_FRONT_REMOTE_IP = '*';

    const CURRENT_ACCOUNT = 'Corriente';
    const SAVING_ACCOUNT = 'Ahorros';

    const EMAIL_MASTER = 'notificaciones@youneed.com.ec';
    const NAME_MASTER = 'Youneed';

    const EMAIL_TESTING = 'unrealmach@gmail.com';
    const SMS_TESTING = '0996683742';

    const EN_ESPERA = 'EN_ESPERA';
    const ACEPTADO = 'ACEPTADO';
    const RECHAZADO = 'RECHAZADO';
    const CONFIRMADO = 'CONFIRMADO';
    const TERMINADO = 'TERMINADO';
    const PAGADO = 'PAGADO';
    const CANCELADO = 'CANCELADO';

    //descripciones para eventos
    const SOLICITUD_NUEVA = 'solicitud nueva';
    const SOLICITUD_ACEPTADA = 'solicitud aceptada';
    const SOLICITUD_EN_EJECUCION = 'solicitud en ejecucion';
    const SOLICITUD_CANCELADA = 'solicitud cancelada';
    const SOLICITUD_PAGADA = 'solicitud pagada';
    const SOLICITUD_RECHAZADA = 'solicitud rechazada';
    const SOLICITUD_CONFIRMADA = 'solicitud confirmada';
    const SOLICITUD_TERMINADA = 'solicitud terminada';
    const SOLICITUD_REAGENDADA = 'solicitud reagendada';


    /**
     * Get an id status filtered by the name of orders in youneedcontext
     * @param $searchStatus
     * @return mixed
     */
    public static function getIdStatusOrder($searchStatus)
    {
        foreach (Yii::$app->params['estados_pedidos'] as $key => $pedido) {
            if ($searchStatus == $pedido) {
                return $key;
            }
        }
        return false;
    }


    /**
     * Get the type of account to be used in select2 data
     * @return mixed
     */
    public static function getTiposCuenta()
    {
        $out[] = ['id' => self::CURRENT_ACCOUNT, 'value' => self::CURRENT_ACCOUNT];
        $out[] = ['id' => self::SAVING_ACCOUNT, 'value' => self::SAVING_ACCOUNT];

        return $out;
    }

    /**
     * Get the type of formaPagos to be used in select2 data
     * @return array
     */
    public static function getTiposFormaPagos()
    {
        $formasPago = Yii::$app->params['forma_pago'];
        $out=[];
        foreach ($formasPago as $key => $value){
            $out[] = ['id' => $key, 'value' => $value];
        }
        return $out;
    }

    /**
     * Get the type of status to be used in select2 data
     * @return array
     */
    public static function getTiposEstados(){
        $formasPago = Yii::$app->params['estados_pedidos'];
        $out=[];
        foreach ($formasPago as $key => $value){
            $out[] = ['id' => $key, 'value' => $value];
        }
        return $out;
    }

    /**
     * Get the type of attention to be used in select2 data
     * @return array
     */
    public static function getTiposAtencion(){
        $formasPago = Yii::$app->params['tipo_atencion'];
        $out=[];
        foreach ($formasPago as $key => $value){
            $out[] = ['id' => $key, 'value' => $value];
        }
        return $out;
    }
}