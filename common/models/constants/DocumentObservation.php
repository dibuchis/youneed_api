<?php

namespace app\common\models\constants;

/**
 * Class DocumentObservation helps to define the message to send in observations account
 * @package app\common\models\constants
 */
class DocumentObservation
{
    //errors
    const NUMBER_DNI_ERROR = '[E1] La cédula no corresponde al número registrado.';
    const MAYOR_CERTIFICATE_ERROR = '[E2] El Certificado o Título que acreditan sus conocimientos no cumple con los requisitos.';
    const PICTURE_DNI_ERROR = "[E3] La fotografía de su perfil no cumple con las reglas establecidas  [Regla:  foto tipo carnet  del rostro de frente].";
    const INCORRECT_RUC_RISE_ERROR = '[E4] El documento de RUC/RISE no corresponde al número registrado.';
    const DISABLED_RUC_RISE_ERROR = '[E5] El RUC/RISE se encuentra suspendido o inhabilitado.';
    const PASSPORT_INCONSISTENCIES_ERROR = '[E6] Pasaporte o Visa de Trabajo no se encuentra activo/actualizado.';
    const MISSING_CERTIFICATE_ERROR = '[E7] Falta incluir Certificado o Título que acredite sus conocimientos en esta especialidad.';
    const BREACH_OF_RULES ='[E8]¡! Estimado Asociado ¡!. Hemos detectado incumplimiento en los acuerdos y políticas del uso
    de la aplicación, por lo que conforme a la normativa interna de la empresa, se procede con la
    DESACTIVACIÓN DE TU REGISTRO COMO ASOCIADO DE YOUNEED.';
    const LAYOUT_ERROR_MESSAGE_1 = ' ¡¡Bienvenido a tu Aplicación de Muti Servicios¡¡ MAS INGRESOS Y BENEFICIOS PARA TI.
    Estamos en proceso de Validación de tu registro, hemos detectado los siguientes
    inconvenientes: ^  Ingresa a la APP mediante este link para completar la información:
    https://youneed.com.ec/app/login.php;';


    //success
    const WELCOME_MESSAGE = ' ¡¡Bienvenido a tu Aplicación de Muti Servicios¡¡ MAS INGRESOS Y BENEFICIOS PARA TI. 
    Tu registro ha sido ACTIVADO “MUY PRONTO LOS CLIENTES SOLICITARAN TUS SERVICIOS” 
    Ingresa a la APP mediante este link :: https://youneed.com.ec/app/login.php';


    public static function getErrorList()
    {
        $output=[];
        $output[] = ['id' => self::NUMBER_DNI_ERROR, 'value' => self::NUMBER_DNI_ERROR];
        $output[] = ['id' => self::MAYOR_CERTIFICATE_ERROR, 'value' => self::MAYOR_CERTIFICATE_ERROR];
        $output[] = ['id' => self::PICTURE_DNI_ERROR, 'value' => self::PICTURE_DNI_ERROR];
        $output[] = ['id' => self::INCORRECT_RUC_RISE_ERROR, 'value' => self::INCORRECT_RUC_RISE_ERROR];
        $output[] = ['id' => self::DISABLED_RUC_RISE_ERROR, 'value' => self::DISABLED_RUC_RISE_ERROR];
        $output[] = ['id' => self::PASSPORT_INCONSISTENCIES_ERROR, 'value' => self::PASSPORT_INCONSISTENCIES_ERROR];
        $output[] = ['id' => self::MISSING_CERTIFICATE_ERROR, 'value' => self::MISSING_CERTIFICATE_ERROR];
        $output[] = ['id' => self::BREACH_OF_RULES, 'value' => self::BREACH_OF_RULES];

        return $output;
    }

    public static function getLayoutErrorList(){
        $output=[];
        $output[] = ['id' => self::LAYOUT_ERROR_MESSAGE_1, 'value' => self::LAYOUT_ERROR_MESSAGE_1];

        return $output;
    }

    public static function getSuccessMessage(){
        $output=[];
        $output[] = ['id' => self::WELCOME_MESSAGE, 'value' => self::WELCOME_MESSAGE];

        return $output;
    }
}