<?php


namespace app\common\models\DTO;

/**
 * Class CompleteInfoPedido helps to make and object to provide secure data of the pedido
 * @package app\common\models\DTO
 */
class CompleteInfoPedido
{
    private $_pedido;
    private$_cliente;
    private$_asociado;
    private $_forma_pago;
    private $_tipo_atencion;
    private $_estado;
    private $_ciudad;
    private $_servicio;

    public function __construct()
    {
        $this->_pedido=NULL;
        $this->_cliente=NULL;
        $this->_asociado=NULL;
        $this->_forma_pago=NULL;
        $this->_tipo_atencion=NULL;
        $this->_estado=NULL;
        $this->_ciudad=NULL;
        $this->_servicio=NULL;
    }

    /**
     * @param null $pedido
     */
    public function setPedido($pedido): void
    {
        $this->_pedido = $pedido;
    }

    /**
     * @param null $cliente
     */
    public function setCliente($cliente): void
    {
        $this->_cliente = $cliente;
    }

    /**
     * @param null $asociado
     */
    public function setAsociado($asociado): void
    {
        $this->_asociado = $asociado;
    }

    /**
     * @param null $forma_pago
     */
    public function setFormaPago($forma_pago): void
    {
        $this->_forma_pago = $forma_pago;
    }

    /**
     * @param null $tipo_atencion
     */
    public function setTipoAtencion($tipo_atencion): void
    {
        $this->_tipo_atencion = $tipo_atencion;
    }

    /**
     * @param null $estado
     */
    public function setEstado($estado): void
    {
        $this->_estado = $estado;
    }

    /**
     * @param null $ciudad
     */
    public function setCiudad($ciudad): void
    {
        $this->_ciudad = $ciudad;
    }

    /**
     * @param null $servicio
     */
    public function setServicio($servicio): void
    {
        $this->_servicio = $servicio;
    }

    public function getInfoPedido()
    {
        return [
            'pedido'=>$this->_pedido,
            'cliente'=>$this->_cliente,
            'asociado'=>$this->_asociado,
            'forma_pago'=>$this->_forma_pago,
            'tipo_atencion'=>$this->_tipo_atencion,
            'estado'=>$this->_estado,
            'servicio'=>$this->_servicio,
        ];
    }

}