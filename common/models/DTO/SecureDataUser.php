<?php


namespace app\common\models\DTO;

/**
 * Class SecureDataUser helps to make and object to provide secure data of user
 * @package app\common\models\DTO
 */
class SecureDataUser
{
    private $_tipo_identificacion;
    private $_identificacion;
    private $_imagen;
    private $_nombres;
    private $_apellidos;
    private $_email;
    private $_estado;
    private $_bloqueo;
    private $_extra_info;
    private $_numero_celular;
    private $_telefono_domicilio;

    public function __construct()
    {
        $this->_tipo_identificacion = NULL;
        $this->_identificacion = NULL;
        $this->_imagen = NULL;
        $this->_nombres = NULL;
        $this->_apellidos = NULL;
        $this->_email = NULL;
        $this->_estado = NULL;
        $this->_bloqueo = NULL;
        $this->_extra_info = NULL;
        $this->_numero_celular = NULL;
        $this->_telefono_domicilio = NULL;
    }

    public function getSecureDataUser()
    {
        return [
            'tipo_identificacion' => $this->_tipo_identificacion,
            'identificacion' => $this->_identificacion,
            'imagen' => $this->_imagen,
            'nombres' => $this->_nombres,
            'apellidos' => $this->_apellidos,
            'email' => $this->_email,
            'estado' => $this->_estado,
            'bloqueo' => $this->_bloqueo,
            'extra_info' => $this->_extra_info,
            'numero_celular' => $this->_numero_celular,
            'telefono_domicilio' => $this->_telefono_domicilio,
        ];
    }

    public function extractDataFromModel($model)
    {
        if (isset($model->attributes)) {
            $this->setEstado($model->estado);
            $this->setTipoIdentificacion($model->tipo_identificacion);
            $this->setIdentificacion($model->identificacion);
            $this->setNombres($model->nombres);
            $this->setApellidos($model->apellidos);
            $this->setBloqueo($model->bloqueo);
            $this->setEmail($model->email);
            $this->setImagen($model->imagen);
            $this->setNumeroCelular($model->numero_celular);
            $this->setTelefonoDomicilio($model->telefono_domicilio);
        }
        return $this->getSecureDataUser();
    }

    /**
     * @param null $tipo_identificacion
     */
    public function setTipoIdentificacion($tipo_identificacion): void
    {
        $this->_tipo_identificacion = $tipo_identificacion;
    }

    /**
     * @param null $identificacion
     */
    public function setIdentificacion($identificacion): void
    {
        $this->_identificacion = $identificacion;
    }

    /**
     * @param null $imagen
     */
    public function setImagen($imagen): void
    {
        $this->_imagen = $imagen;
    }

    /**
     * @param null $nombres
     */
    public function setNombres($nombres): void
    {
        $this->_nombres = $nombres;
    }

    /**
     * @param null $apellidos
     */
    public function setApellidos($apellidos): void
    {
        $this->_apellidos = $apellidos;
    }

    /**
     * @param null $email
     */
    public function setEmail($email): void
    {
        $this->_email = $email;
    }

    /**
     * @param null $estado
     */
    public function setEstado($estado): void
    {
        $this->_estado = $estado;
    }

    /**
     * @param null $bloqueo
     */
    public function setBloqueo($bloqueo): void
    {
        $this->_bloqueo = $bloqueo;
    }

    /**
     * @param null $extra_info
     */
    public function setExtraInfo($extra_info): void
    {
        $this->_extra_info = $extra_info;
    }

    /**
     * @param null $telefono_domicilio
     */
    public function setTelefonoDomicilio($telefono_domicilio): void
    {
        $this->_telefono_domicilio = $telefono_domicilio;
    }

    /**
     * @param null $numero_celular
     */
    public function setNumeroCelular($numero_celular): void
    {
        $this->_numero_celular = $numero_celular;
    }



}