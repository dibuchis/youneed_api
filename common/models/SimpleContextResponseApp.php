<?php


namespace app\common\models;


use app\common\utils\UtilCommunication;

/**
 * Class SimpleContextResponseApp help to make a response to the APP
 * @author Mauricio Chamorro unrealmach@gmail.com
 * @package app\common\models
 */
class SimpleContextResponseApp extends SimpleContextResponse
{
    public function setContext($_status, $_message, $_data = NULL)
    {
        UtilCommunication::setHeader(200);
        return ['status' => $_status, 'message' => $_message, 'data' => $_data];
    }
}