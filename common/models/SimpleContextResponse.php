<?php

namespace app\common\models;

/**
 * Class SimpleContextResponse help to make a response
 * @author Mauricio Chamorro unrealmach@gmail.com
 * @package app\common\models
 */
class SimpleContextResponse
{
    private $_message;
    private $_status;
    private $_data;

    public function __construct()
    {
    }

    public function setContext($_status, $_message, $_data = NULL)
    {
        return ['status' => $_status, 'message' => $_message, 'data' => $_data];
    }


}