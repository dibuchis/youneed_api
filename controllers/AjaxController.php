<?php

namespace app\controllers;

use app\common\models\constants\EmailContext;
use app\common\models\constants\YouNeedContext;
use app\common\utils\DataTransformation;
use app\common\utils\EmailComposer;
use app\models\Bancos;
use Yii;
use yii\base\View;
use yii\db\Query;
use yii\helpers\ArrayHelper as ArrayHelperAlias;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\UploadedFile;
use app\models\Categorias;
use app\models\Servicios;
use app\models\Medidas;
use app\models\CategoriasServicios;
use app\models\UsuariosServicios;
use app\models\Notificaciones;
use app\models\Pedidos;
use app\models\Util;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;
use app\models\Trazabilidades;
use app\models\Usuarios;
use app\models\Traccar;
use app\models\Paises;
use app\models\Ciudades;

/**
 * EmpresasController implements the CRUD actions for Empresas model.
 */
class AjaxController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionTestemail()
    {
        //EmailComposer::sendEmail(EmailContext::SEND_TEST);
        //echo "<script>console.log('" . $send . "');</script>";
		$srv_id = 79;
		$orden = 'nombres';
		$rows = 10;
		$offset = 0;
		$ciudad = 16;
		$filter = " AND usuarios.ciudad_id = " . $ciudad . " ";
		
		$asociados = UsuariosServicios::findBySql("SELECT COUNT(*) as cuenta FROM usuarios_servicios LEFT JOIN usuarios ON usuarios.id = usuarios_servicios.usuario_id LEFT JOIN ciudades ON ciudades.id = usuarios.ciudad_id WHERE usuarios_servicios.servicio_id in (" . $srv_id . ") " . $filter . " ORDER BY usuarios." . $orden . " LIMIT " . $rows . " OFFSET " . $offset )->asArray()->all();
		 var_dump($asociados);
		 return null;
    }


    public function actionSubirimagencategorias()
    {
        $model = new Categorias();
        $image = UploadedFile::getInstance($model, 'imagen_upload');
        if ($image) {
            $model->imagen = Util::getGenerarPermalink(Yii::$app->security->generateRandomString()) . '.jpg';
            $path = Yii::getAlias('@webroot') . Yii::$app->params['uploadImages'] . $model->imagen;
            $pathweb = Yii::getAlias('@web') . Yii::$app->params['uploadImages'] . $model->imagen;
            if ($image->saveAs($path)) {

                $thumbnail = Image::thumbnail($path, 300, 300);
                $size = $thumbnail->getSize();
                // if ($size->getWidth() < 250 or $size->getHeight() < 150) {
                $white = Image::getImagine()->create(new Box(300, 300));
                $thumbnail = $white->paste($thumbnail, new Point(300 / 2 - $size->getWidth() / 2, 300 / 2 - $size->getHeight() / 2));
                // }
                $thumbnail->save(Yii::getAlias($path), ['quality' => 70]);

                $imageData = base64_encode(file_get_contents($path));

                return Json::encode([
                    [
                        'name' => $model->imagen,
                        'size' => $image->size,
                        'url' => $pathweb,
                        'thumbnailUrl' => $path,
                        // 'deleteUrl' => 'image-delete?name=' . $fileName,
                        'deleteType' => 'POST',
                        'base64' => 'data:' . mime_content_type($path) . ';base64,' . $imageData,
                    ],
                ]);
            }
        }
        return '';
    }

    public function actionSubirimagenservicios()
    {
        $model = new Servicios();
        $image = UploadedFile::getInstance($model, 'imagen_upload');
        if ($image) {
            $model->imagen = Util::getGenerarPermalink(Yii::$app->security->generateRandomString()) . '.jpg';
            $path = Yii::getAlias('@webroot') . Yii::$app->params['uploadImages'] . $model->imagen;
            $pathweb = Yii::getAlias('@web') . Yii::$app->params['uploadImages'] . $model->imagen;
            if ($image->saveAs($path)) {

                $thumbnail = Image::thumbnail($path, 300, 300);
                $size = $thumbnail->getSize();
                // if ($size->getWidth() < 250 or $size->getHeight() < 150) {
                $white = Image::getImagine()->create(new Box(300, 300));
                $thumbnail = $white->paste($thumbnail, new Point(300 / 2 - $size->getWidth() / 2, 300 / 2 - $size->getHeight() / 2));
                // }
                $thumbnail->save(Yii::getAlias($path), ['quality' => 70]);

                $imageData = base64_encode(file_get_contents($path));

                return Json::encode([
                    [
                        'name' => $model->imagen,
                        'size' => $image->size,
                        'url' => $pathweb,
                        'thumbnailUrl' => $path,
                        // 'deleteUrl' => 'image-delete?name=' . $fileName,
                        'deleteType' => 'POST',
                        'base64' => 'data:' . mime_content_type($path) . ';base64,' . $imageData,
                    ],
                ]);
            }
        }
        return '';
    }

    public function actionGetservicio()
    {
        $out = [];
        if (isset($_REQUEST['serviceID'])) {
            $serviceID = $_REQUEST['serviceID'];
            $cat_id = CategoriasServicios::find()->where(['servicio_id' => $serviceID])->one();
            $servicio = Servicios::find()->where(['id' => $serviceID])->asArray()->one();
			$medida = Medidas::find()->where(['id' => $servicio['medida_id']])->one();
			//foreach($servicio as $serv){
            //}
			$out = $servicio;
			
			// $out["medida"] = Medidas::find()->where(['id' => $servicio->medida_id]);
			$out["medida"] = $medida->descripcion;
            $out["cat_id"] = $cat_id->categoria_id;
            // return Json::encode(['output'=>$out, 'selected'=>'']);
            return Json::encode(['servicio' => $out]);
        }
    }

    public function actionListadocategorias()
    {
        $categorias = [];

        $categorias = Categorias::find()
            ->all();

        if (isset($_REQUEST['ordenado'])) {
            $categorias = Categorias::find()
                ->orderBy('nombre ASC')
                ->all();
        }

        foreach ($categorias as $cat) {
            $categorias [] = $cat->attributes;
        }
        return Json::encode(['output' => $categorias]);
        //return Json::encode(['output'=>'', 'Seleccione'=>'']);
    }

    public function actionListadoasociados()
    {
        $asociados = [];
        $rows = 10;
        $offset = 0;
		
		$filter = '';
		$orden = 'id';
		
		$ciudad = 0;
		
		if(isset($_REQUEST['filtro_orden'])){
			$orden = $_REQUEST['filtro_orden'];
		}
		
		if(isset($_REQUEST['filtro_ciudad'])){
			$ciudad = $_REQUEST['filtro_ciudad'];
			if($ciudad > 0){ $filter .= " AND usuarios.ciudad_id = " . $ciudad . " "; }
		}

        if (isset($_REQUEST['srv_id'])) {
            $srv_id = $_REQUEST['srv_id'];
            if ($srv_id != null) {

                if (isset($_REQUEST['page'])) {
                    $offset = ($_REQUEST['page'] * $rows) - $rows;
                }
                

                $_total = UsuariosServicios::findBySql("SELECT COUNT(*) as cuenta FROM usuarios_servicios LEFT JOIN usuarios ON usuarios.id = usuarios_servicios.usuario_id LEFT JOIN ciudades ON ciudades.id = usuarios.ciudad_id WHERE usuarios_servicios.servicio_id in (" . $srv_id . ") " . $filter . " ORDER BY usuarios." . $orden )->asArray()->all();
                
				$total = $_total[0]['cuenta'];

                $pages = ceil($total / $rows);

                /*$asociados = UsuariosServicios::find()
					->asArray()
					->select(['usuarios_servicios.id', 'usuarios_servicios.usuario_id', 'usuarios_servicios.servicio_id', 'usuarios.id', 'usuarios.nombres', 'usuarios.imagen', 'usuarios.estado', 'ciudades.id', 'ciudades.nombre as ciudad'])
					->leftJoin('usuarios', 'usuarios.id = usuarios_servicios.usuario_id')
					//->leftJoin('ciudades', 'ciudades.id = usuarios.ciudad_id')
                    ->andWhere(['in', 'usuarios_servicios.servicio_id', $srv_id])
                    ->orderBy(['usuarios.' . $orden => SORT_ASC])
                    ->limit($rows)
                    ->offset($offset)
                    ->all();*/
				
				$asociados = UsuariosServicios::findBySql("SELECT usuarios.id as id, usuarios_servicios.id as usid, usuarios_servicios.usuario_id, usuarios_servicios.servicio_id, usuarios.id, usuarios.nombres, usuarios.imagen, usuarios.estado, ciudades.id as cid, ciudades.nombre as ciudad FROM usuarios_servicios LEFT JOIN usuarios ON usuarios.id = usuarios_servicios.usuario_id LEFT JOIN ciudades ON ciudades.id = usuarios.ciudad_id WHERE usuarios_servicios.servicio_id in (" . $srv_id . ") " . $filter . " ORDER BY usuarios." . $orden . " LIMIT " . $rows . " OFFSET " . $offset )->asArray()->all();

                return Json::encode([
                    'asociados' => $asociados,
                    'total' => $total,
                    'pages' => $pages,
                    'offset' => $offset,
                    'rows' => $rows
                ]);
                //return;
            }
        }
        return Json::encode(['output' => '', 'Seleccione' => '']);
    }

    public function actionListadoservicios()
    {
        $out = [];
        if (isset($_REQUEST['depdrop_parents'])) {
            $parents = $_REQUEST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents;
                $servicios = CategoriasServicios::find()
                    ->joinWith('servicio', true, ' INNER JOIN ')
                    ->andWhere(['servicios.mostrar_app' => 1])
                    ->andWhere(['servicios.tiene_soft_delete' => 0])
                ;

                if ($cat_id == 0) {
                    $servicios = $servicios->all();
                } else if ($cat_id > 0) {
                    $servicios = $servicios
                        ->andWhere(['in', 'categoria_id', $cat_id])
                        ->all();
                }


                if (isset($_REQUEST['ordenado'])) {
                    $servicios = CategoriasServicios::findBySql(
                        'SELECT cs.id, cs.categoria_id, cs.servicio_id FROM categorias_servicios cs, servicios s '.
                            'WHERE cs.categoria_id in(' . $cat_id . ') AND s.id = cs.servicio_id '.
                            //'AND s.mostrar_app==1 AND s.tiene_soft_delete==0 '.
                            'ORDER BY s.nombre')
                        ->all();
                }

                foreach ($servicios as $servicio) {
                    // $out [] = ['id'=>$servicio->servicio_id, 'name'=>strip_tags($servicio->servicio->nombre)];
                    $out [] = ['id' => $servicio->servicio_id, 'parent' => $cat_id, 'imagen' => $servicio->servicio->imagen, 'nombre' => $servicio->servicio->nombre, 'precio' => $servicio->servicio->total, 'body' => '<img src="' . $servicio->servicio->imagen . '"><span>' . strip_tags($servicio->servicio->nombre) . '</span><btn class="btn btn-vermas btn-sm center-block" data-srv="' . $servicio->servicio_id . '">Conocer más</btn><btn class="btn btn-success btn-sm center-block btn_add_service" data-srv="' . $servicio->servicio_id . '" data-cat="' . $cat_id . '" data-name="' . strip_tags($servicio->servicio->nombre) . '" >Escoger</btn>'];
                    // $out [] = ['item'=>'<div class="serv-item" data-id="' . $servicio->servicio_id . '"><img src="' . $servicio->servicio->imagen . '"><span>' . strip_tags($servicio->servicio->nombre) . '</span></div>'];
                }
                // return Json::encode(['output'=>$out, 'selected'=>'']);
                return Json::encode(['output' => $out]);

            }
        }
        return Json::encode(['output' => '', 'Seleccione' => '']);
    }

    public function actionContarasociados()
    {

        if (isset($_GET['srv_id'])) {
            $srv_id = $_GET['srv_id'];
            $servicio = Servicios::findOne($srv_id);
            $count = (new Query())
                ->from('usuarios_servicios')
                ->where(['servicio_id' => $srv_id])
                ->count();

            return Json::encode(['count' => $count, 'nombre_servicio' => $servicio->nombre]);
        } else {
            return Json::encode(['count' => 0, 'nombre_servicio' => $servicio->nombre]);
        }
    }

    public function actionVerasociado()
    {

        if (isset($_REQUEST['api_token'])) {

            if (isset($_REQUEST['aso_id']) && $_REQUEST['api_token'] == Yii::$app->params['api_token']) {
                $aso_id = $_REQUEST['aso_id'];
                $asociado = Usuarios::find()
                    ->where(['id' => $aso_id])
                    ->select([
                        'id',
                        'imagen',
                        'nombres',
                        'apellidos',
                        'estado',
                        'jornada_trabajo',
                        'observaciones',
                        'pais_id',
                        'ciudad_id'
                    ])
                    ->asArray()
                    ->one();

                $asociado['estado'] = Yii::$app->params['estados_genericos'][$asociado['estado']];

                $asociado['pais'] = Paises::findOne($asociado['pais_id']);

                $asociado['ciudad'] = Ciudades::findOne($asociado['ciudad_id']);
                // $asociado = Usuarios::findOne()->one();

                return Json::encode($asociado);
            } else {
                return Json::encode(['id' => null]);
            }
        } else {
            return Json::encode(['id' => null]);
        }
    }

    public function actionSubirfotografia()
    {
        $model = new Usuarios();
        $image = UploadedFile::getInstance($model, 'imagen_upload');
        if ($image) {
            $model->imagen = Util::getGenerarPermalink(Yii::$app->security->generateRandomString()) . '.jpg';
            $path = Yii::getAlias('@webroot') . Yii::$app->params['uploadImages'] . $model->imagen;
            $pathweb = Yii::getAlias('@web') . Yii::$app->params['uploadImages'] . $model->imagen;
            if ($image->saveAs($path)) {

                $thumbnail = Image::thumbnail($path, 400, 400);
                $size = $thumbnail->getSize();
                // if ($size->getWidth() < 250 or $size->getHeight() < 150) {
                $white = Image::getImagine()->create(new Box(400, 400));
                $thumbnail = $white->paste($thumbnail, new Point(400 / 2 - $size->getWidth() / 2, 400 / 2 - $size->getHeight() / 2));
                // }
                $thumbnail->save(Yii::getAlias($path), ['quality' => 75]);

                $imageData = base64_encode(file_get_contents($path));

                return Json::encode([
                    [
                        'name' => $model->imagen,
                        'size' => $image->size,
                        'url' => $pathweb,
                        'thumbnailUrl' => $path,
                        // 'deleteUrl' => 'image-delete?name=' . $fileName,
                        'deleteType' => 'POST',
                        'base64' => 'data:' . mime_content_type($path) . ';base64,' . $imageData,
                    ],
                ]);
            }
        }
        return '';
    }

    public function actionSubirdocumento($atributo_upload, $atributo_modelo)
    {
        $model = new Usuarios();
        $file = UploadedFile::getInstance($model, $atributo_upload);
        if ($file) {
            $model->$atributo_modelo = Util::getGenerarPermalink(Yii::$app->security->generateRandomString()) . '.' . $file->getExtension();
            $path = Yii::getAlias('@webroot') . Yii::$app->params['uploadFiles'] . $model->$atributo_modelo;
            $pathweb = Yii::getAlias('@web') . Yii::$app->params['uploadFiles'] . $model->$atributo_modelo;

            if ($file->saveAs($path)) {

                return Json::encode([
                    [
                        'name' => $model->$atributo_modelo,
                        'size' => $file->size,
                        'url' => $pathweb,
                        'thumbnailUrl' => $path,
                        // 'deleteUrl' => 'image-delete?name=' . $fileName,
                        'deleteType' => 'POST',
                    ],
                ]);
            }
        }
        return '';
    }

    /*public function actionProvincias()
    {
        $out = [];
                $adicionales = Provincias::find()->orderBy(['nombre' => SORT_ASC])->all();
                foreach ($adicionales as $adicional) {
                    $out [] = ['id' => $adicional->id, 'nombre' => $adicional->nombre];
                }
                return Json::encode(['output' => $out, 'selected' => '']);
    }*/
	
	public function actionCiudades()
    {
        $out = [];
        if (isset($_REQUEST['depdrop_parents'])) {
            $parents = $_REQUEST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $adicionales = Ciudades::find()->andWhere(['pais_id' => $cat_id])->all();
                foreach ($adicionales as $adicional) {
                    $out [] = ['id' => $adicional->id, 'name' => $adicional->nombre, 'nombre' => $adicional->nombre];
                }
                return Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        return Json::encode(['output' => '', 'Seleccione' => '']);
    }

    /**
     * Get the information to save in pagos
     * @return array
     */

    public function actionGetDataPagos()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $output = [
            'instituciones' => DataTransformation::transformToSelect2(ArrayHelperAlias::map(
                Bancos::find()->orderBy('nombre')->asArray()->all(), 'id', 'nombre'
            )),
            'tipo_cuenta' => DataTransformation::transformToSelect2(ArrayHelperAlias::map(
                YouNeedContext::getTiposCuenta(), 'id', 'value'
            )),
            'tipo_ruc' => DataTransformation::transformToSelect2(Yii::$app->params['tipo_ruc'])
        ];
        return ['output' => $output, 'selected' => ''];
    }

    /**
     * Get a list of document types
     * @return array
     */
    public function actionGetTipoIdentificacion()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $output = [
            'tipo_identificacion' => DataTransformation::transformToSelect2(Yii::$app->params['tipo_identificacion'])
        ];
        return ['output' => $output, 'selected' => ''];
    }

    /**
     * Get a list of document types
     * @return array
     */
    public function actionGetTiposDocumentos()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $output = [
            'tipos_documentos' => Yii::$app->params['tipos_documentos']
        ];
        return ['output' => $output, 'selected' => ''];
    }

}
