<?php

namespace app\controllers;

use app\common\models\constants\EmailContext;
use app\common\models\constants\YouNeedContext;
use app\common\utils\EmailComposer;
use app\models\Util;
use Yii;
use app\models\Usuarios;
use app\models\UsuariosSearch;
use yii\db\Expression;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\widgets\ActiveForm;

/**
 * UsuariosController implements the CRUD actions for Usuarios model.
 */
class UsuariosController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete', 'bulkdelete', 'clientes',
                    'asociados',],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuarios models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuariosSearch();
        $dataProvider = $searchModel->searchSuperadmin(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionClientes()
    {
        $searchModel = new UsuariosSearch();
        $dataProvider = $searchModel->searchClientes(Yii::$app->request->queryParams);

        return $this->render('clientes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAsociados()
    {
        $request = Yii::$app->request;
        $searchModel = new UsuariosSearch();
        $dataProvider = $searchModel->searchAsociados(Yii::$app->request->queryParams);


        if ($request->isAjax) {
            return $this->renderAjax('asociados', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        return $this->render('asociados', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Usuarios model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Usuarios #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Usuarios model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Usuarios();
        $model->scenario = YouNeedContext::WEBAPP_SCENARIO;

        $usuario_tipo = Yii::$app->request->get('tipo_usuario');

        switch ($usuario_tipo) {
            case 'es_cliente':
            {
                $model->es_cliente = 1;
                $model->es_asociado = 0;
                $model->es_operador = 0;
                $model->es_super = 0;
                break;
            }
            default:
                $model->es_cliente = 0;
                $model->es_asociado = 0;
                $model->es_operador = 0;
                $model->es_super = 0;
                break;
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Crear nuevo  Usuario",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post())) {

                $model->numero_celular = preg_replace('/\s+/', '', $model->numero_celular);
                $model->numero_celular = str_replace(' ', '', $model->numero_celular);

                //  $model->es_super = 1;
                if ($model->save()) {
                    $model->clave = Yii::$app->getSecurity()->generatePasswordHash($model->clave);
                    $model->save();

                    return [
                        'forceReload' => '#crud-datatable-clientes-pjax',
                        'title' => "Create new Usuarios",
                        'content' => '<span class="text-success">Create Usuarios success</span>',
                        'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Create More', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                } else {
                    return [
                        'title' => "Create new Usuarios",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            } else {

                return [
                    'title' => "Crear nuevo Cliente",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Usuarios model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->scenario = YouNeedContext::WEBAPP_SCENARIO;
        $target_pjax_datatable = '#crud-datatable';
        if ($model->es_asociado == 1) {
            $target_pjax_datatable ="#crud-datatable-asociados-pjax";
        }
        if ($model->es_cliente == 1)
            $target_pjax_datatable = "#crud-datatable-clientes-pjax";

        $modelOld = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Update Usuarios #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {

                if ($model->estado == 0) {
                    $model->fecha_desactivacion = date('Y-m-d H:i:s');
                    $model->activacion = null;
                    $model->save();
                } else {
                    $model->fecha_desactivacion = null;
                    $model->activacion = date('Y-m-d H:i:s');
                    $model->save();
                }

                if ($modelOld->clave != $model->clave) {
                    $model->clave = Yii::$app->getSecurity()->generatePasswordHash($model->clave);
                    $model->save();
                }
                return [
                    'forceReload' => $target_pjax_datatable,
                    'title' => "Usuarios #" . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Update Usuarios #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionCreateAsociado()
    {
        $request = Yii::$app->request;
        $model = new Usuarios();
        $model->scenario = YouNeedContext::LAGGART_ASSOCIATE_SCENARIO;
        $model->estado = 0; // Inactivo (puede logear y actualizar datos pero no realizar transacciones ni aparecer en busqueda)
        $model->bloqueo = 0; // Desbloqueado (logesrse, actualizar) -> inactivo
        $model->estado_validacion_documentos = 0; // NO validados
        $usuario_tipo = Yii::$app->request->get('tipo_usuario');
        $target_pjax_datatable = '#crud-datatable';
        if ($model->es_asociado == 1) {
            $target_pjax_datatable ="#crud-datatable-asociados-pjax";
        }
        if ($model->es_cliente == 1)
            $target_pjax_datatable = "#crud-datatable-clientes-pjax";

        switch ($usuario_tipo) {
            case 'es_asociado':
            {
                $model->es_cliente = 0;
                $model->es_asociado = 1;
                $model->es_operador = 0;
                $model->es_super = 0;
                break;
            }
            default:
                $model->es_cliente = 0;
                $model->es_asociado = 0;
                $model->es_operador = 0;
                $model->es_super = 0;
                break;
        }


        //$modelOld = $this->findModel($id);

        //   if($request->isAjax){
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "create Usuarios #",
                    'content' => $this->renderAjax('create_asociados', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post())) {

                if ($model->estado == 0) {
                    $model->fecha_desactivacion = date('Y-m-d H:i:s');
                    $model->activacion = null;
                } else {
                    $model->fecha_desactivacion = null;
                    $model->activacion = date('Y-m-d H:i:s');
                }

                $clave = $model->clave;
                $model->clave = Yii::$app->getSecurity()->generatePasswordHash($clave);
                if ($model->validate() && $model->save()) {
                    $model->clave = Yii::$app->getSecurity()->generatePasswordHash($clave);
                    $model->update();
                    return [
                        'forceReload' => $target_pjax_datatable,
                        'title' => "Usuarios #",
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Edit', ['update', 'id' => ''], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {

                    return [
                        'title' => "Create Usuarios #",
                        'content' => $this->renderAjax('create_asociados', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }

            } else {
                return [
                    'title' => "Create Usuarios #",
                    'content' => $this->renderAjax('create_asociados', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        }
    }

    /**
     * Delete an existing Usuarios model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        Util::borrarRegistrosRecursivos($model);
        $target_pjax_datatable = '#crud-datatable';
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->es_asociado == 1) {
                $target_pjax_datatable ="#crud-datatable-asociados-pjax";
            }
            if ($model->es_cliente == 1)
                $target_pjax_datatable = "#crud-datatable-clientes-pjax";

            return ['forceClose' => true, 'forceReload' => $target_pjax_datatable];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Usuarios model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-clientes-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Usuarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuarios::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    //----------------------------------------------- AJAX --------------------------


    public function actionAjaxSendEmail()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = Yii::$app->request->post();

        if ($post['type'] == 'withErrors') {
            return EmailComposer::sendEmail(EmailContext::SEND_OBSERVATIONS, $post);
        } else {
            return EmailComposer::sendEmail(EmailContext::SEND_REVIEW_OK, $post);
        }
    }

    /*
     * Alimenta el select2 del header de la lista de pedidos
     */
    public function actionAjaxGetUsuarios($q = null, $tipo, $targetField)
    {
        $q = strtoupper($q);

        Yii::$app->response->format = Response::FORMAT_JSON;
        $query1 = new Query;
        $query1->select([
            new Expression("UPPER(CONCAT(nombres,' ',apellidos )) AS nombreCompleto "),
            new Expression(" id "),
            new Expression("UPPER(CONCAT(nombres,' ',apellidos )) AS  text "),
        ]);
        $query1->from('usuarios');

        if ($tipo == 'CLIENTE') {
            $query1->where(" es_cliente = 1");
        }

        if ($tipo == 'ASOCIADO') {
            $query1->where(" es_asociado = 1");
        }

        if ($targetField == 'nombres') {
            if (!is_null($q)) {
                $query1->andWhere("UPPER(CONCAT(nombres,' ',apellidos )) LIKE '%$q%'");
            }
        } else if ($targetField == 'multiple_id') {
            if (!is_null($q)) {
                $query1->andWhere("id like '%$q%'");
            }
        }

        $query1->limit(10);
        $command = $query1->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);
        return $out;
    }

    public function actionAjaxAsociadosBancos()
    {
        $request = Yii::$app->request;
        $searchModel = new UsuariosSearch();
        $dataProvider = $searchModel->searchAsociados(Yii::$app->request->queryParams);
        if ($request->isAjax) {
            return $this->renderAjax('asociadosBancos', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        return $this->render('asociadosBancos', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
