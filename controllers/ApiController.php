<?php

namespace app\controllers;

use app\common\models\constants\EmailContext;
use app\common\models\constants\ResponseConstants;
use app\common\models\constants\SMSContext;
use app\common\models\constants\YouNeedContext;
use app\common\models\DTO\CompleteInfoPedido;
use app\common\models\DTO\SecureDataUser;
use app\common\models\sessions\UserSession;
use app\common\models\SimpleContextResponse;
use app\common\models\SimpleContextResponseApp;
use app\common\utils\CommonUtil;
use app\common\utils\EmailComposer;
use app\common\utils\SMSComposer;
use app\common\utils\UtilCommunication;
use app\models\Documentos;
use app\models\TiposNotificaciones;
use DateTime;
use ReCaptcha\Response;
use Symfony\Component\HttpFoundation\Tests\Session\Storage\Handler\NullSessionHandlerTest;
use Yii;

use app\models\Util;
use app\models\Usuarios;
use app\models\Traccar;
use app\models\Configuraciones;
use app\models\Categorias;
use app\models\Servicios;
use app\models\UsuariosCategorias;
use app\models\UsuariosServicios;
use app\models\CategoriasServicios;
use app\models\Pedidos;
use app\models\Items;
use app\models\Notificaciones;
use yii\base\Exception;
use yii\web\Controller;
use yii\filters\VerbFilter;


class ApiController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'getinfoapp' => ['get'], //Información global de la plataforma para rastreo y variables internas
                    'login' => ['post'], //Ingreso de usuarios
                    'register' => ['post'], //Registro de usuarios
                    'contratarasociado' => ['post'], //Registro de usuarios
                    'recoverpassword' => ['get'], //Recuperar la clave de la cuenta
                    'requestnewpassword' => ['get'], //Generar token de recuperación de la clave de la cuenta
                    'basicupdateprofile' => ['post'], //Actualización Básica de datos
                    'termsconditions' => ['get'], //Información de terminos y condiciones
                    'getcategories' => ['get'], //Listado de categorias
                    'getassociates' => ['get'], //Listado de asociados
                    'getservices' => ['get'], //Listado de servicios
                    'setitemcart' => ['post'], //Permite agregar un item al carrito de compras
                    'deleteitemcart' => ['get'], //Permite borrar un item del carrito de compras
                    'getshoppingcart' => ['get'], //Devuelve el carrito de compras de un usuario
                    'getorders' => ['get'],
                    'setshoppingcart' => ['get'],
                    'transmittotraccar' => ['get'],
                    'getnotificaciones' => ['post'],
                    'getpedidos' => ['post'],
                    'send-email-cli-reg' => ['post'], //permite enviar emails desde el FO al registrar un cliente
                    'getinfopedido' => ['post'], //Devuelve informacion del pedido
                    'getinfoclient' => ['post'], //Devuelve informacion del cliente a partir de un id de pedido

                    'aceptarpedido' => ['post'],
                    'confirmarpedido' => ['post'],
                    'rechazarpedido' => ['post'],
                    'terminarpedido' => ['post'],
                    'cancelarpedido' => ['post'],
                    'reagendarpedido' => ['post'],

                ],

            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => UtilCommunication::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600,                 // Cache (seconds)
                ],
            ],
        ];
    }


    /**
     * Filter if an action has permission to this controller
     * @param $event
     * @return array|bool
     */
    public function beforeAction($event)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $action = $event->id;
        if (isset($this->actions[$action])) {
            $verbs = $this->actions[$action];
        } elseif (isset($this->actions['*'])) {
            $verbs = $this->actions['*'];
        } else {
            return $event->isValid;
        }
        $verb = Yii::$app->getRequest()->getMethod();

        $allowed = array_map('strtoupper', $verbs);

        if (!in_array($verb, $allowed)) {

            UtilCommunication::setHeader(400);
            return array('status' => 0, 'error_code' => 400, 'message' => 'Método no encontrado');
            exit;

        }
        return true;
    }

    public function actionGetinfoapp($token = null)
    {
        if (is_null($token)) {
            UtilCommunication::setHeader(200);
            return ['status' => 0,
                'message' => 'Error al recuperar información de conectividad GPS',
            ];
        } else {
            UtilCommunication::setHeader(200);
            return ['status' => 1,
                'message' => 'Información global de APP',
                'data' => [
                    'traccar_user' => Yii::$app->params['traccar']['usuario'],
                    'traccar_pass' => Yii::$app->params['traccar']['clave'],
                    'traccar_server' => Yii::$app->params['traccar']['transmision_url'],
                    'traccar_server_rest' => Yii::$app->params['traccar']['rest_url'],
                ]
            ];

        }
    }

    public function actionLogin()
    {
        $simpleResponse = new SimpleContextResponse;
        $post = Yii::$app->request->post();

        if ($post['email'] && $post['clave']) {
            $usuario = Usuarios::find()->andWhere(['email' => $post['email']])->one();
            //var_dump($usuario);die();
            if (!is_object($usuario)) {
                UtilCommunication::setHeader(200);
                return $simpleResponse
                    ->setContext(0, ResponseConstants::USER_NOT_FOUND);
            } else if (!$usuario || !$usuario->validatePassword($post['clave'])) {
                UtilCommunication::setHeader(200);
                return $simpleResponse
                    ->setContext(0, ResponseConstants::INCORRECT_EMAIL_AND_PASSWORD);
            }

            if (is_object($usuario)) {
                $userSession = new UserSession();
                UtilCommunication::setHeader(200);
                //TODO: Q? Porque guarda el usuario??
                if ($usuario->save()) {
                    $items_cart = 0;
                    $pedido = Pedidos::find()->andWhere(['cliente_id' => $usuario->id, 'estado' => 0])->one();

                    if (is_object($pedido)) {
                        $items_cart = count($pedido->items);
                    }

                    if ($usuario->es_super == 1) {
                        return $simpleResponse
                            ->setContext(0, ResponseConstants::USER_VALID_IN_WEB_ENV);
                    }

                    $userSession->setId($usuario->id);
                    $userSession->setEstado($usuario->estado);
                    $userSession->setDisplayName($usuario->nombres . ' ' . $usuario->apellidos);
                    $userSession->setNombres($usuario->nombres);
                    $userSession->setApellidos($usuario->apellidos);
                    $userSession->setEmail($usuario->email);
                    $userSession->setNumeroCelular($usuario->numero_celular);
                    $userSession->setTelefonoDomicilio($usuario->telefono_domicilio);
                    $userSession->setImagen($usuario->imagen);
                    $userSession->setToken($usuario->token);
                    $userSession->setTraccarId($usuario->traccar_id);
                    $userSession->setTraccarTransmision(Yii::$app->params['traccar']['transmision_url']);
                    $userSession->setImei($usuario->imei);
                    $userSession->setItemsCart($items_cart);
                    $userSession->setFechaCreacion($usuario->fecha_creacion);
                    $userSession->setFechaActivacion($usuario->fecha_activacion);
                    $userSession->setIdentificacion($usuario->identificacion);
                    $userSession->setJornadaTrabajo($usuario->jornada_trabajo);

                    //PAGOS
                    $userSession->setBancoId($usuario->banco_id);
                    $userSession->setNombreBeneficiario($usuario->nombre_beneficiario);
                    $userSession->setTipoCuenta($usuario->tipo_cuenta);
                    $userSession->setPreferenciasDeposito($usuario->preferencias_deposito);
                    $userSession->setTipoRuc($usuario->tipo_ruc);


                    $serviciosLista = [];
                    $serviciosUsuario = $usuario->usuariosServicios;
                    //$planUsuario = $usuario->plan;
                    // $categoriasUsuario = $usuario->usuariosCategorias;
                    //   $documentosUsuarios = $usuario->documentos;
                    //  $bancosUsuarios = $usuario->banco;

                    for ($i = 0; $i < count($serviciosUsuario); $i++) {
                        $servicio = Servicios::findOne($serviciosUsuario[$i]['servicio_id']);
                        $categoria_servicio = CategoriasServicios::find()->andwhere(['servicio_id' => $servicio->id])->one();
                        $documento = Documentos::findOne($serviciosUsuario[$i]->documento_id);
                        $serviciosLista[$i] = [
                            "servicio_id" => $servicio->id,
                            "categoria_id" => $categoria_servicio->categoria_id,
                            "servicio_nombre" => $servicio->nombre,
                            'obligatorio_certificado' => $servicio->obligatorio_certificado,
                            'documento' => is_object($documento) ? $documento->imagen : "",
                        ];
                    }

                    if (($usuario->es_asociado == 1 && $usuario->es_cliente == 1) || $usuario->es_asociado == 1) {
                        $userSession->setTipo(ResponseConstants::ASSOCIATE_CLIENT);
                        $userSession->setNumeroCuenta($usuario->numero_cuenta);
                        $userSession->setPais(isset($usuario->pais) ? $usuario->pais->nombre : NULL);
                        $userSession->setCiudad(isset($usuario->ciudad) ? $usuario->ciudad->nombre : NULL);
                        $userSession->setPlan(isset($usuario->plan) ? $usuario->plan->nombre : NULL);
                        $userSession->setCategorias($usuario->usuariosCategorias);
                        $userSession->setServicios($serviciosLista);
                        $userSession->setDocumentos($usuario->documentos);
                        $userSession->setPagos([]);//TODO: para la pasarela de pagos?
                        $userSession->setPlanInfo(isset($usuario->plan) ? $usuario->plan : NULL);
                        $userSession->setBanco(isset($usuario->banco) ? $usuario->banco : NULL);
                        $userSession->setUsuariosServicios($usuario->usuariosServicios);
                        $userSession->setRuc($usuario->ruc);
                        $userSession->setFotografiaCedula($usuario->fotografia_cedula);
                        $userSession->setVisaTrabajo($usuario->visa_trabajo);
                        $userSession->setRise($usuario->rise);
                        $userSession->setReferenciasPersonales($usuario->referencias_personales);
                        $userSession->setTituloAcademico($usuario->titulo_academico);

                        if ($usuario->es_asociado == 1 && $usuario->es_cliente != 1) {
                            $userSession->setTipo(ResponseConstants::ASSOCIATE);
                        }
                    } elseif ($usuario->es_cliente == 1) {
                        $userSession->setTipo(ResponseConstants::CLIENT);
                    } else {
                        return $simpleResponse
                            ->setContext(0, ResponseConstants::USER_WITHOUT_ROL);
                    }

                    return $simpleResponse
                        ->setContext(1, 'Bienvenid@: ' . $usuario->nombres,
                            ['usuario' => $userSession->getUserSession()]);

                } else {
                    return $simpleResponse->setContext(0, ResponseConstants::ERROR_IN_LOGIN,
                        ['errors' => $usuario->getErrors()]);
                }
            } else {
                return $simpleResponse->setContext(0, ResponseConstants::USER_NOT_FOUND);
            }
        } else {
            return $simpleResponse->setContext(0, ResponseConstants::EMAIL_AND_PASSWORD_REQUIRED);
        }
    }

    public function actionRegister()
    {
        $simpleResponse = new SimpleContextResponse;
        UtilCommunication::setHeader(200);
        $post = Yii::$app->request->post();
        $model = new Usuarios();

        if ($model->load($post, '')) {

            $model->numero_celular = preg_replace('/\s+/', '', $model->numero_celular);
            $model->numero_celular = str_replace(' ', '', $model->numero_celular);

            $validacion_numero = substr($model->numero_celular, 0, 1);
            if ((int)$validacion_numero == 0) {
                $model->numero_celular = YouNeedContext::COUNTRY_PREFIX . substr($model->numero_celular, 1);
            }

            if ($post['tipo'] == 'Asociado') {
                $model->scenario = YouNeedContext::ASSOCIATED_SCENARIO;
            } elseif ($post['tipo'] == 'Cliente') {
                $model->scenario = YouNeedContext::REGISTER_CLIENT_SCENARIO;
            } else {
                return $simpleResponse->setContext(0, ResponseConstants::TYPE_PARAM_ERROR);
            }

            if (isset($post['validate']) && $post['validate'] == 1) {
                $model->validate();
                return $simpleResponse->setContext((count($model->getErrors()) == 0) ? 1 : 0,
                    ResponseConstants::VALIDATE_INFORMATION, ['errors' => $model->getErrors()]);
            } else {
                if ($model->save()) {
                    $model->token = Util::getGenerarPermalink(Yii::$app->getSecurity()->generatePasswordHash('YOUAbitmedia' . $model->id . date('Y-m-d H:i:s')));
                    $model->clave = Yii::$app->getSecurity()->generatePasswordHash($model->clave);
                    $model->imei = rand(pow(10, 4 - 1), pow(10, 4) - 1) . time();
                    $model->es_cliente = 1;
                    $model->save(); //TODO: porque doble save??

                    // $response = Traccar::setDevice( $model, 'POST' );
                    // $model->traccar_id = $response['id'];
                    // $model->save();

                    return $simpleResponse->setContext(1,
                        ResponseConstants::SUCCESS_REGISTER);

                } else {
                    return $simpleResponse->setContext(0,
                        ResponseConstants::ERROR_IN_CURRENT_USER, ['errors' => $model->getErrors()]);
                }
            }
        } else {
            return $simpleResponse->setContext(0,
                ResponseConstants::INCORRECT_SENDED_PARAMETERS);
        }
    }

    public function actionBasicupdateprofile()
    {
        //TODO: hacer transaccional todo el proceso
        $simpleResponse = new SimpleContextResponse;
        UtilCommunication::setHeader(200);
        $post = Yii::$app->request->post();
        $model = Usuarios::find()->andWhere(['email' => isset($post['email']) ? $post['email'] : null])->one();

        if (is_object($model)) {
            $model->load($post);
            if ($model->es_cliente == 1) {
                $model->scenario = YouNeedContext::CLIENT_SCENARIO;
            }

            // ¡¡¡ATENCIÓN!!! Agregar Validación de actualización de acuerdo a los perfiles
            UtilCommunication::setHeader(200);
            $model->nombres = isset($post['nombres']) ? $post['nombres'] : null;
            $model->apellidos = isset($post['apellidos']) ? $post['apellidos'] : null;
            $model->email = isset($post['email']) ? $post['email'] : null;
            $model->imagen = isset($post['foto_perfil']) ? $post['foto_perfil'] : null;
            $model->identificacion = isset($post['identificacion']) ? $post['identificacion'] : null;
            $model->numero_celular = isset($post['numero_celular']) ? $post['numero_celular'] : null;
            $model->telefono_domicilio = isset($post['telefono_domicilio']) ? $post['telefono_domicilio'] : null;
            $model->jornada_trabajo = isset($post['day']) ? json_encode($post['day']) : null;

            //var_dump($model->validate());
            if (!$model->validate()) {
                return $simpleResponse->setContext(0, CommonUtil::getErrorsFromModel($model));
            } //die('dd');

            $listado_categorias = explode(",", isset($post['categorias']) ? $post['categorias'] : '');
            $update_servicios = false;

            if (is_array($listado_categorias)) {
                UsuariosCategorias::deleteAll('usuario_id = ' . $model->id);
                foreach ($listado_categorias as $pc) {
                    $p = new UsuariosCategorias();
                    $p->categoria_id = $pc;
                    $p->usuario_id = $model->id;
                    if ($p->save()) {
                        $update_servicios = true;
                    }
                }
            }

            $listado_servicios = explode(",", isset($post['servicios']) ? $post['servicios'] : '');

            if (is_array($listado_servicios)) {
                UsuariosServicios::deleteAll('usuario_id = ' . $model->id);
                foreach ($listado_servicios as $pc) {
                    $p = new UsuariosServicios();
                    $p->servicio_id = $pc;
                    $p->usuario_id = $model->id;
                    if ($p->save()) {
                        $update_servicios = true;
                    }
                }
            }

            Documentos::deleteAll(['usuario_id' => $model->id]);

            if (isset($post['Usuarios']['usuarios_servicios']['file'])) {
                $tipo_documento_id = 7;
                $es_obligatorio = 1;
                $array_servicios_id = array_keys($post['Usuarios']['usuarios_servicios']['file'][$model->id]);
                foreach ($array_servicios_id as $k => $servicio_id) {
                    $file_base64 = $post['Usuarios']['usuarios_servicios']['file'][$model->id][$servicio_id];
                    $documento = new Documentos();
                    $documento->usuario_id = $model->id;
                    $documento->tipo_documento_id = $tipo_documento_id;
                    $documento->imagen = $file_base64;
                    $documento->es_obilgatorio = $es_obligatorio;
                    if ($documento->save()) {
                        $p = UsuariosServicios::find()->where(['usuario_id' => $model->id, 'servicio_id' => $servicio_id])->one();
                        $p->documento_id = $documento->id;
                        $p->update();
                    }
                }
            }

            if ($model->save() || $update_servicios) {
                //Email al Cliente FUNCIóN COMENTADA
                return $simpleResponse->setContext(1, ResponseConstants::SUCCESS_UPDATE_PROFILE);
            } else {
                return $simpleResponse->setContext(0, ResponseConstants::ERROR_FROM_SERVER);
            }
            return $simpleResponse->setContext(0, ResponseConstants::ERROR_PASSWORD_RESET);

        } else {
            return $simpleResponse->setContext(0, CommonUtil::getErrorsFromModel($model));

            //  return $simpleResponse->setContext(0, ResponseConstants::INVALID_DATA);
        }
    }

    public function actionRecoverpassword()
    {
        $simpleResponse = new SimpleContextResponse;
        UtilCommunication::setHeader(200);
        $email = Yii::$app->request->post('email');
        $clave = Yii::$app->request->post('clave');
        $token = Yii::$app->request->post('token');
        $model = new Usuarios();

        if ($email == "" && $clave == "") {
            return $simpleResponse->setContext(0, ResponseConstants::NO_TEXT_ERROR);
        } else {
            $model = Usuarios::find()->andWhere(['email' => $email])->one();
            if (is_object($model)) {
                if ($token !== $model->password_token) {
                    return $simpleResponse->setContext(2, ResponseConstants::INVALID_RECOVERY_PASSWORD_ERROR);
                }
                if ($clave !== "" && $token == $model->password_token) {
                    $model->clave = Yii::$app->getSecurity()->generatePasswordHash($clave);
                    $model->password_token = '';
                    if ($model->update()) {
                        EmailComposer::sendEmail(EmailContext::SEND_SUCCESS_RECOVERY_PASSWORD,
                            ['emailTo' => $model->email, 'body' => $model->nombres . " " . $model->apellidos]);
                        return $simpleResponse->setContext(1, ResponseConstants::SUCCESS_REGISTER_PASSWORD);
                    } else {
                        return $simpleResponse->setContext(0, ResponseConstants::ERROR_WHEN_RECOVERY_PASSWORD);
                    }
                }
                return $simpleResponse->setContext(0, ResponseConstants::RECOVERY_PASSWORD_TRY_AGAIN_ERROR);
            } else {
                return $simpleResponse->setContext(0, ResponseConstants::COMMON_MESSAGE_ERROR);
            }
        }
    }

    public function actionRequestnewpassword()
    {
        $simpleResponse = new SimpleContextResponse;
        UtilCommunication::setHeader(200);
        $email = Yii::$app->request->post('email');
        $model = new Usuarios();

        if ($email == "") {
            UtilCommunication::setHeader(200);
            return $simpleResponse->setContext(0, ResponseConstants::EMPTY_EMAIL_ERROR);
        } else {
            $model = Usuarios::find()->andWhere(['email' => $email])->one();
            if (is_object($model)) {
                $longitud = 80;
                $password_token = bin2hex(openssl_random_pseudo_bytes(($longitud - ($longitud % 2)) / 2));
                $model->password_token = $password_token;

                if ($model->update()) {
                    $link = 'https://youneed.com.ec/app/resetpassword?email=' . $model->email . '&token=' . $password_token;
                    //Email al Cliente FUNCIóN COMENTADA
                    EmailComposer::sendEmail(EmailContext::RECOVERY_PASSWORD, ['emailTo' => $model->email, 'link' => $link, 'model' => $model]);
                    return $simpleResponse->setContext(1, ResponseConstants::RECOVERY_EMAIL_SENDED);
                } else {
                    return $simpleResponse->setContext(0, ResponseConstants::COMMON_MESSAGE_ERROR);
                }
                return $simpleResponse->setContext(0, ResponseConstants::REPLAY_NEW_PASSWORD_ERROR);
            } else {
                return $simpleResponse->setContext(0, ResponseConstants::INVALID_DATA);
            }
        }
    }

    // CONTRATAR

    public function actionContratarasociado()
    {
        $request = $_POST;

        $cliente_id = $_POST['cliente_id'];
        $asociado_id = $_POST['asociado_id'];
        $servicio_id = $_POST['servicio_id'];

        if ($cliente_id == $asociado_id) {
            UtilCommunication::setHeader(200);
            return ['status' => 0,
                'message' => 'Lo sentimos, no puedes contratarte a ti mismo.'
            ];
        }

        $pedido = Pedidos::find()->andWhere(['cliente_id' => $cliente_id, 'asociado_id' => $asociado_id, 'servicio_id' => $servicio_id, 'estado' => 0])->one();

        if (!is_object($pedido)) {
            $pedido = new Pedidos();

            $cliente = Usuarios::find()->andWhere(['id' => $_POST['cliente_id']])->one();
            $asociado = Usuarios::find()->andWhere(['id' => $_POST['asociado_id']])->one();
            $servicio = Servicios::find()->andWhere(['id' => $_POST['servicio_id']])->one();

            if ($pedido->load($_POST, '')) {
                $pedido->ciudad_id = $asociado->ciudad_id;
                $pedido->identificacion = $cliente->identificacion;
                $pedido->razon_social = $cliente->nombres . ' ' . $cliente->apellidos;
                $pedido->email = $cliente->email;
                $pedido->telefono = $cliente->numero_celular;
                //$pedido->fecha_creacion = date('Y-m-d H:i:s');
                $pedido->estado = 0;
                $pedido->tipo_atencion = Yii::$app->request->post('tipo_atencion');
                $pedido->subtotal = Yii::$app->request->post('subtotal_cliente');
                $pedido->iva = Yii::$app->request->post('iva_cliente');
                $pedido->total = Yii::$app->request->post('total_cliente');
                $_date = $_POST['fecha_para_servicio'];

                $aDate = explode("_", $_date);

                $fixDate = $aDate[0] . "-" . $aDate[1] . "-" . $aDate[2] . " " . $aDate[3] . ":" . $aDate[4] . ":" . $aDate[5];

                $pedido->fecha_para_servicio = $fixDate;

                if ($pedido->save()) {

                    //Email para enviar al cliente FUNCIóN COMENTADA
                    EmailComposer::sendEmail(EmailContext::SERVICE_REQUEST, ['emailTo' => $cliente->email, 'servicio' => $servicio, 'cliente' => $cliente]);

                    //Email al Asociado  FUNCIóN COMENTADA
                    EmailComposer::sendEmail(EmailContext::ASSOCIATE_SERVICE_REQUEST,
                        ['emailTo' => $asociado->email, 'asociado' => $asociado,
                            'cliente' => $cliente, 'pedido' => $pedido, 'servicio' => $servicio]);

                    $notificacionUsuario = new Notificaciones();
                    $notificacionUsuario->usuario_id = $cliente->id;
                    $notificacionUsuario->tipo_notificacion_id = 9;
                    $notificacionUsuario->save();

                    $notificacionAsociado = new Notificaciones();
                    $notificacionAsociado->usuario_id = $asociado->id;
                    $notificacionAsociado->tipo_notificacion_id = 5;
                    $notificacionAsociado->save();

                    UtilCommunication::setHeader(200);
                    return ['status' => 1,
                        'message' => 'Contrato Realizado'
                    ];

                } else {
                    UtilCommunication::setHeader(200);
                    return ['status' => 0,
                        'message' => 'Error de Sistema'
                    ];
                }
            }
        } else {
            UtilCommunication::setHeader(200);
            return ['status' => 2,
                'message' => 'Actualmente tienes una solicitud pendiente con este Asociado.'
            ];
        }

    }

    // FIN CONTRATAR


    public function actionTermsconditions()
    {
        $config = Configuraciones::findOne(1);
        UtilCommunication::setHeader(200);
        return ['status' => 1,
            'message' => 'Términos y condiciones',
            'data' => $config->politicas_condiciones,
        ];
    }

    public function actionGetcategories($categoria_id = null)
    {

        if (is_null($categoria_id)) {
            $array_categorias = [];
            $categorias = Categorias::find();
            $categorias->orderBy(['rand()' => SORT_DESC]);
            // $categorias->limit(10);
            $categorias = $categorias->all();
            foreach ($categorias as $categoria) {
                $array_categorias[] = [
                    'id' => $categoria->id,
                    'nombre' => mb_convert_encoding(trim(substr($categoria->nombre, 0, 100)) . '...', 'UTF-8', 'UTF-8'),
                    'descripcion' => mb_convert_encoding(trim(substr(strip_tags($categoria->descripcion), 0, 80)) . '...', 'UTF-8', 'UTF-8'),
                    'imagen' => $categoria->imagen,
                ];
            }
            UtilCommunication::setHeader(200);
            return ['status' => 1,
                'message' => 'Listado de categorías',
                'data' => ['categorias' => $array_categorias],
            ];
        } else {
            $categoria = Categorias::findOne($categoria_id);
            if (is_object($categoria)) {

                UtilCommunication::setHeader(200);
                return ['status' => 1,
                    'message' => 'Información de categoria',
                    'data' => ['categoria' => $categoria->attributes],
                ];

            } else {

                UtilCommunication::setHeader(200);
                return ['status' => 0,
                    'message' => 'Categoria no encontrada',
                ];

            }
        }

    }

    public function actionGetassociates($categoria_id = null, $servicio_id = null, $asociado_id = null)
    {

        $array_asociados = [];
        $asociados = Usuarios::find();

        if (!is_null($categoria_id)) {
            $asociados->andWhere(['categoria_id' => $categoria_id]);
        }
        if (!is_null($servicio_id)) {
            $asociados->innerJoinWith('usuariosServicios', 't.id = usuariosServicios.usuario_id');
            $asociados->andWhere(['usuarios_servicios.servicio_id' => $servicio_id]);
        }
        if (!is_null($asociado_id)) {
            $asociados->andWhere(['id' => $asociado_id]);
        }

        $asociados->andWhere(['es_asociado' => 1, 'estado' => 1]);
        // $categorias->limit(10);
        $asociados = $asociados->all();
        foreach ($asociados as $asociado) {
            $array_asociados[] = $asociado->attributes;
        }

        UtilCommunication::setHeader(200);
        return ['status' => 1,
            'message' => 'Listado de asociados',
            'data' => ['asociados' => $array_asociados, 'total' => count($array_asociados), 'servicio_id' => $servicio_id],
        ];

    }

    public function actionGetservices($servicio_id = null, $asociado_id = null, $categoria_id = null, $name = null)
    {

        $array_servicios = [];
        $array_asociado = [];

        if (!is_null($asociado_id)) {
            $asociado = Usuarios::findOne($asociado_id);
            if (is_object($asociado)) {
                $array_asociado = ['id' => $asociado->id, 'nombres' => $asociado->nombres, 'apellidos' => $asociado->apellidos];
            }
        }

        $servicios = Servicios::find();
        $servicios->andWhere(['tiene_soft_delete' => 0]);
        $servicios->andWhere(['mostrar_app' => 1]);
        $servicios->andWhere(['>', 'total', 0]);

        if (!is_null($servicio_id)) {
            $servicios->andWhere(['id' => $servicio_id]);
        }

        if (!is_null($categoria_id)) {
            $servicios->innerJoinWith('categoriasServicios', 't.id = categoriasServicios.categoria_id');
            $servicios->andWhere(['categorias_servicios.categoria_id' => $categoria_id]);
        }

        if (!is_null($name)) {
            $servicios->andFilterWhere(['like', 'nombre', $name]);
            $servicios->limit(10);
        }

        // $categorias->limit(10);
        $servicios = $servicios->all();
        foreach ($servicios as $servicio) {
            $array_servicios[] = [
                'id' => $servicio->id,
                // 'nombre' => ( is_null( $servicio_id ) ) ? mb_convert_encoding( trim(substr( $servicio->nombre, 0, 100 )).'...' , 'UTF-8', 'UTF-8' ) : $servicio->nombre,
                'nombre' => $servicio->nombre,
                'name' => $servicio->nombre,
                'incluye' => (is_null($servicio_id)) ? mb_convert_encoding(trim(substr(strip_tags($servicio->incluye), 0, 80)) . '...', 'UTF-8', 'UTF-8') : $servicio->incluye,
                // 'imagen' => $servicio->imagen,
                'no_incluye' => $servicio->no_incluye,
                'aplica_iva' => $servicio->aplica_iva,
                'subtotal' => $servicio->subtotal,
                'total' => $servicio->total,
                'subtotal_diagnostico' => (float)Yii::$app->params['parametros_globales']['valor_visita_diagnostico'] / (float)Yii::$app->params['parametros_globales']['iva_valor'],
                'total_diagnostico' => Yii::$app->params['parametros_globales']['valor_visita_diagnostico'],
                'asociado' => $array_asociado,
            ];
        }

        UtilCommunication::setHeader(200);
        return ['status' => 1,
            'message' => 'Listado de asociados',
            'data' => ['servicios' => $array_servicios, 'total' => count($array_servicios)],
        ];

    }


    public function actionSetitemcart()
    {
        // token
        // servicio_id
        // cantidad
        // costo_unitario
        // es_diagnostico
        // tipo_atencion
        // asociado_id
        $request = Yii::$app->request;
        $model = new Items();

        if ($model->load($request->post(), '')) {

            if (Yii::$app->request->post('token')) { //Token de usuario

                $usuario = Usuarios::find()->andWhere(['token' => Yii::$app->request->post('token')])->one();
                if (is_object($usuario)) {

                    $pedido = Pedidos::find()->andWhere(['cliente_id' => $usuario->id, 'estado' => 0])->one();

                    if (!is_object($pedido)) {
                        $pedido = new Pedidos();
                        $pedido->cliente_id = $usuario->id;
                        $pedido->identificacion = $usuario->identificacion;
                        $pedido->razon_social = $usuario->nombres . ' ' . $usuario->apellidos;
                        $pedido->email = $usuario->email;
                        $pedido->telefono = $usuario->numero_celular;
                        $pedido->fecha_creacion = date('Y-m-d H:i:s');
                        $pedido->estado = 0;
                        $pedido->tipo_atencion = Yii::$app->request->post('tipo_atencion');
                        $pedido->save();
                    }

                    $item = null;
                    if (Yii::$app->request->post('es_diagnostico') != 1) {
                        $item = Items::find()->andWhere(['pedido_id' => $pedido->id, 'servicio_id' => Yii::$app->request->post('servicio_id'), 'es_diagnostico' => 0])->one();
                    }

                    if (is_object($item)) {
                        $item->cantidad = (int)$item->cantidad + Yii::$app->request->post('cantidad');
                        $item->costo_unitario = Yii::$app->request->post('costo_unitario');
                        if ($item->save()) {
                            Util::calcularPedido($pedido->id);
                            UtilCommunication::setHeader(200);
                            return ['status' => 1,
                                'message' => 'Servicio agregado exitosamente',
                                'data' => ['items_cart' => count($pedido->items)]
                            ];
                        } else {
                            UtilCommunication::setHeader(200);
                            return ['status' => 0,
                                'message' => 'Ocurrio un error al registrar item',
                                'data' => ['errors' => $item->getErrors()],
                            ];
                        }

                    } else {

                        $model->pedido_id = $pedido->id;

                        if ($model->save()) {

                            Util::calcularPedido($pedido->id);

                            UtilCommunication::setHeader(200);
                            return ['status' => 1,
                                'message' => 'Registrado exitosamente',
                                'data' => ['items_cart' => count($pedido->items)]
                            ];

                        } else {
                            UtilCommunication::setHeader(200);
                            return ['status' => 0,
                                'message' => 'Ocurrio un error al registrar item',
                                'data' => ['errors' => $model->getErrors()],
                            ];
                        }

                    }

                } else {
                    UtilCommunication::setHeader(200);
                    return ['status' => 0,
                        'message' => 'Usuario no encontrado',
                    ];
                }

            } else {
                UtilCommunication::setHeader(200);
                return ['status' => 0,
                    'message' => 'El parámetro token es necesario',
                ];
            }

        } else {
            UtilCommunication::setHeader(200);
            return ['status' => 0,
                'message' => 'Parámetros recibidos incorrectos',
            ];
        }
    }

    public function actionDeleteitemcart($item_id = null)
    {
        $item = Items::findOne($item_id);
        if (is_object($item)) {
            $pedido_id = $item->pedido_id;
            $pedido = Pedidos::findOne($item->pedido_id);
            if (\app\models\Util::borrarRegistrosRecursivos($item)) {
                Util::calcularPedido($pedido_id);
                UtilCommunication::setHeader(200);
                return ['status' => 1,
                    'message' => 'Item eliminado correctamente',
                    'data' => ['total' => count($pedido->items)],
                ];
            } else {
                UtilCommunication::setHeader(200);
                return ['status' => 0,
                    'message' => 'Ocurrio un error, vuelva a intentarlo',
                ];
            }
        } else {
            UtilCommunication::setHeader(200);
            return ['status' => 0,
                'message' => 'Item no encontrado',
            ];
        }
    }

    public function actionGetshoppingcart($token = null, $numero_items = null)
    {
        $usuario = Usuarios::find()->andWhere(['token' => $token])->one();
        if (is_object($usuario)) {
            $items = [];
            $pedido = Pedidos::find()->andWhere(['cliente_id' => $usuario->id, 'estado' => 0])->one();
            if (is_object($pedido)) {

                if (is_null($numero_items)) {
                    foreach ($pedido->items as $item) {
                        $items [] = ['id' => $item->id,
                            'descripcion' => ($item->es_diagnostico == 1) ? $item->servicio->nombre . ' - ' . Yii::$app->params['parametros_globales']['texto_visita_diagnostico'] : $item->servicio->nombre,
                            'cantidad' => $item->cantidad,
                            'costo_unitario' => $item->costo_unitario,
                            'costo_total' => $item->costo_total
                        ];
                    }

                    $pedido_info = null;

                    if (is_object($pedido)) {
                        $pedido_info = $pedido->attributes;
                    } else {
                        $pedido_info = ['id' => 0];
                    }

                    UtilCommunication::setHeader(200);
                    return ['status' => 1,
                        'message' => 'Carrito de compras',
                        'data' => ['pedido' => $pedido_info, 'items' => $items, 'total' => count($pedido->items)],
                    ];
                } else {
                    $pedido_info = ['id' => 0];
                    UtilCommunication::setHeader(200);
                    return ['status' => 1,
                        'message' => 'Carrito de compras',
                        'data' => ['pedido' => $pedido_info, 'total' => count($pedido->items)],
                    ];
                }

            } else {
                $pedido_info = ['id' => 0];
                UtilCommunication::setHeader(200);
                return ['status' => 1,
                    'message' => 'Carrito de compras',
                    'data' => ['pedido' => $pedido_info, 'total' => 0],
                ];
            }

        } else {
            UtilCommunication::setHeader(200);
            return ['status' => 0,
                'message' => 'Usuario no encontrado'
            ];
        }
    }

    public function actionGetorders($token = null, $usuario_tipo = 0, $estado = null)
    {

        //TODO: usar datos del params
        //$usuario_tipo
        // 0 -> Cliente
        // 1 -> Asociado

        // $estado
        // 0 -> En espera
        // 1 -> Reservada
        // 2 -> En ejecución
        // 3 -> Pagada
        // 4 -> Cancelada

        $array_pedidos = [];

        $usuario = Usuarios::find()->andWhere(['token' => $token])->one();

        if (!is_object($usuario)) {
            UtilCommunication::setHeader(200);
            return ['status' => 0,
                'message' => 'Usuario no encontrado',
            ];
        }

        $pedidos = Pedidos::find();

        if ($usuario_tipo == 0) {
            $pedidos->andWhere(['cliente_id' => $usuario->id]);
        } else {
            $pedidos->andWhere(['asociado_id' => $usuario->id]);
        }

        if (!is_null($estado)) {
            $pedidos->andWhere(['estado' => $estado]);
        }

        $pedidos->orderBy(['id' => SORT_DESC]);
        $pedidos = $pedidos->all();

        foreach ($pedidos as $pedido) {
            $nombres_asociado = '';
            $items = [];
            $estado_pedido = Yii::$app->params['estados_pedidos'][$pedido->estado];

            foreach ($pedido->items as $item) {
                $items [] = $item->servicio->nombre;
            }

            if (is_object($pedido->asociado)) {
                $nombres_asociado = $pedido->asociado->nombres . ' ' . $pedido->asociado->apellidos;
            }
            $array_pedidos[] = [
                'id' => $pedido->id,
                'razon_social' => $pedido->razon_social,
                'servicios' => implode(',', $items),
                'fecha_creacion' => $pedido->fecha_creacion,
                'total' => $pedido->total,
                'fecha_para_servicio' => $pedido->fecha_para_servicio,
                'nombres_asociado' => $nombres_asociado,
                'estado' => $estado_pedido,
            ];
        }

        UtilCommunication::setHeader(200);
        return ['status' => 1,
            'message' => 'Listado de pedidos',
            'data' => ['pedidos' => $array_pedidos, 'total' => count($array_pedidos)],
        ];


    }

    public function actionSetshoppingcart($pedido_id = null)
    {

        if (is_null($pedido_id)) {
            UtilCommunication::setHeader(200);
            return ['status' => 0,
                'message' => 'El parámetro pedido_id es requerido',
            ];
        } else {
            $pedido = Pedidos::findOne($pedido_id);
            if (is_object($pedido)) {
                $pedido->estado = 1;
                if ($pedido->save()) {
                    UtilCommunication::setHeader(200);
                    return ['status' => 1,
                        'message' => 'Pedido reservado exitosamente',
                    ];
                } else {
                    UtilCommunication::setHeader(200);
                    return ['status' => 0,
                        'message' => 'Ocurrio un error, vuelva a intentarlo',
                    ];
                }
            } else {
                UtilCommunication::setHeader(200);
                return ['status' => 0,
                    'message' => 'Pedido no encontrado',
                ];
            }
        }

    }

    public function actionSetposition($token = null, $lat = null, $lon = null, $velocidad = null, $bateria = null, $traccar = true)
    {
        if (is_null($token) && is_null($lat) && is_null($lon)) {
            UtilCommunication::setHeader(200);
            return ['status' => 0,
                'message' => 'Los parámetros token, lat y lon son requeridos',
            ];
        } else {

            $usuario = Usuarios::find()->andWhere(['token' => $token])->one();
            if (is_object($usuario)) {
                if (is_object($usuario->dispositivo)) {
                    $usuario->dispositivo->latitude = $lat;
                    $usuario->dispositivo->longitude = $lon;
                    $usuario->dispositivo->velocidad = $velocidad;
                    $usuario->dispositivo->bateria = (float)$bateria * 100;
                    $usuario->dispositivo->ultima_transmision = date('Y-m-d H:i:s');
                    if ($usuario->dispositivo->save()) {

                        if ($traccar) {
                            Traccar::setPosition($usuario->dispositivo->imei, $lat, $lon);
                        }

                        UtilCommunication::setHeader(200);
                        return ['status' => 1,
                            'message' => 'Posición actualizada exitosamente.',
                        ];
                    } else {
                        UtilCommunication::setHeader(200);
                        return ['status' => 0,
                            'message' => 'Hubo un error al actualizar posición',
                            'data' => ['errores' => $usuario->getErrors()],
                        ];
                    }
                } else {
                    UtilCommunication::setHeader(200);
                    return ['status' => 0,
                        'message' => 'Usuario no tiene asignado un dispositivo',
                    ];
                }
            } else {
                UtilCommunication::setHeader(200);
                return ['status' => 0,
                    'message' => 'Usuario no encontrado con token especificado',
                ];
            }

        }
    }

    public function actionTransmittotraccar()
    {

        $json = file_get_contents('php://input');
        $data = json_decode($json);
        // return $data->location->id;
        Traccar::setPosition($data->location->id, $data->location->lat, $data->location->lon, 0, (float)$data->location->batt * 100);

    }

    public function actionGetnotificaciones()
    {
        $simpleResponseApp = new SimpleContextResponseApp();
        $post = Yii::$app->request->post();
        $limit = 5;
        $page = 0;
        $offset = 0;

        if (!isset($post['uid'])) {
            return $simpleResponseApp->setContext(null, ResponseConstants::INVALID_DATA);
        }

        if (isset($post['page'])) {
            $page = $post['page'];
            $offset = $page * $limit;
        }

        $notif = Yii::$app->db
            ->createCommand('SELECT n.id, n.usuario_id, n.tipo_notificacion_id, n.fecha_notificacion, n.model_name, n.model_id, t.categoria, t.descripcion , t.mensaje FROM notificaciones n, tipos_notificaciones t WHERE t.id = n.tipo_notificacion_id AND n.usuario_id = ' . $post["uid"] . ' ORDER BY n.fecha_notificacion DESC, id DESC LIMIT ' . $limit . ' OFFSET ' . $offset)
            ->queryAll();

        return $simpleResponseApp->setContext('1', 'notificaciones', ['notificaciones' => $notif]);
    }

    public function actionGetpedidos()
    {
        UtilCommunication::setHeader(200);
        $limit = 5;
        $page = 0;
        $offset = 0;

        if (!isset($_POST['uid'])) {
            return ['status' => null];
        }

        if (isset($_POST['page'])) {
            $page = $_POST['page'];
            $offset = $page * $limit;
        }
        $uid = Yii::$app->request->post('uid');
        $usuario = Usuarios::find()->andWhere(['id' => $uid])->one();

        if ($usuario) {
            if ($usuario->es_asociado) {
                $pedidos = Pedidos::find()
                    ->andWhere(['asociado_id' => $usuario->id])
                    ->orderBy(['tipo_atencion' => SORT_ASC, 'fecha_creacion' => SORT_DESC])
                    //->limit($limit)
                    //->offset($offset)
                    ->all();
                return ['pedidos' => $pedidos];
            } else if ($usuario->es_cliente) {
                $pedidos = Pedidos::find()->where(['cliente_id' => $usuario->id])
                    ->orderBy(['tipo_atencion' => SORT_ASC, 'fecha_creacion' => SORT_DESC])
                    //->limit($limit)
                    //->offset($offset)
                    ->all();

                $outPut = [];
                foreach ($pedidos as $pedido) {
                    $outPut[] = $this->getInfoPedido($pedido->id);
                }

                return ['pedidos' => $outPut];

            }
        } else {
            return ['pedidos' => 0];
        }
    }

    public function actionSendEmailCliReg()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            return EmailComposer::sendEmail(EmailContext::SEND_SUCCESS_REGISTER_CLIENT,
                ['emailTo' => $post['emailTo'], 'body' => $post['nombre']]);
        }
    }

    public function actionGetinfopedido()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $this->getInfoPedido(Yii::$app->request->post('pedido_id'));
    }

    public function actionGetinfoclient()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $infoPedido = new CompleteInfoPedido();
        $secureDataUser = new SecureDataUser();
        $pedido = Pedidos::findOne(Yii::$app->request->post('pedido_id'));
        $infoPedido->setPedido($pedido->attributes);
        $infoPedido->setCiudad(isset($pedido->ciudad) ? $pedido->ciudad->attributes : NULL);
        $infoPedido->setCliente(isset($pedido->cliente) ? $secureDataUser->extractDataFromModel($pedido->cliente) : NULL);
        return $infoPedido->getInfoPedido();
    }

    //--------------------- INICIO MANEJO DE ESTADOS del pedido ------------------------------

    /**
     * Cambia del estado 0-> EN_RESERVA al estado 1->ACEPTADO del pedido
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionAceptarpedido()
    {
        $response = new SimpleContextResponseApp();
        $post = Yii::$app->request->post();

        if ($post['id'] && $post['estado_previo'] == YouNeedContext::getIdStatusOrder(YouNeedContext::EN_ESPERA)
            && $post['estado_actual'] == YouNeedContext::getIdStatusOrder(YouNeedContext::ACEPTADO)) {
            $pedido = Pedidos::findOne($post['id']);

            if ($pedido) {
                $pedido->estado = $post['estado_actual'];
                $transaction = Pedidos::getDb()->beginTransaction();

                try {
                    $notification = $this->doNotification($pedido, $post['action_executor'], YouNeedContext::SOLICITUD_ACEPTADA);
                    $flag = true;
                    $flag = $flag && $pedido->update(false);
                    $flag = $flag && $notification->save(false);

                    if ($flag) {
                        $emailResponse = EmailComposer::sendEmail(EmailContext::ACCEPTED_SERVICE, ['emailTo' => $pedido->cliente->email, 'pedido' => $pedido]);
                        $smsResponse = SMSComposer::sendSMS(SMSContext::ACCEPTED_ORDER, ['SMSTo' => $pedido->cliente->numero_celular, 'params' => $pedido]);
                        $transaction->commit();
                        return $response->setContext(1, ResponseConstants::SUCCESS_REGISTER,
                            ['emailResponse' => $emailResponse, 'smsResponse' => $smsResponse]);
                    } else {
                        $transaction->rollBack();
                        return $response->setContext(null, 'ERROR AL ACTUALIZAR', null);
                    }

                } catch (Exception $e) {
                    Yii::error($e->getMessage());
                    $transaction->rollBack();
                    return $response->setContext(null, 'ERROR AL ACTUALIZAR', null);
                }
            } else {
                return $response->setContext(null, ResponseConstants::INCORRECT_SENDED_PARAMETERS, null);
            }
        } else {
            return $response->setContext(null, ResponseConstants::INCORRECT_SENDED_PARAMETERS, null);
        }
    }

    public function actionRechazarpedido()
    {
        $response = new SimpleContextResponseApp();
        $post = Yii::$app->request->post();

        if ($post['id'] && $post['estado_previo'] == YouNeedContext::getIdStatusOrder(YouNeedContext::EN_ESPERA)
            && $post['estado_actual'] == YouNeedContext::getIdStatusOrder(YouNeedContext::RECHAZADO)) {
            $pedido = Pedidos::findOne($post['id']);

            if ($pedido) {
                $pedido->estado = $post['estado_actual'];
                $transaction = Pedidos::getDb()->beginTransaction();

                try {
                    $notification = $this->doNotification($pedido, $post['action_executor'], YouNeedContext::SOLICITUD_RECHAZADA);
                    $flag = true;
                    $flag = $flag && $pedido->update(false);
                    $flag = $flag && $notification->save(false);

                    if ($flag) {
                        $emailResponse = EmailComposer::sendEmail(EmailContext::REJECT_SERVICE, ['emailTo' => $pedido->cliente->email, 'body' => $pedido]);
                        $smsResponse = SMSComposer::sendSMS(SMSContext::REJECT_ORDER, ['SMSTo' => $pedido->cliente->numero_celular, 'body' => $pedido]);
                        $transaction->commit();
                        return $response->setContext(1, ResponseConstants::SUCCESS_REGISTER,
                            ['emailResponse' => $emailResponse, 'smsResponse' => $smsResponse]);
                    } else {
                        $transaction->rollBack();
                        return $response->setContext(null, 'ERROR AL ACTUALIZAR', null);
                    }

                } catch (Exception $e) {
                    Yii::error($e->getMessage());
                    $transaction->rollBack();
                    return $response->setContext(null, 'ERROR AL ACTUALIZAR', null);
                }

            } else {
                return $response->setContext(null, ResponseConstants::INCORRECT_SENDED_PARAMETERS, null);
            }
        } else {
            return $response->setContext(null, ResponseConstants::INCORRECT_SENDED_PARAMETERS, null);
        }
    }

    /**
     * Confirma el pedido y valida que la fecha del servicio sea 1 hora antes de finaliza para poder activar
     * la accion y que el ambiente sea produccion
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionConfirmarpedido()
    {
        $response = new SimpleContextResponseApp();
        $post = Yii::$app->request->post();

        if ($post['id'] && $post['estado_previo'] == YouNeedContext::getIdStatusOrder(YouNeedContext::ACEPTADO)
            && $post['estado_actual'] == YouNeedContext::getIdStatusOrder(YouNeedContext::CONFIRMADO)) {
            $pedido = Pedidos::findOne($post['id']);

            if ($pedido) {
                $pedido->estado = $post['estado_actual'];
                $transaction = Pedidos::getDb()->beginTransaction();

                try {

                    $fecha_actual = strtotime(date("d-m-Y H:i:00", time()));
                    $fecha_entrada = strtotime($pedido->fecha_para_servicio);

                    if ($fecha_actual >= $fecha_entrada) {
                        $transaction->rollBack();
                        return $response->setContext(0, 'La fecha actual es mayor a la fecha de servicio. El cliente debe reagendar un nuevo servicio');
                    } else {
                        $horaInicio = new DateTime();
                        $horaTermino = new DateTime($pedido->fecha_para_servicio);
                        $interval = $horaTermino->diff($horaInicio);

                        if (YII_ENV_PROD && intval($interval->format('%H')) >= 1) {
                            $transaction->rollBack();
                            return $response->setContext(0, $interval->format('Lo sentimos, el servicio aun no puede confirmarse, intente nuevamente dentro de de %d dias %H horas %i minutos'));
                        }
                        $notification = $this->doNotification($pedido, $post['action_executor'], YouNeedContext::SOLICITUD_CONFIRMADA);

                        $flag = true;
                        $flag = $flag && $pedido->update(false);
                        $flag = $flag && $notification->save(false);

                        if ($flag) {
                            $emailResponse = EmailComposer::sendEmail(EmailContext::CONFIRMED_SERVICE, ['emailTo' => $pedido->cliente->email, 'body' => $pedido]);
                            $smsResponse = SMSComposer::sendSMS(SMSContext::CONFIRMED_ORDER, ['SMSTo' => $pedido->cliente->numero_celular, 'body' => $pedido]);
                            $transaction->commit();
                            return $response->setContext(1, ResponseConstants::SUCCESS_REGISTER,
                                ['emailResponse' => $emailResponse, 'smsResponse' => $smsResponse]);
                        } else {
                            Yii::error($pedido->validate());
                            Yii::error($notification->validate());
                            $transaction->rollBack();
                            return $response->setContext(null, 'ERROR AL ACTUALIZAR', null);
                        }
                    }
                } catch
                (Exception $e) {
                    Yii::error($e->getMessage());
                    $transaction->rollBack();
                    return $response->setContext(null, 'ERROR AL ACTUALIZAR EN LA Transaccion', null);
                }


            } else {
                return $response->setContext(null, ResponseConstants::INCORRECT_SENDED_PARAMETERS, null);
            }
        } else {
            return $response->setContext(null, ResponseConstants::INCORRECT_SENDED_PARAMETERS, null);
        }
    }

    public function actionTerminarpedido()
    {
        $response = new SimpleContextResponseApp();
        $post = Yii::$app->request->post();

          if ($post['id'] && $post['estado_previo'] == YouNeedContext::getIdStatusOrder(YouNeedContext::CONFIRMADO)
            && $post['estado_actual'] == YouNeedContext::getIdStatusOrder(YouNeedContext::TERMINADO)) {
            $pedido = Pedidos::findOne($post['id']);

            if ($pedido) {
                $pedido->estado = $post['estado_actual'];
                $transaction = Pedidos::getDb()->beginTransaction();

                try {
                    $notification = $this->doNotification($pedido, $post['action_executor'], YouNeedContext::SOLICITUD_TERMINADA);

                    $flag = true;
                    $flag = $flag && $pedido->update(false);
                    $flag = $flag && $notification->save(false);

                    if ($flag) {
                        $emailResponse = null;
                        $smsResponse = null;
                        $transaction->commit();
                        return $response->setContext(1, ResponseConstants::SUCCESS_REGISTER,
                            ['emailResponse' => $emailResponse, 'smsResponse' => $smsResponse]);
                    } else {
                        Yii::error($pedido->validate());
                        Yii::error($notification->validate());
                        $transaction->rollBack();
                        return $response->setContext(null, 'ERROR AL ACTUALIZAR', null);
                    }

                } catch (Exception $e) {
                    Yii::error($e->getMessage());
                    $transaction->rollBack();
                    return $response->setContext(null, 'ERROR AL ACTUALIZAR EN LA Transaccion', null);
                }

            } else {
                return $response->setContext(null, ResponseConstants::INCORRECT_SENDED_PARAMETERS, null);
            }
        } else {
            return $response->setContext(null, ResponseConstants::INCORRECT_SENDED_PARAMETERS, null);
        }
    }

    public function actionCancelarpedido()
    {
        $response = new SimpleContextResponseApp();
        $post = Yii::$app->request->post();

        if ($post['id'] && ($post['estado_previo'] == YouNeedContext::getIdStatusOrder(YouNeedContext::ACEPTADO)
                || $post['estado_previo'] == YouNeedContext::getIdStatusOrder(YouNeedContext::CONFIRMADO))
            && $post['estado_actual'] == YouNeedContext::getIdStatusOrder(YouNeedContext::CANCELADO)) {
            $pedido = Pedidos::findOne($post['id']);

            if (is_object($pedido)) {
                $pedido->estado = $post['estado_actual'];
                $transaction = Pedidos::getDb()->beginTransaction();

                try {
                    $notification = $this->doNotification($pedido, $post['action_executor'], YouNeedContext::SOLICITUD_CANCELADA);
                    $flag = true;
                    $flag = $flag && $pedido->update(false);
                    $flag = $flag && $notification->save(false);

                    if ($flag) {
                        $emailResponse = EmailComposer::sendEmail(EmailContext::CANCELED_SERVICE_FROM_CLIENT,
                            ['emailTo' => $pedido->asociado->email, 'pedido' => $pedido]);

                        $smsResponse = null;
                        $transaction->commit();
                        return $response->setContext(4, ResponseConstants::SUCCESS_REGISTER,
                            ['emailResponse' => $emailResponse, 'smsResponse' => $smsResponse]);

                    } else {
                        Yii::error($pedido->validate());
                        Yii::error($notification->validate());
                        $transaction->rollBack();
                        return $response->setContext(null, 'ERROR AL ACTUALIZAR', null);
                    }

                } catch (Exception $e) {
                    Yii::error($e->getMessage());
                    $transaction->rollBack();
                    return $response->setContext(null, 'ERROR AL ACTUALIZAR EN LA Transaccion', null);
                }
            }
        }


        return $response->setContext(null, YouNeedContext::INCORRECT_SENDED_PARAMETERS, null);

    }

    public function actionReagendarpedido()
    {
        $response = new SimpleContextResponseApp();
        $post = Yii::$app->request->post();

        if ($post['id'] && (
                $post['estado_previo'] == YouNeedContext::getIdStatusOrder(YouNeedContext::ACEPTADO) ||
                $post['estado_previo'] == YouNeedContext::getIdStatusOrder(YouNeedContext::CONFIRMADO)
            )
            && $post['estado_actual'] == YouNeedContext::getIdStatusOrder(YouNeedContext::EN_ESPERA)) {
            $pedido = Pedidos::findOne($post['id']);

            if ($pedido) {
                $pedido->estado = $post['estado_actual'];

                // TO validate dates
                $extra_data_post = json_decode($post['extra']);
                $new_fecha_para_servicio = new  DateTime($extra_data_post->fecha_para_servicio);
                $old_fecha_para_servicio = new  DateTime($pedido->fecha_para_servicio);
                if ($new_fecha_para_servicio < $old_fecha_para_servicio) {
                    return $response->setContext(-1, 'La nueva fecha del servicio es menor', null);
                }

                $pedido->fecha_para_servicio = $new_fecha_para_servicio->format('Y-m-d H:i:s');
                $transaction = Pedidos::getDb()->beginTransaction();

                try {
                    $notification = $this->doNotification($pedido, $post['action_executor'],
                          YouNeedContext::SOLICITUD_REAGENDADA);

                    $flag = true;
                    $flag = $flag && $pedido->update(false);
                    $flag = $flag && $notification->save(false);

                    if ($flag) {
                        $emailResponse = null;
                        $smsResponse = null;
                        $transaction->commit();
                        return $response->setContext(1, ResponseConstants::SUCCESS_REGISTER,
                            ['emailResponse' => $emailResponse, 'smsResponse' => $smsResponse]);
                    } else {
                        Yii::error($pedido->validate());
                        $transaction->rollBack();
                        return $response->setContext(null, 'ERROR AL ACTUALIZAR', null);
                    }

                } catch (Exception $e) {
                    Yii::error($e->getMessage());
                    $transaction->rollBack();
                    return $response->setContext(null, 'ERROR AL ACTUALIZAR EN LA Transaccion', null);
                }

            } else {
                return $response->setContext(null, ResponseConstants::INCORRECT_SENDED_PARAMETERS, null);
            }
        } else {
            return $response->setContext(null, ResponseConstants::INCORRECT_SENDED_PARAMETERS, null);
        }
    }
    //--------------------- FIN MANEJO DE ESTADOS del pedido ------------------------------


    /**
     * Obtains the total information for a Pedido
     * @param Integer $pedido_id
     * @return array
     */
    private function getInfoPedido($pedido_id): array
    {
        $infoPedido = new CompleteInfoPedido();
        $secureDataUser = new SecureDataUser();
        $pedido = Pedidos::findOne($pedido_id);
        $infoPedido->setPedido($pedido->attributes);
        $infoPedido->setAsociado(isset($pedido->asociado) ? $secureDataUser->extractDataFromModel($pedido->asociado) : NULL);
        $infoPedido->setCiudad(isset($pedido->ciudad) ? $pedido->ciudad->attributes : NULL);
        $infoPedido->setCliente(isset($pedido->cliente) ? $secureDataUser->extractDataFromModel($pedido->cliente) : NULL);
        $infoPedido->setServicio(isset($pedido->servicio) ? $pedido->servicio->attributes : NULL);
        $infoPedido->setEstado(Yii::$app->params['estados_pedidos'][$pedido->estado]);
        $infoPedido->setFormaPago('PENDIENTE');
        $infoPedido->setTipoAtencion(Yii::$app->params['tipo_atencion'][$pedido->tipo_atencion]);
        return $infoPedido->getInfoPedido();
    }

    /**
     * @param Pedidos $pedido
     * @param string $target
     * @param string $descripcion
     * @return Notificaciones
     */
    private function doNotification(Pedidos $pedido, string $target, string $descripcion): Notificaciones
    {
        $notificacion = new Notificaciones();
        $notificacion->model_id = $pedido->id;
        $notificacion->model_name = $pedido::tableName();
        $now = new DateTime();
        $notificacion->fecha_notificacion = $now->format('Y-m-d H:i:s');
        $notificacion->usuario_id = $target === 'ASOCIADO' ? $pedido->cliente_id : $pedido->asociado_id;
        $notificacion->tipo_notificacion_id = TiposNotificaciones::find()->where(['descripcion' => $descripcion])->one()->getAttribute('id');
        return $notificacion;
    }
}