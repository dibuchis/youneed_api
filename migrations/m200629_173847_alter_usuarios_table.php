<?php

use yii\db\Migration;

/**
 * Class m200629_173847_alter_usuarios_table
 */
class m200629_173847_alter_usuarios_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('usuarios',['pais_id'=>1,'ciudad_id'=>1],'ciudad_id IS NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
