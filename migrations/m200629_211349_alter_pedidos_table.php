<?php

use yii\db\Migration;

/**
 * Class m200629_211349_alter_pedidos_table
 */
class m200629_211349_alter_pedidos_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pedidos','subtotal_cliente','decimal(10,2) default 0');
        $this->addColumn('pedidos','iva_cliente','decimal(10,2) default 0');
        $this->addColumn('pedidos','iva_0_cliente','decimal(10,2) default 0');
        $this->addColumn('pedidos','iva_impuesto_cliente','decimal(10,2) default 0');
        $this->addColumn('pedidos','total_cliente','decimal(10,2) default 0');
        $this->addColumn('pedidos','identificacion_factura','varchar(200)');
        $this->addColumn('pedidos','nombre_factura','varchar(200)');
        $this->addColumn('pedidos','direccion','varchar(200)');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('pedidos','subtotal_cliente');
        $this->dropColumn('pedidos','iva_cliente');
        $this->dropColumn('pedidos','iva_0_cliente');
        $this->dropColumn('pedidos','iva_impuesto_cliente');
        $this->dropColumn('pedidos','total_cliente');
        $this->dropColumn('pedidos','identificacion_factura');
        $this->dropColumn('pedidos','nombre_factura');
        $this->dropColumn('pedidos','direccion');
    }


}
