<?php

use yii\db\Migration;

/**
 * Class m200704_081808_fix_facturacion_pedido
 */
class m200704_081808_fix_facturacion_pedido extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn("pedidos", "facturacion_cliente_id", "integer(11) default null");
		$this->alterColumn("pedidos", "facturacion_asociado_id", "integer(11) default null");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200704_081808_fix_facturacion_pedido cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200704_081808_fix_facturacion_pedido cannot be reverted.\n";

        return false;
    }
    */
}
