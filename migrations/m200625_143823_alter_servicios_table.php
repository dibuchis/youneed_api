<?php

use yii\db\Migration;

/**
 * Class m200625_143823_alter_servicios_table
 */
class m200625_143823_alter_servicios_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('servicios', 'tiene_soft_delete', 'boolean default false ');
        $this->addColumn('servicios', 'fecha_soft_delete', 'datetime ');
        $this->addColumn('servicios', 'user_soft_delete', 'integer ');
        $this->update('pedidos', ['cantidad' => 1], ['cantidad' => null]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('servicios', 'tiene_soft_delete');
        $this->dropColumn('servicios', 'fecha_soft_delete');
        $this->dropColumn('servicios', 'user_soft_delete');
    }

}
