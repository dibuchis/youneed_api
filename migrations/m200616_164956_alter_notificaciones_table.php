<?php

use yii\db\Migration;

/**
 * Class m200616_164956_alter_notificaciones_table
 */
class m200616_164956_alter_notificaciones_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notificaciones','model_name','varchar(100) null');
        $this->addColumn('notificaciones','model_id','integer null');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notificaciones','model_name');
        $this->dropColumn('notificaciones','modelo_id');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200616_164956_alter_notificaciones_table cannot be reverted.\n";

        return false;
    }
    */
}
