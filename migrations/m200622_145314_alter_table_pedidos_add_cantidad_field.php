<?php

use yii\db\Migration;

/**
 * Class m200622_145314_alter_table_pedidos_add_cantidad_field
 */
class m200622_145314_alter_table_pedidos_add_cantidad_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pedidos','cantidad','integer null');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('pedidos','cantidad');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200616_164956_alter_notificaciones_table cannot be reverted.\n";

        return false;
    }
    */
}

