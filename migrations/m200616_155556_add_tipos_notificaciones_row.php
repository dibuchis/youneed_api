<?php

use yii\db\Migration;

/**
 * Class m200616_155556_add_tipos_notificaciones_row
 */
class m200616_155556_add_tipos_notificaciones_row extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('tipos_notificaciones',['categoria', 'descripcion', 'mensaje'],[
            [3,'solicitud rechazada','Tu solicitud ha sido rechazada por el proveedor'],
            [3,'solicitud confirmada','Tu solicitud ha sido confirmada por el proveedor'],
            [3,'solicitud terminada','Tu solicitud ha llegado a termino'],
            [3,'solicitud reagendada','Tu solicitud ha sido reagendada'],
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->delete('tipos_notificaciones','descripcion=="solicitud rechazada"');
       $this->delete('tipos_notificaciones','descripcion=="solicitud confirmada"');
       $this->delete('tipos_notificaciones','descripcion=="solicitud terminada"');
       $this->delete('tipos_notificaciones','descripcion=="solicitud reagendada"');
    }
}
