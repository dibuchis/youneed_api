<?php

use yii\db\Migration;

/**
 * Class m200704_071728_drop_fields_pedidos_factura
 */
class m200704_071728_drop_fields_pedidos_factura extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('pedidos','identificacion_factura');
        $this->dropColumn('pedidos','nombre_factura');
        $this->dropColumn('pedidos','direccion'); 
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200704_071728_drop_fields_pedidos_factura cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200704_071728_drop_fields_pedidos_factura cannot be reverted.\n";

        return false;
    }
    */
}
