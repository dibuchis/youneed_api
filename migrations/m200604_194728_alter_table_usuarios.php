<?php

use yii\db\Migration;

/**
 * Class m200604_194728_alter_table_usuarios
 */
class m200604_194728_alter_table_usuarios extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        //only mysql SET GLOBAL max_allowed_packet=16777216;
        $this->alterColumn('usuarios', 'imagen', 'LONGTEXT');
        $this->alterColumn('usuarios', 'fotografia_cedula', 'LONGTEXT');
        $this->alterColumn('usuarios', 'ruc', 'LONGTEXT');
        $this->alterColumn('usuarios', 'visa_trabajo', 'LONGTEXT');
        $this->alterColumn('usuarios', 'rise', 'LONGTEXT');
        $this->alterColumn('usuarios', 'referencias_personales', 'LONGTEXT');
        $this->alterColumn('usuarios', 'titulo_academico', 'LONGTEXT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('usuarios','imagen','TEXT');
        $this->alterColumn('usuarios', 'fotografia_cedula', 'TEXT');
        $this->alterColumn('usuarios', 'ruc', 'TEXT');
        $this->alterColumn('usuarios', 'visa_trabajo', 'TEXT');
        $this->alterColumn('usuarios', 'rise', 'TEXT');
        $this->alterColumn('usuarios', 'referencias_personales', 'TEXT');
        $this->alterColumn('usuarios', 'titulo_academico', 'TEXT');
    }

    }
