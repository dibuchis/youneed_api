<?php

use yii\db\Migration;

/**
 * Class m200526_000307_alter_table_documentos
 */
class m200526_000307_alter_table_documentos extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('documentos','imagen','LONGTEXT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('documentos','imagen','TEXT');
    }


}
