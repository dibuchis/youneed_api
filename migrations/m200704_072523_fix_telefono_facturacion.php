<?php

use yii\db\Migration;

/**
 * Class m200704_072523_fix_telefono_facturacion
 */
class m200704_072523_fix_telefono_facturacion extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn('facturacion', 'telefono', 'varchar(45) not null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('facturacion', 'telefono');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200704_072523_fix_telefono_facturacion cannot be reverted.\n";

        return false;
    }
    */
}
