<?php

use yii\db\Migration;

/**
 * Class m200704_065722_add_fields_to_pedidos
 */
class m200704_065722_add_fields_to_pedidos extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pedidos','subtotal_asociado','decimal(10,2) default 0');
        $this->addColumn('pedidos','iva_asociado','decimal(10,2) default 0');
        $this->addColumn('pedidos','iva_0_asociado','decimal(10,2) default 0');
        $this->addColumn('pedidos','iva_impuesto_asociado','decimal(10,2) default 0');
        $this->addColumn('pedidos','total_asociado','decimal(10,2) default 0');
        $this->addColumn('pedidos','facturacion_cliente_id','decimal(10,2) default 0');
        $this->addColumn('pedidos','facturacion_asociado_id','decimal(10,2) default 0');
		
		$this->createTable('facturacion', [
            'id' => $this->primaryKey(),
            'usuario_id' => $this->integer()->notNull(),
            'identificacion' => $this->integer()->notNull(),
            'nombre' => $this->string(200)->notNull(),
            'direccion' => $this->string(300)->notNull(),
            'telefono' => $this->integer()->notNull(),
            'correo_electronico' => $this->string(300)->notNull(),
            'referencia_ubicacion' => $this->string(300)->notNull(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('pedidos','subtotal_asociado');
        $this->dropColumn('pedidos','iva_asociado');
        $this->dropColumn('pedidos','iva_0_asociado');
        $this->dropColumn('pedidos','iva_impuesto_asociado');
        $this->dropColumn('pedidos','total_asociado');
        $this->dropColumn('pedidos','identificacion_factura');
        $this->dropColumn('pedidos','nombre_factura');
        $this->dropColumn('pedidos','direccion');
		$this->dropTable('facturacion');
    }

}
