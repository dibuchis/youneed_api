<?php

use yii\db\Migration;

/**
 * Class m200622_171239_create_table_medidas
 */
class m200622_171239_create_table_medidas extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('medidas', [
            'id' => $this->primaryKey(),
            'descripcion' => $this->string(120)->notNull(),
            'sufijo' => $this->string(15)->notNull()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('medidas');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200622_165439_create_table_medidas cannot be reverted.\n";

        return false;
    }
    */
}
