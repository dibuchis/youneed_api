<?php

use yii\db\Migration;

/**
 * Class m200622_171301_alter_table_servicios_add_medida_id_fk
 */
class m200622_171301_alter_table_servicios_add_medida_id_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('servicios', 'medida_id', "integer");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200622_171301_alter_table_servicios_add_medida_id_fk cannot be reverted.\n";

        return false;
    }
    */
}
