<?php

use yii\db\Migration;

/**
 * Class m200512_233814_fix_work_time_to_user_table
 */
class m200512_233814_fix_work_time_to_user_table extends Migration
{

    public function safeUp()
    {
        $this->dropColumn('usuarios', 'horarios_trabajo');
        $this->dropColumn('usuarios', 'dias_trabajo');
        $this->addColumn('usuarios', 'jornada_trabajo', $this->text());
    }


    public function safeDown()
    {
        $this->addColumn('usuarios', 'horarios_trabajo', $this->integer());
        $this->addColumn('usuarios', 'dias_trabajo', $this->integer());
        $this->dropColumn('usuarios', 'jornada_trabajo');
    }

}
