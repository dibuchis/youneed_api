<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%usuarios_servicios}}`.
 */
class m200520_164453_add_document_id_column_to_usuarios_servicios_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%usuarios_servicios}}', 'documento_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%usuarios_servicios}}', 'documento_id');
    }
}
