<?php

use yii\db\Migration;

/**
 * Class m200622_175827_alter_table_medidas_fix_lenght_sufijo
 */
class m200622_175827_alter_table_medidas_fix_lenght_sufijo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn("medidas", 'sufijo', "varchar(20)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200622_175827_alter_table_medidas_fix_lenght_sufijo cannot be reverted.\n";

        return false;
    }
    */
}
